<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Api',
], function () {

    Route::group(['prefix' => 'home'], function () {
        Route::get("list_brands","HomeController@brands");
        Route::get("search_products","HomeController@search_products");
    });

    Route::group(['prefix' => 'lists'],function (){
        Route::get('cities', 'HomeController@cities');
        Route::get('car_types', 'HomeController@car_types');
        Route::get('categories', 'HomeController@categories');
        Route::get('charity_addresses', 'HomeController@charity_addresses');
        Route::get('user_addresses', 'HomeController@user_addresses');
    });

    Route::group(['prefix' => 'Auth'], function () {
        Route::post('login_user', 'AuthController@login');
        Route::post('register_user', 'AuthController@register');
        Route::post('register_delivery', 'AuthController@register_delivery');
        Route::post('login_delivery', 'AuthController@login_delivery');
        Route::post('register_user', 'AuthController@register');
        Route::post('forgetPassword', 'AuthController@forgetPassword');
        Route::post('resetPassword', 'AuthController@resetPassword');
        Route::post('checkCode', 'AuthController@checkCodeActivation');
        Route::post('checkCodeCorrect', 'AuthController@checkCodeCorrect');
        Route::post('resendCode', 'AuthController@resendCode');
        Route::post('changPassword', 'AuthController@changPassword');
        Route::post('editProfile', 'AuthController@editProfile');
        Route::post('logOut', 'AuthController@logOut');
    });


    Route::resource('user_address','AddressController',[
                'parameters' => ['user_address' => 'address']
        ]);
    Route::post('user_address/{address}/update_is_default','AddressController@update_is_default');
    Route::get("users/list_favourite_products","ProductController@list_favourite_products");
    Route::get("users/list_favourite_brands","ProductController@list_favourite_brands");
    Route::get('users/user_info',"UserController@user_info");


    Route::resource('products', 'ProductController');
    Route::get('products/{product}/list_comments', 'ProductController@list_comments');
    Route::post('products/{product}/rate', 'ProductController@make_rate');
    Route::post('products/{product}/favourite_product', 'ProductController@make_favourite_product');
    Route::post('products/{brand}/favourite_brand', 'ProductController@make_favourite_brand');

    Route::get("cart_data","OrderController@cart_data");
    Route::resource('orders', 'OrderController');
    Route::get('orders_list_delivery', 'OrderController@list_order_delivery');
    Route::post('orders/promo_code', 'OrderController@promo_code');
    Route::post('orders/accepted', 'OrderController@accepted');
    Route::post('orders/refuse_delivery', 'OrderController@refuse_delivery');
    Route::post('orders/refuse_delivery_underway', 'OrderController@refuse_delivery_underway');
    Route::post('orders/finish', 'OrderController@finish');

    Route::group(['prefix' => 'setting'], function () {
        Route::get('aboutUs', 'SettingController@aboutUs');
        Route::get('terms_user', 'SettingController@terms_user');
        Route::get('typesSupport', 'SettingController@getTypesSupport');
        Route::get('terms_delivery', 'SettingController@terms_delivery');
        Route::post('contact_us','SettingController@contact_us');
    });


    Route::get("notifications","NotificationController@index");



    Route::put("/update_products",function (Request  $request){

        foreach ($request->products as $product){

            $item =  \App\Models\UserProduct::where('user_id','=',75)
                ->where('product_id','=',$product['product_id'])->first();

            $item->decrement('quantity',$product['quantity']);
        }
//        $products = \Illuminate\Support\Facades\DB::table("user_product")->update(['quantity' => 100]);

    });


    Route::get("products_agent_id",function (){

        $products = \Illuminate\Support\Facades\DB::table("user_product")
            ->where('user_id','=',75)->latest('id')->get();
//
        return $products;
    });

});
