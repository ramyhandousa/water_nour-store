<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

//Route::get('lang/{language}', 'LanguageController@switchLang')->name('lang.switch');

Route::group(['prefix' => 'administrator'], function () {


    Route::get('/login', 'Admin\LoginController@login')->name('admin.login');
    Route::post('/login', 'Admin\LoginController@postLogin')->name('admin.postLogin');

    // Password Reset Routes...

    Route::get('password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('administrator.password.request');
    Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('administrator.password.email');
    Route::get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('administrator.password.reset.token');
    Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset');

});
Route::group(['prefix' => 'administrator','namespace' => 'Admin', 'middleware' => ['admin']], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('admin.home');

    Route::resource('helpAdmin', 'HelpAdminController');
    Route::get('helpAdmin/{id}/delete', 'HelpAdminController@delete')->name('helpAdmin.delete');
    Route::post('helpAdmin/{id}/delete', 'HelpAdminController@deleteHelpAdmin')->name('helpAdmin.message.delete');
    Route::post('helpAdmin/{id}/suspend', 'HelpAdminController@suspendHelpAdmin')->name('helpAdmin.message.suspend');
    Route::get('user/{id}/delete', 'UsersController@delete')->name('user.for.delete');
    Route::post('user/suspend', 'HelpAdminController@suspend')->name('user.suspend');
    // Roles routes ..........
    Route::resource('roles', 'RolesController');
    Route::post('role/delete', 'RolesController@delete')->name('role.delete');

    Route::resource('users', 'UsersController');
    Route::post('refuseUser','UsersController@refuseUser')->name('user.refuseUser');
    Route::post('reAcceptUser','UsersController@reAcceptUser')->name('user.reAcceptUser');
    Route::post('accepted','UsersController@accpetedUser')->name('user.accepted');
    Route::post('suspendUser','UsersController@suspendUser')->name('user.suspendUser');

    Route::resource('deliveries', 'DeliveryController');
    Route::post('deliveries/accepted','DeliveryController@accpetedUser')->name('deliveries.accepted');
    Route::post('deliveries/refuseUser','DeliveryController@refuseUser')->name('deliveries.refuseUser');

    Route::resource('agents', 'AgentController');

    Route::resource('cities', 'CitiesController');
    Route::post('cities/delete', 'CitiesController@delete')->name('cities.delete');
    Route::post('city/suspend', 'CitiesController@suspend')->name('city.suspend');

    // -------------------------------------- products .................
    Route::resource('products', 'ProductController');
    Route::post('products/delete', 'ProductController@delete')->name('products.delete');
    Route::post('products/suspend', 'ProductController@suspend')->name('products.suspend');

    // -------------------------------------- agent products .................
    Route::resource('agent_products', 'AgentProductController');
    Route::post('agent_products/delete', 'AgentProductController@delete')->name('agent_products.delete');
    Route::post('agent_products/suspend', 'AgentProductController@suspend')->name('agent_products.suspend');
    Route::post('agent_products/renderProductData', 'AgentProductController@renderProductData')->name('agent_products.renderProductData');

    // -------------------------------------- agent products Offers .................
    Route::resource('agent_product_offer', 'AgentProductOfferController');
    Route::post('agent_product_offer/delete', 'AgentProductOfferController@delete')->name('agent_product_offer.delete');
    Route::post('agent_product_offer/suspend', 'AgentProductOfferController@suspend')->name('agent_product_offer.suspend');

    // -------------------------------------- offer products .................
    Route::resource('offer_products', 'OfferProductController');
    Route::post('offer_products/delete', 'OfferProductController@delete')->name('offer_products.delete');
    Route::post('offer_products/suspend', 'OfferProductController@suspend')->name('offer_products.suspend');

    // --------------------------------------  promo_codes .................
    Route::resource('promo_codes', 'PromoCodesController');
    Route::post('promo_codes/delete', 'PromoCodesController@delete')->name('promo_codes.delete');
    Route::post('promo_codes/suspend', 'PromoCodesController@suspend')->name('promo_codes.suspend');

    // -------------------------------------- categories .................
    Route::resource('categories', 'CategoriesController');
    Route::post('categories/delete', 'CategoriesController@delete')->name('categories.delete');
    Route::post('categories/suspend', 'CategoriesController@suspend')->name('categories.suspend');

    // -------------------------------------- cars .................
    Route::resource('cars', 'CarController');
    Route::post('cars/delete', 'CarController@delete')->name('cars.delete');
    Route::post('cars/suspend', 'CarController@suspend')->name('cars.suspend');

    // -------------------------------------- brands .................
    Route::resource('brands', 'BrandController');
    Route::post('brands/delete', 'BrandController@delete')->name('brands.delete');
    Route::post('brands/suspend', 'BrandController@suspend')->name('brands.suspend');

    // -------------------------------------- charity_addresses .................
    Route::resource('charity_addresses', 'CharityAddressController');
    Route::post('charity_addresses/delete', 'CharityAddressController@delete')->name('charity_addresses.delete');
    Route::post('charity_addresses/suspend', 'CharityAddressController@suspend')->name('charity_addresses.suspend');

    // -------------------------------------- notifications_admin .................
    Route::resource('notifications_admin', 'NotificationController');
    Route::get('notifications/promo_code', 'NotificationController@promo_code')->name('notifications_admin.promo_code');
    Route::get('notifications/get_promo_code', 'NotificationController@get_promo_code')->name('notifications_admin.get_promo_code');
    Route::get('notifications/renderUsesData', 'NotificationController@renderUsesData')->name('notifications_admin.renderUsesData');
    Route::post('notifications_admin/send_promo_code', 'NotificationController@send_promo_code')->name('notifications_admin.send_promo_code');
    Route::post('notifications_admin/delete', 'NotificationController@delete')->name('notifications_admin.delete');

    Route::get('settings/contacts', 'SettingsController@contactus')->name('settings.contactus');
    Route::get('settings/orderPricing', 'SettingsController@orderPricing')->name('settings.order.pricing');
    Route::get('settings/aboutus', 'SettingsController@aboutus')->name('settings.aboutus');
    Route::get('settings/terms', 'SettingsController@terms')->name('settings.terms');
    Route::get('settings/termsProvider', 'SettingsController@termsGym')->name('settings.termsGym');
    Route::get('settings/termsDelivery', 'SettingsController@termsDelivery')->name('settings.termsDelivery');

    Route::post('/settings', 'SettingsController@store')->name('administrator.settings.store');
    Route::post('/settings/packageStore', 'SettingsController@packageStore')->name('administrator.package.store');


    Route::resource('banks', 'BanksController');
    Route::post('bank/suspend', 'BanksController@suspend')->name('bank.suspend');

    Route::get('bank-transfer-admin', 'BankTransferController@index')->name('bank-transfer-admin');
    Route::post('accept-transfer-admin', 'BankTransferController@accepted')->name('accept-transfer-admin');
    Route::post('refuse-transfer-admin', 'BankTransferController@refuse')->name('refuse-transfer-admin');

    Route::get('subscriptions-dealers', 'SubscriptionController@index')->name('subscriptions-dealers');
    Route::post('subscriptions-dealers', 'SubscriptionController@accepted')->name('accept-subscriptions-dealers');


    Route::resource('contact_us_inbox', 'ContactUsController');
    Route::get('updateIsRead', 'ContactUsController@updateIsRead')->name('admin.support.updateIsRead');
    Route::get('updateIsDeleted', 'ContactUsController@updateIsDeleted')->name('admin.support.updateIsDeleted');
    Route::get('removeAllMessages', 'ContactUsController@removeAllMessages')->name('admin.support.removeAllMessages');


    Route::get('invoices_orders','ReportsController@invoices_orders')->name("admin.invoices_orders");
    Route::get('invoices_orders_show/{user}','ReportsController@invoices_orders_show')->name("admin.invoices_orders_show");
    Route::get('invoices_orders_excel/{user}','ReportsController@invoices_orders_excel')->name("admin.invoices_orders_excel");
    Route::post('/logout', 'LoginController@logout')->name('administrator.logout');

});



Route::get('/sub', function (Illuminate\Http\Request $request) {

    $cities =\App\Models\City::whereParentId($request->id)->get();

    if (!empty($cities) && count($cities) > 0){
        return response()->json( $cities);
    }else{

        return response()->json(401);
    }


})->name('getSub');



Route::get('roles', function () {

    $user = auth()->user();
//    $user->retract('admin');
    $user->assign('*');
    Bouncer::allow('*')->everything();
    $user->allow('users_manage');
});


Route::post('user/update/token', function (Illuminate\Http\Request $request) {

    $user = \App\User::whereId($request->id)->first();


    if ($request->token) {
        $data = \App\Models\Device::where('device', $request->token)->first();
        if ($data) {
            $data->user_id = $user->id;
            $data->save();
        } else {


            $data = new \App\Models\Device;
            $data->device = $request->token;
            $data->user_id = $user->id;
            $data->device_type = 'web';
            $data->save();
        }
    }


})->name('user.update.token');


Route::get('test_image',function (){

    $user = \App\User::with('profile.media')->find(5);

    $image = $user->profile->getMedia('default', ['image' => 'profile']);

//    $image = $user->profile->getMedia('default');
//    $image = $user->profile->getFirstMediaUrl();
    $data = [
        'url' => $image->first() ? $image[0]->getUrl() : null,
        'name' => 'ramy'
    ];
    return $data;
});


Route::get("php_info",function (){

//    phpinfo();
});


Route::get("test_tabel",function (){

    $users =  DB::select('describe users');
//    $data = (DB::select(DB::raw('SHOW FIELDS FROM users')));

//    return collect($users)->reject(function ($value, $key) {
////        return data_get($value, 'Field') == "id";
//    })->values();
    return collect($users)->skip(20)->values();
});



