@extends('admin.layouts.master')

@section("title", __("maincp.call_us"))
@section('styles')

    <style>
        .customeStyleSocail{

            margin: 10px auto;

        }

        .hidden{
            display: none;
        }
    </style>
@endsection
@section('content')
    <form action="{{ route('administrator.package.store') }}" data-parsley-validate novalidate method="post"
          enctype="multipart/form-data">
    {{ csrf_field() }}
    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-0">
                    <div class="btn-group pull-right m-t-15">
                        <button type="button" class="btn btn-custom  waves-effect waves-light"
                                onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                        </button>
                    </div>

                </div>
                <h4 class="page-title">  التحكم في أسعار التوصيل  </h4>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive m-t-0">

                    <div class="form-group">
                        <section class="pricing py-5">
                            <div class="container">
                                <div class="row">
                                    <!-- Free Tier -->
                                    <div class="col-lg-8">
                                        <div class="card mb-5 mb-lg-0">
                                            <div class="card-body">
                                                <h5 class="card-title text-muted text-uppercase text-center">  الأسعار</h5>
                                                <hr>
                                                <ul class="fa-ul">
                                                    <div class="col-md-6">
                                                        <span class="input-group-addon" id="basic-addon2">  السعر</span>
                                                        <input type="text" class="form-control percent_app input_first_price" min="1" max="1000" value="{{$pricePakage->getPrice(1)}}" name="1[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <span class="input-group-addon">  المسافة بالكيلو</span>
                                                        <input type="text"  class="form-control percent_app input_first_kilo" min="1" max="1000" value="{{$pricePakage->getKilo(1)}}" name="1[kilometer]" >
                                                        <br>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control percent_app input_second_price" min="1" max="1000" value="{{$pricePakage->getPrice(2)}}" name="2[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text"  class="form-control percent_app input_second_kilo" min="1" max="1000" value="{{$pricePakage->getKilo(2)}}" name="2[kilometer]" >
                                                        <br>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control percent_app input_three_price" min="1" max="1000" value="{{$pricePakage->getPrice(3)}}" name="3[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control percent_app input_three_kilo" min="1" max="1000" value="{{$pricePakage->getKilo(3)}}" name="3[kilometer]" >
                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <br>
                        <div class="col-xs-12 text-right">

                            <button type="submit" class="btn btn-warning hidden"  id="spinnerDiv">
                                @lang('maincp.save_data')
                            </button>

                        </div>

                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
    <script type="text/javascript" src="/assets/admin/js/validate-ar.js"></script>


    <script type="text/javascript">

        $(document).ready(function() {
            $("#spinnerDiv").removeClass("hidden");
        });



        function validation_inputs(){
         var first_price =   $(".input_first_price").val(),
             first_kilo =   $(".input_first_kilo").val(),
             second_price =   $(".input_second_price").val(),
             second_kilo =   $(".input_second_kilo").val(),
             three_price =   $(".input_three_price").val(),
             three_kilo =   $(".input_three_kilo").val()

            if (  !first_price || ! first_kilo|| !second_price || ! second_kilo || !three_price || ! three_kilo ) {
                errorMessageTostar("خطأ","من فضلك تأكد من السعر والكليو  للمرحل  لإضافته في التطبيق");
                return  false;
            }
            return true;
        }

        $('form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);

            var form = $(this);

            form.parsley().validate();
            if (form.parsley().isValid() &&  validation_inputs()) {
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        messageDisplay( 'نجاح' ,data.message )
                        {{--setTimeout(function () {--}}
                        {{--    window.location.href = '{{ route('settings.order') }}';--}}
                        {{--}, 1500);--}}
                    },
                    error: function (data) {
                    }
                });
            }
        });
        function messageDisplay($title, $message) {
            var shortCutFunction = 'success';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            $toastlast = toastr[shortCutFunction](msg, title);
        }

        function errorMessageTostar($title, $message) {
            var shortCutFunction = 'error';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            var $toast = toastr[shortCutFunction](msg, title);
            $toastlast = $toast;
        }

    </script>
@endsection

