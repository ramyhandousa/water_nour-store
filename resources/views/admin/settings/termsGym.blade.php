@extends('admin.layouts.master')
@section('title' ,__('maincp.use_Treaty'))

@section('content')
    <form method="post" action="{{ route('administrator.settings.store') }}" enctype="multipart/form-data"
          data-parsley-validate novalidate >
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-12 " >
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>

                </div>
                <h4 class="page-title">@lang('trans.terms')  </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 " >
                <div class="card-box">

{{--                    <div class="col-xs-12">--}}
{{--                        <div class="form-group {{ $errors->has('terms_provider_ar') ? 'has-error' : '' }}">--}}
{{--                            <label for="terms_clients">الشروط والاحكام باللغة العربية </label>--}}
{{--                            <textarea  class="form-control msg_body requiredField" required--}}
{{--                                      name="terms_provider_ar">{{ $setting->getBody('terms_provider_ar' ) }}--}}
{{--                            </textarea>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="col-xs-12">--}}
{{--                        <div class="form-group {{ $errors->has('terms_provider_en') ? 'has-error' : '' }}">--}}
{{--                            <label for="terms_clients">الشروط والاحكام  باللغة الإنجليزية </label>--}}
{{--                            <textarea  class="form-control msg_body requiredField" required--}}
{{--                                      name="terms_provider_en">{{ $setting->getBody('terms_provider_en' ) }} </textarea>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <h2 class="text-center">اللغة العربية</h2><br>
                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('terms_provider_ar') ? 'has-error' : '' }}">
                            <label for="terms_clients">النص الاول  </label>
                            <textarea id="editor" class="msg_body requiredField" required
                                      name="terms_provider_ar">
                                {{ $setting->getBody('terms_provider_ar' ) }}
                            </textarea>
                        </div>
                    </div>

{{--                    <div class="col-xs-6">--}}
{{--                        <div class="form-group {{ $errors->has('terms_provider1_ar') ? 'has-error' : '' }}">--}}
{{--                            <label for="terms_clients">النص الثاني </label>--}}
{{--                            <textarea id="editor1" class="msg_body requiredField" required--}}
{{--                                      name="terms_provider1_ar">--}}
{{--                                {{ $setting->getBody('terms_provider1_ar' ) }}--}}
{{--                            </textarea>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <h2 class="text-center">اللغة الإنجليزية</h2><br>
                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('terms_provider_en') ? 'has-error' : '' }}">
                            <label for="terms_clients">النص الاول </label>
                            <textarea id="editor1" class="msg_body requiredField" required
                                      name="terms_provider_en">
                                {{ $setting->getBody('terms_provider_en' ) }}
                            </textarea>
                        </div>
                    </div>

{{--                    <div class="col-xs-6">--}}
{{--                        <div class="form-group {{ $errors->has('terms_provider1_en') ? 'has-error' : '' }}">--}}
{{--                            <label for="terms_clients">النص الثاني </label>--}}
{{--                            <textarea id="editor3" class="msg_body requiredField" required--}}
{{--                                      name="terms_provider1_en">--}}
{{--                                {{ $setting->getBody('terms_provider1_en' ) }}--}}
{{--                            </textarea>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')

    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script>


        CKEDITOR.replace('editor1');
        // CKEDITOR.addCommand( 'editor1', new CKEDITOR.dialogCommand( 'editor1', {
        //
        //     allowedContent: allowed,   requiredContent: required   } ) );
        CKEDITOR.replace('editor2');
        CKEDITOR.replace('editor3');

    </script>


    <script type="text/javascript">

        // $('form').on('submit', function (e) {
        //     e.preventDefault();
        //     var formData = new FormData(this);
        //
        //     var form = $(this);
        //      form.parsley().validate();
        //
        //     if (form.parsley().isValid() ){
        //      $.ajax({
        //         type: 'POST',
        //          beforeSend: function()
        //          {
        //              $('#spinnerDiv').show();
        //
        //          },
        //         url: $(this).attr('action'),
        //         data: formData,
        //         cache: false,
        //         contentType: false,
        //         processData: false,
        //         success: function (data) {
        //
        //
        //             if(data.status == true){
        //                 //  $('#messageError').html(data.message);
        //                 $('#spinnerDiv').hide();
        //                 var shortCutFunction = 'success';
        //                 var msg = data.message;
        //                 var title = 'نجاح';
        //                 toastr.options = {
        //                     positionClass: 'toast-top-left',
        //                     onclick: null
        //                 };
        //                 var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
        //                 $toastlast = $toast;
        //              }
        //
        //         },
        //         error: function (data) {
        //         }
        //     });
        //     }
        // });

    </script>
@endsection


