
@extends('admin.layouts.master')
@section('title', 'عرض  بياناتي ')
@section('styles')

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap -->

    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/bootstrap-rtl.min.css">
    <!-- <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css"> -->
    <link rel="stylesheet" href='{{request()->root()}}/public/gym/css/animate.css'>
    <!-- time picker -->
    <link rel="stylesheet" type="text/css" href="{{request()->root()}}/public/gym/css/jquery.ptTimeSelect.css" />
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />
    <link rel="stylesheet" href="{{request()->root()}}/public/gym/css/style.css">


@endsection

@section('content')


    <div class="container">
        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                 من فضلك إختار موقعك من علي الخريطة
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('users.store') }}" id="main-form" enctype="multipart/form-data" data-parsley-validate
                  novalidate>
            {{ csrf_field() }}
                <!-- map -->
            <div class="col-md-12 wow fadeInDown">
                <!--<div class="col-md-8">-->
                <input id="pac-input" name="address_search"
                       class="controls"
                       type="text"
                       style="z-index: 1;position: absolute;top: 11px;left: 113px;height: 34px;width: 63%;"
                       placeholder="@lang('institutioncp.search_address')" required>
                <input type="hidden" name="latitute" id="lat"/>
                <input type="hidden" name="longitute" id="lng"/>
                <input type="hidden" name="address" id="address"/>
            <div id="googleMap" class=""></div>
            </div>
            <!-- upload image -->
                <div class="col-12 field wow fadeInDown{{ $errors->has('files') ? ' has-error' : '' }}">
                    <div class="text-center">
                        <input type="file"  id="files" name="images[]" multiple />
                        <label for="files" class="custom-file-upload text-center">
                            <i class="fas fa-camera"></i>
                        </label>
                        @if($errors->has('files'))
                            <p class="help-block">
                                {{ $errors->first('files') }}
                            </p>
                        @endif

                        @if($errors->any())
                            <h4 style="color: red">{{$errors->first()}}</h4>
                        @endif
                    </div>

                </div>
                <!--  -->
                <div class="col-md-6 form-group wow fadeInUp{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input type="text" name="name"  class="form-control" placeholder="الاسم" required data-parsley-required-message="يجب ادخال  اسم المستخدم">
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
                <!--  -->
                <div class="col-md-6 form-group wow fadeInUp{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <input type="number" name="phone" class="form-control" required
                           data-parsley-maxLength="20"
                           data-parsley-maxLength-message=" الاسم  يجب أن يكون عشرون حروف فقط"
                           data-parsley-minLength="5"
                           data-parsley-minLength-message=" الاسم  يجب أن يكون اكثر من 5 حروف "
                           data-parsley-required-message="يجب ادخال رقم الجوال"
                           placeholder="رقم الجوال...">
                    @if($errors->has('phone'))
                        <p class="help-block">
                            {{ $errors->first('phone') }}
                        </p>
                    @endif
                </div>
                <!--  -->
                <div class="col-md-6 form-group wow fadeInUp{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" name="email" class="form-control" placeholder="البريد الإلكتروني..." required
                           data-parsley-type="email"
                           data-parsley-type-message="أدخل البريد الالكتروني بطريقة صحيحة"
                           data-parsley-required-message="يجب ادخال  البريد الالكتروني"
                           data-parsley-maxLength="50"
                           data-parsley-maxLength-message=" البريد الالكتروني  يجب أن يكون 50 حرف فقط">
                    @if($errors->has('email'))
                        <p class="help-block">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group wow fadeInUp{{ $errors->has('price') ? ' has-error' : '' }}">
                    <input type="number" name="price" class="form-control" placeholder="السعر"
                           required data-parsley-required-message="يجب ادخال  السعر "
                           data-parsley-minLength="1"
                           data-parsley-minLength-message=" السعر  يجب أن يكون اكثر من   رقم "
                           data-parsley-maxLength="5"
                           data-parsley-maxLength-message="  السعر  يجب أن يكون 5 ارقام فقط">
                    @if($errors->has('price'))
                        <p class="help-block">
                            {{ $errors->first('price') }}
                        </p>
                    @endif
                </div>

                <div class="col-md-6 form-group wow fadeInUp{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="text" name="password" id="pass1" value="{{ old('password') }}"
                           class="form-control"
                           placeholder="كلمة المرور..."
                           required
                           data-parsley-required-message="هذا الحقل مطلوب"
                           data-parsley-maxLength="20"
                           data-parsley-maxLength-message=" الباسورد  يجب أن يكون عشرون حروف فقط"
                           data-parsley-minLength="5"
                           data-parsley-minLength-message=" الباسورد  يجب أن يكون اكثر من 5 حروف "
                    />

                    @if($errors->has('password'))
                        <p class="help-block">{{ $errors->first('password') }}</p>
                    @endif
                </div>

                <div class="col-md-6 form-group wow fadeInUp{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input   name="password_confirmation" type="text" required
                           placeholder="تأكيد كلمة المرور..." class="form-control" id="passWord2"
                           data-parsley-required-message="تأكيد كلمة المرور مطلوب"
                           data-parsley-maxlength="20"
                           data-parsley-minlength="5"
                           data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (20) حرف"
                           data-parsley-minlength-message=" أقل عدد الحروف المسموح بها هى (5) حرف"
                           data-parsley-equalto="#pass1"
                           data-parsley-equalto-message ='غير مطابقة لكلمة المرور'
                    >
                    @if($errors->has('password_confirmation'))
                        <p class="help-block">{{ $errors->first('password_confirmation') }}</p>
                    @endif
                </div>
                <!-- select -->
                <div class="col-md-6 form-group  wow fadeInUp">
                    <select name="city" class="form-control category"  required  data-parsley-required-message="من فضلك اختار  المدينة   " >
                        <option   > اختار المدينة</option>
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 form-group  wow fadeInUp">
                    <select name="city" class="form-control City_chid"  required  data-parsley-required-message="من فضلك اختار    الحي " >
                        <option disabled selected>اختار</option>
                    </select>
                </div>
                <!-- description -->
                <div class="col-md-12 form-group wow fadeInUp{{ $errors->has('description') ? ' has-error' : '' }}">
                    <textarea class="form-control" name="description" rows="5"  placeholder="الوصف"
                              required  data-parsley-required-message="من فضلك اكتب الوصف "
                              data-parsley-minLength="100"
                              data-parsley-minLength-message=" الوصف  يجب أن يكون اكثر من 100 حروف "
                              data-parsley-maxLength="3000"
                              data-parsley-maxLength-message=" الوصف  يجب أن يكون 3000 حروف فقط"></textarea>
                    @if($errors->has('description'))
                        <p class="help-block">{{ $errors->first('description') }}</p>
                    @endif
                </div>
                <!-- workdays -->
                <div class="col-md-12 wow fadeInDown">
                    <h3>
                        ايام العمل
                    </h3>
                    <div class=" workdays">
                        <div class="check" id="check">
                            <input type="checkbox" id="1" name="day[1]" value="1">
                            <label for="1">السبت</label>
                        </div>
                        <div id="time" class=" time-1">
                            <input type="hidden"  name="day[1][day_id]" value="1" >
                            <input type="text" id="from1" name="day[1][start]" placeholder="من">
                            <input type="text" id="to1" name="day[1][end]" placeholder="الى">
                        </div>
                    </div>
                    <div class=" workdays">
                        <div class="check" id="check">
                            <input type="checkbox" id="2" name="day[2]" value="2">
                            <label for="2">الأحد</label>
                        </div>
                        <div id="time" class=" time-2">
                            <input type="hidden"  name="day[2][day_id]" value="2" >
                            <input type="text" id="from2" name="day[2][start]" placeholder="من">
                            <input type="text" id="to2" name="day[2][end]" placeholder="الى">
                        </div>
                    </div>
                    <div class=" workdays">
                        <div class="check" id="check">
                            <input type="checkbox" id="3" name="day[3]" value="3">
                            <label for="3">الاثنين</label>
                        </div>
                        <div id="time" class=" time-3">
                            <input type="hidden"  name="day[3][day_id]" value="3" >
                            <input type="text" id="from" name="day[3][start]" placeholder="من">
                            <input type="text" id="to" name="day[3][end]" placeholder="الى">
                        </div>
                    </div>
                    <div class=" workdays">
                        <div class="check">
                            <input type="checkbox" id="4" name="day[4]" value="4">
                            <label for="4">الثلاثاء</label>
                        </div>
                        <div id="time" class=" time-4">
                            <input type="hidden"  name="day[4][day_id]" value="4" >
                            <input type="text" id="from" name="day[4][start]" placeholder="من">
                            <input type="text" id="to" name="day[4][end]" placeholder="الى">
                        </div>
                    </div>
                    <div class=" workdays">
                        <div class="check">
                            <input type="checkbox" id="5" name="day[5]" value="5">
                            <label for="5">الأربعاء</label>
                        </div>
                        <div id="time" class=" time-5">
                            <input type="hidden"  name="day[5][day_id]" value="5" >
                            <input type="text" id="from" name="day[5][start]" placeholder="من">
                            <input type="text" id="to" name="day[5][end]" placeholder="الى">
                        </div>
                    </div>
                    <div class=" workdays">
                        <div class="check">
                            <input type="checkbox" id="6" name="day[6]" value="6">
                            <label for="6">الخميس</label>
                        </div>
                        <div id="time" class=" time-6">
                            <input type="hidden"  name="day[6][day_id]" value="6" >
                            <input type="text" id="from" name="day[6][start]" placeholder="من">
                            <input type="text" id="to" name="day[6][end]" placeholder="الى">
                        </div>
                    </div>
                    <div class=" workdays">
                        <div class="check">
                            <input type="checkbox" id="7" name="day[7]" value="7">
                            <label for="7">الجمعة</label>
                        </div>
                        <div id="time" class=" time-7">
                            <input type="hidden"  name="day[7][day_id]" value="7" >
                            <input type="text" id="from" name="day[7][start]" placeholder="من">
                            <input type="text" id="to" name="day[7][end]" placeholder="الى">
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="col-md-12 form-group wow fadeInUp">
                    <button type="submit" id="mySubmit" class="btn btn-send"> تأكيد </button>
                </div>
            </form>
        </div>

    </div>



@endsection


@section('scripts')

    {{--<script type="text/javascript" src="{{request()->root()}}/public/gym/js/jquery-3.2.1.min.js"></script>--}}
    {{--    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/bootstrap.min.js"></script>--}}
    <!-- <script type="text/javascript" src="js/owl.carousel.min.js"></script> -->
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/wow.min.js"></script>
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/scripts.js"></script>
    <!-- time picker scripts -->
    <script type="text/javascript" src="{{request()->root()}}/public/gym/js/jquery.ptTimeSelect.js"></script>
    <!-- map script -->

    <script>


        function initAutocomplete() {

            map = new google.maps.Map(document.getElementById('googleMap'), {

                // center: {lat:  window.lat   , lng:  window.lng   },
                center: {lat: 30.06263, lng: 31.24967},
                zoom: 15,
                mapTypeId: 'roadmap'
            });


            var marker;
            google.maps.event.addListener(map, 'click', function (event) {

                map.setZoom();
                var mylocation = event.latLng;
                map.setCenter(mylocation);


                $('#lat').val(event.latLng.lat());
                $('#lng').val(event.latLng.lng());



                codeLatLng(event.latLng.lat(), event.latLng.lng());

                setTimeout(function () {
                    if (!marker)
                        marker = new google.maps.Marker({position: mylocation, map: map});
                    else
                        marker.setPosition(mylocation);
                }, 600);

            });


            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });


            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                // var location = place.geometry.location;
                // var lat = location.lat();
                // var lng = location.lng();
                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];


                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    $('#lat').val(place.geometry.location.lat());
                    $('#lng').val(place.geometry.location.lng());
                    $('#address').val(place.formatted_address);


                });


                map.fitBounds(bounds);
            });


        }


        function showPosition(position) {

            map.setCenter({lat: position.coords.latitude, lng: position.coords.longitude});
            codeLatLng(position.coords.latitude, position.coords.longitude);


        }


        function codeLatLng(lat, lng) {

            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({
                'latLng': latlng
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        // console.log(results[1].formatted_address);
                        $("#demo").html(results[1].formatted_address);

                        $("#address").val(results[1].formatted_address);
                        $("#map").val(results[1].formatted_address);
                        $("#pac-input").val(results[1].formatted_address);

                            $('.alert').addClass('fade');





                    } else {
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        }
        $(document).ready(function() {

            $("#mySubmit").click(function () {
                var lat = $('#lat').val();
                if (lat == '' || lat === undefined || lat === null) {
                    $('.alert').removeClass('fade');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
                }
            })


            $(document).on('change', '.category', function () {
                var City_id =   $(this).val();
                var div =   $(this).parent().parent().parent();
                var op =" ";
                var showElement =  div.find('.showElement');
                $.ajax({
                    type:"Get",
                    url: "{{route('getSub')}}",
                    data: {'id': City_id},
                    success:function (data) {


                        op += '<option  disabled selected>إختر الحي </option>';
                        for (var i= 0 ; i <data.length ; i ++){
                            op += '<option value="'+ data[i].id +'">' + data[i].name + '</option>';
                        }
                        div.find('.City_chid').html(" ");
                        div.find('.City_chid').append(op);
                        if (data == null || data == undefined || data.length == 0){
                            showElement.delay(500).slideUp();
                        }else {
                            showElement.delay(500).slideDown();

                        }
                    },
                    error:function (error) {
                        console.log(error)
                    }
                })
            });



        });

        $(document).on('change','#files',function (value) {



            if (parseInt( $(".pip").length) > 0){

                swal({
                    title: "سوف يتم اخد الصورة الاولي فقط شخصية  ",
                    text: "سيتم إضافة الباقي لك في المعرض الخاص بك بحد اقصي 5 صور",
                    type: "success",
//                        showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "موافق",
                    confirmButtonClass: 'btn-success waves-effect waves-light',
                    closeOnConfirm: true,
//                        closeOnCancel: true,
                });

            }


        })

        @if(session()->has('myErrors'))
            setTimeout(function () {
            showErrors('{{ session()->get('myErrors') }}');
        }, 1000);

        @endif

        function showErrors(message) {

            var shortCutFunction = 'errors';
            var msg = message;
            var title = "خطأ";
            toastr.options = {
                positionClass: 'toast-top-center',
                onclick: null,
                showMethod: 'slideDown',
                hideMethod: "slideUp",
            };
            var $toast = toastr[shortCutFunction](msg, title);
            // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;


        }


    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOHMIfoX5CvzkfHlfeuJKZEn2EZfKZ6qc&language={{ config('app.locale') }}&&callback=initAutocomplete&libraries=places"></script>


@endsection