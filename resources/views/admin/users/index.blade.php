@extends('admin.layouts.master')

@section('title', 'المستخدمين')
@section('styles')

    <!-- Custom box css -->
    <link href="{{ request()->root() }}/assets/admin/plugins/custombox/dist/custombox.min.css" rel="stylesheet">




    <style>
        .errorValidationReason{

            border: 1px solid red;

        }
    </style>
@endsection
@section('content')

    <!-- Page-Title -->



    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            @if(request('type') == 'gym')
            <div class="btn-group pull-right m-t-15 ">
                <a href="{{ route('users.create') }}"
                   type="button" class="btn btn-custom waves-effect waves-light"
                   aria-expanded="false">
                <span class="m-l-5">
                <i class="fa fa-plus"></i>
                </span>
                    إضافة جيم جديد
                </a>
            </div>
            @endif
            <h4 class="page-title">{{$pageName}}  </h4>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <div class="dropdown pull-right">


                </div>

                <h4 class="header-title m-t-0 m-b-30">
                    {{--@lang('trans.managers_system')--}}
                </h4>

                <table id="datatable-responsive-custom" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>@lang('trans.username')</th>
                        <th>@lang('maincp.mobile_number')</th>
                        <th>@lang('trans.status' ) </th>
                        {{--<th>مفعل</th>--}}
                        <th>@lang('trans.created_at')</th>
                        <th>@lang('trans.options')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $row)
                        <tr>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>
                                <div class="StatusActive{{ $row->id }}"  style="display: {{ $row->is_suspend == 0 ? "none" : "block" }}; text-align: center;">
{{--                                    <img  width="23px" src="{{ request()->root() }}/public/assets/admin/images/false.png" alt="">--}}
                                    <img  width="23px" src="{{ asset('assets/admin/images/false.png') }}" alt="">
                                </div>
                                <div class="StatusNotActive{{ $row->id }}" style="display: {{ $row->is_suspend == 0 ? "block" : "none" }};  text-align: center;">
                                    <img width="23px" src="{{ asset('assets/admin/images/ok.png') }}" alt="">
                                </div>

                            </td>

                            <td>{{ $row->created_at != ''? @$row->created_at->format('Y/m/d'): "--" }}</td>
                            <td>


                                <a href="{{ route('users.show', $row->id) }}"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="@lang('institutioncp.show_details')"
                                   class="btn btn-icon btn-xs waves-effect  btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if($row->defined_user != 'user' && $row->is_accepted == 0 )
                                    <a href="javascript:;" data-id="{{ $row->id }}"
                                       data-toggle="tooltip" data-placement="top"
                                       data-original-title="@lang('institutioncp.show_details')"
                                       class="btn btn-icon btn-xs waves-effect accepted  btn-info">
                                        قبول الطلب
                                    </a>
                                @endif

                                @if( $row->is_suspend == 0  )
                                    <a href="#" data-id="{{ $row->id }}"
                                       data-toggle="modal" data-target="#suspendUserApp{{$row->id }}" data-placement="top"
                                       data-original-title="حظر "
                                       class="btn btn-icon btn-xs waves-effect btn-warning">
                                        <i class="fa fa-lock"></i>
                                    </a>

                                @endif
                                <a href="javascript:;" data-id="{{ $row->id }}" data-type="0"
                                   data-url="{{ route('user.suspend') }}"  style="@if($row->is_suspend == 0) display: none;  @endif"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  suspendElement suspend{{ $row->id }}"
                                   id="suspendElement" data-message="تاكيد التفعيل"
                                   data-toggle="tooltip" data-placement="top"
                                   title="" data-original-title="فك الحظر ">
                                    <i class="fa fa-unlock"></i>
                                </a>

                                    <div class="modal fade" id="suspendUserApp{{$row->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">لن يتمكن المستخدم  من اجراء اي عمليات داخل التطبيق</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="post" action="{{ route('user.suspendUser') }}" data-parsley-validate
                                                      novalidate >
                                                    {{ csrf_field() }}
                                                    <div class="modal-body">
                                                        <input type="hidden" name="id" value="{{$row->id}}">
                                                        <input type="hidden" name="type" value="-1">
                                                        <div class="form-group">
                                                            <label for="message-{{$row->id}}" class="col-form-label">سبب الحظر :</label>
                                                            <textarea rows="4" cols="50" class="form-control" id="message-text{{$row->id}}" required
                                                                      name="message"
                                                                      data-parsley-maxLength="250"
                                                                      data-parsley-maxLength-message=" السبب  يجب أن يكون 250 حروف فقط"
                                                                      data-parsley-minLength="10"
                                                                      data-parsley-minLength-message=" السبب  يجب أن يكون اكثر من 10 حروف "
                                                                      data-parsley-required-message="يجب ادخال    سبب الحظر     {{$row->name}}"></textarea>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">إالغاء</button>
                                                        <button type="submit" class="btn btn-primary">ارسال سبب الحظر   </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection


@section('scripts')

    <!-- Modal-Effect -->
    <script src="{{ request()->root() }}/assets/admin/plugins/custombox/dist/custombox.min.js"></script>
    <script src="{{ request()->root() }}/assets/admin/plugins/custombox/dist/legacy.min.js"></script>


    <script>


            $(document).ready(function () {
                $('#datatable-responsive-custom').DataTable( {
                    "order": [[ 4, "desc" ]]
                } );

            });


        $('body').on('click', '.accepted', function () {
            var id = $(this).attr('data-id');
            swal({
                title: "قبول الطلب؟",
                text: "",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-success waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $('.loading').show();
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('user.accepted') }}',
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {
                            $('.loading').hide();
                            if (data) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت القبول  بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-center',
                                    onclick: null,
                                    showMethod: 'slideDown',
                                    hideMethod: "slideUp",
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;
                                setTimeout(function(){// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                                }, 1200);
                            }

                        }
                    });
                } else {

                }
            });
        });



        function reAccepted(id) {


            swal({
                title: "هل تريد إعادة  مزود الخدمة ؟",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: false,
                closeOnCancel: true,
            }, function (isConfirm) {

                if (isConfirm) {

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('user.reAcceptUser') }}',
                        beforeSend: function () {
                            $('.loading').show();
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {  id : id  },
                        dataType: 'json',
                        success: function (data) {
                            $('.loading').hide();
                            if (data) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت إعادة القبول  بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-center',
                                    onclick: null,
                                    showMethod: 'slideDown',
                                    hideMethod: "slideUp",
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;
                                setTimeout(function(){// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                                }, 1200);
                            }

                        }
                    });
                } else {

                }
            });
        }
    </script>


@endsection



