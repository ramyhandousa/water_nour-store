@extends('admin.layouts.master')
@section('title', 'الصفحة الرئيسية')


@section('content')

    @can('statistics_manage')
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">@lang('maincp.control_panel')</h4>
        </div>
    </div>




        <div class="row statistics">

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> @lang('trans.clients')</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{\App\User::whereDefinedUser('user')->count()}}</h2>
                            <p class="text-muted m-b-0">عدد المستخدمين المسجلين في التطبيق  </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> الوكلاء</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{\App\User::whereDefinedUser('agent')->count()}}</h2>
                            <p class="text-muted m-b-0">عدد الوكلاء المسجلين في التطبيق  </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> المناديب</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{\App\User::whereDefinedUser('delivery')->count()}}</h2>
                            <p class="text-muted m-b-0">عدد الوكلاء المسجلين في التطبيق  </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> المنتجات</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{\App\Models\Product::where('is_deleted',0)->count()}}</h2>
                            <p class="text-muted m-b-0">عدد المنتجات  التطبيق  </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

            <div class="col-lg-3 col-md-6">
                <a href="javascript:;">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"> عدد العروض المضافة في النظام     </h4>

                        <div class="widget-box-2">
                            <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                            class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                                <h2 class="m-b-0">{{ \App\Models\Offer::where('end_at','>',now())->count()}}</h2>
                                <p class="text-muted m-b-0">عددالعروض  المضافة في النظام  </p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6">
                <a href="javascript:;">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"> عدد الأقسام المضافة في النظام     </h4>

                        <div class="widget-box-2">
                            <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                            class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                                <h2 class="m-b-0">{{\App\Models\Category::where('is_deleted',0)->count()}}</h2>
                                <p class="text-muted m-b-0">عدد الأقسام  المضافة في النظام  </p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6">
                <a href="javascript:;">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"> عدد السيارات المضافة في النظام     </h4>

                        <div class="widget-box-2">
                            <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                            class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                                <h2 class="m-b-0">{{\App\Models\CarType::count()}}</h2>
                                <p class="text-muted m-b-0">عدد السيارات  المضافة في النظام  </p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <!-- end col -->


        <!-- end col -->
    </div>

    @else
     <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">مرحبا بك في تطبيق الجيم</h4>
        </div>
    </div>

    @endcan


@endsection
