@extends('admin.layouts.master')
@section('title' , 'إضافة مستخدم')

@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }


    </style>
@endsection
@section('content')
    <form id="myForm" method="POST" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data" data-parsley-validate
          novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة المنتجات</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> تعديل منتج  {{$product->name_ar}}</h4>

                    <div class="col-xs-6">
                        <div class="form-group ">
                            <label for="passWord2">الماركة *</label>
                            <select   class="form-control  "  name="brand_id"
                                      required  data-parsley-required-message="من فضلك اختار الماركة"  >
                                <option value="" disabled selected hidden class="text-white">إختر </option>
                                @foreach($brands as  $value)
                                    <option value="{{ $value->id }}" @if($value->id == $product->brand_id) selected @endif >{{ $value->name_ar }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group ">
                            <label for="passWord2">إختار قسم المنتج *</label>
                            <select   class="form-control  "  name="category_id"
                                      required  data-parsley-required-message="من فضلك اختار قسم المنتج"  >
                                <option value="" disabled selected hidden class="text-white">إختر </option>
                                @foreach($categories as  $value)
                                    <option value="{{ $value->id }}" @if($value->id == $product->category_id) selected @endif>{{ $value->name_ar }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName"> {{$pageName}}   باللغة العربية</label>
                            <input type="text" name="name_ar" value="{{$product->name_ar}}"
                                   class="form-control requiredFieldWithMaxLenght"
                                   required
                                   placeholder="  {{$pageName}} باللغة العربية  ..."/>
                            <p class="help-block" id="error_userName"></p>
                            @if($errors->has('name_ar'))
                                <p class="help-block">
                                    {{ $errors->first('name_ar') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName"> {{$pageName}}   باللغة الإنجليزية</label>
                            <input type="text" name="name_en" value="{{$product->name_en}}"
                                   class="form-control requiredFieldWithMaxLenght"
                                   required
                                   placeholder="  {{$pageName}}   باللغة الإنجليزية  ..."/>
                            <p class="help-block" id="error_userName"></p>
                            @if($errors->has('name_en'))
                                <p class="help-block">
                                    {{ $errors->first('name_en') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="desc">    وصف  باللغة العربية  </label>
                            <textarea type="text" name="desc_ar" class="form-control m-input " required
                                      placeholder="إدخل  وصف     "   >{{$product->desc_ar}}</textarea>
                            <p class="help-block" id="error_userName"></p>
                            @if($errors->has('desc'))
                                <p class="help-block">
                                    {{ $errors->first('desc') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="desc">    وصف  بالإنجليزي  </label>
                            <textarea type="text" name="desc_en" class="form-control m-input " required
                                      placeholder="إدخل  وصف     "   >{{$product->desc_en}}</textarea>
                            <p class="help-block" id="error_userName"></p>
                            @if($errors->has('desc'))
                                <p class="help-block">
                                    {{ $errors->first('desc') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <br>



                    <div class="form-group text-right m-t-20">
                        <button id="hiddenButton" class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            حفظ البيانات
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            إلغاء
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

            <div class="col-lg-4">
                <div class="card-box" style="overflow: hidden;">
                    <h4 class="header-title m-t-0 m-b-30">صورة المنتج </h4>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="hidden" value="{{ $product->getFirstMediaUrl() }}" name="oldImage"/>
                            <input data-parsley-fileextension='jpg,png' id="image" type="file" accept='image/*'
                                   name="image" class="dropify" data-max-file-size="6M"
                                   data-default-file="{{ $product->getFirstMediaUrl() }}"/>

                        </div>
                    </div>

                    <span class="help-block">
	                	<strong hidden id='error' style="color: red;">الصورة يجب ان تكون بصيغة PNG او JPG</strong>
	            	</span>


                </div>
            </div><!-- end col -->
        </div>
        <!-- end row -->
    </form>
@endsection

@section('scripts')

    <script>

        function validImages() {

            var images = $( "input[name='image']" ).val();
            var oldImage = $( "input[name='oldImage']" ).val();

            if ( (images === undefined || images === "" ) && (oldImage === undefined || oldImage === "")   ) {
                var shortCutFunction = 'error';
                var msg = 'من فضلك إختار   صورة واحدة علي الأقل  ';
                var title = 'نجاح';
                toastr.options = {
                    positionClass: 'toast-top-left',
                    preventDuplicates: true,
                    onclick: null
                };
                $toastlast = toastr[shortCutFunction](msg, title);
                return false;
            }

            return true;
        }

        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid() && validImages()){
                // if (form.parsley().isValid()  ){
                $('.loading').show();

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                        messageDisplay( 'نجاح' ,data.message )
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 2000);
                    },
                    error: function (data) {
                        $('.loading').hide();
                        errorMessageTostar('فشل',data.responseJSON.error[0]);
                    }
                });
            }else {

            }
        });

        function messageDisplay($title, $message) {
            var shortCutFunction = 'success';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            $toastlast = toastr[shortCutFunction](msg, title);
        }

        function errorMessageTostar($title, $message) {
            var shortCutFunction = 'error';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            var $toast = toastr[shortCutFunction](msg, title);
            $toastlast = $toast;
        }

    </script>


@endsection
