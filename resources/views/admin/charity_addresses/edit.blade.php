@extends('admin.layouts.master')
@section('title', __('maincp.users_manager'))


@section('styles')



@endsection
@section('content')


    <form method="POST" action="{{ route('charity_addresses.update', $charityAddress->id) }}  " enctype="multipart/form-data" data-parsley-validate novalidate>
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            من فضلك إختار موقعك من علي الخريطة
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة {{$pageName}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-7  ">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30"> إضافة {{$pageName}}</h4>

                    <div class="row">



                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">    المدينة*</label>
                                <select name="city_id" id="" class="form-control requiredFieldWithMaxLenght" required>
                                    <option value="" selected disabled=""> إختر المدينة</option>
                                    @foreach($cities as $value)
                                        <option value="{{ $value->id }}"  @if($value->id == $charityAddress->city_id) selected @endif>{{ $value->name_ar }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>



                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="desc">    وصف    </label>
                                <textarea type="text" name="desc_ar" class="form-control m-input requiredFieldWithMaxLenght" required
                                          placeholder="إدخل  وصف     "   >{{$charityAddress->desc_ar}}</textarea>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('desc'))
                                    <p class="help-block">
                                        {{ $errors->first('desc') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <br>


                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="desc">    وصف بالإنجليزي   </label>
                                <textarea type="text" name="desc_en" class="form-control m-input requiredFieldWithMaxLenght" required
                                          placeholder="إدخل  وصف     "   >{{$charityAddress->desc_en}}</textarea>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('desc'))
                                    <p class="help-block">
                                        {{ $errors->first('desc') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <br>



                    </div>


                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

            <div class="col-lg-5">
                <div class="col-12">
                    <label>تحديد موقع العمل الخيري</label>

                    <input id="pac-input" name="address_search"
                           class="controls " value="{{$charityAddress->address}}"
                           type="text"  style="z-index: 1; position: absolute;  top: 10px !important;
		    left: 197px; height: 40px;   width: 63%;   border: none;  box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; padding: 0 1em;   color: rgb(86, 86, 86);
		    font-family: Roboto, Arial, sans-serif;   user-select: none;  font-size: 18px;   margin: 10px;"  placeholder="بحث"  >
                    <input type="hidden" name="latitude"  value="{{$charityAddress->latitude}}" id="lat"/>
                    <input type="hidden" name="longitude" value="{{$charityAddress->longitude}}" id="lng"/>
                    <input type="hidden" name="address"   value="{{$charityAddress->address}}" id="address"/>
                    <div id="googleMap" width="100%" height="300" style="height: 300px;"></div>

                </div>
            </div>

        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')
    <script type="text/javascript"
            src="{{ request()->root() }}/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">

        function validImages() {

            var images = $( "input[name='images[]']" );


            console.log(images)
            if ( images === undefined || images == ""   ) {
                var shortCutFunction = 'error';
                var msg = 'من فضلك إختار   صورة واحدة علي الأقل  ';
                var title = 'نجاح';
                toastr.options = {
                    positionClass: 'toast-top-left',
                    preventDuplicates: true,
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title);
                $toastlast = $toast;
                return false;
            }

            return true;
        }


        function  vaildLocation(){
            var lat = $('#lat').val();
            if (lat === '' || lat === undefined || lat === null) {
                $('.alert').removeClass('fade');
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }
            return  true;
        }


        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            // if (form.parsley().isValid() && validImages()){
            if (form.parsley().isValid()  && vaildLocation() ){
                console.log('yes cliced')
                $('.loading').show();

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    },
                    error: function (data) {
                        $('.loading').hide();

                        var shortCutFunction = 'error';
                        var msg = data.responseJSON.error;
                        var title = 'فشل';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                    }
                });
            }else {
                $('.loading').hide();
            }
        });


        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var file = $(this).attr('data-name');
            swal({
                title: "هل انت متأكد؟",
                text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: id , file: file},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status == true) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                                setTimeout(function () {
                                    location.reload();
                                }, 1000);
                            }

                        }
                    });
                }
            });
        });



    </script>

    <script>
        function initAutocomplete() {

            map = new google.maps.Map(document.getElementById('googleMap'), {

                // center: {lat:  window.lat   , lng:  window.lng   },
                center: {lat: 24.774265, lng: 46.738586},
                zoom: 15,
                mapTypeId: 'roadmap'
            });


            var marker;
            google.maps.event.addListener(map, 'click', function (event) {

                map.setZoom();
                var mylocation = event.latLng;
                map.setCenter(mylocation);


                $('#lat').val(event.latLng.lat());
                $('#lng').val(event.latLng.lng());



                codeLatLng(event.latLng.lat(), event.latLng.lng());

                setTimeout(function () {
                    if (!marker)
                        marker = new google.maps.Marker({position: mylocation, map: map});
                    else
                        marker.setPosition(mylocation);
                }, 600);

            });


            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });


            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                // var location = place.geometry.location;
                // var lat = location.lat();
                // var lng = location.lng();
                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];


                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    $('#lat').val(place.geometry.location.lat());
                    $('#lng').val(place.geometry.location.lng());
                    $('#address').val(place.formatted_address);


                });


                map.fitBounds(bounds);
            });


        }


        function showPosition(position) {

            map.setCenter({lat: position.coords.latitude, lng: position.coords.longitude});
            codeLatLng(position.coords.latitude, position.coords.longitude);


        }


        function codeLatLng(lat, lng) {

            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({
                'latLng': latlng
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        // console.log(results[1].formatted_address);
                        $("#demo").html(results[1].formatted_address);

                        $("#address").val(results[1].formatted_address);
                        $("#map").val(results[1].formatted_address);
                        $("#pac-input").val(results[1].formatted_address);

                        $('.alert').addClass('fade');





                    } else {
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrQCgni07QfWbo-CFd7BiJbReWGNERGok&language=ar&&callback=initAutocomplete&libraries=places"></script>

@endsection

