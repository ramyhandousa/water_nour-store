@extends('admin.layouts.master')

@section('title', 'المستخدمين')
@section('styles')

    <!-- Custom box css -->
    <link href="{{ request()->root() }}/assets/admin/plugins/custombox/dist/custombox.min.css" rel="stylesheet">




    <style>
        .errorValidationReason{

            border: 1px solid red;

        }
    </style>
@endsection
@section('content')

    <!-- Page-Title -->



    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12" style="margin-top: 15px">

            <div class="col-lg-6 col-md-6">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> رصديك في الطلبات     </h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                        <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{   $admin_price}}</h2>
{{--                            <p class="text-muted m-b-0">عدد مزودي الخدمات   المضافة في النظام  </p>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> رصديك التاجر     </h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                        <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{  $agent_price }}</h2>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <div class="dropdown pull-right">
                    <a href="{{ route('admin.invoices_orders_excel', $user->id) }}"
                       data-toggle="tooltip" data-placement="top"
                       class="btn btn-icon btn-xs waves-effect  btn-info">
                        Excel
                    </a>

                </div>

                <h4 class="header-title m-t-0 m-b-30">
                    {{--@lang('trans.managers_system')--}}
                </h4>

                <table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>رقم الطلب </th>
                        <th>إسم الشخص</th>
{{--                        <th>إسم المندرب</th>--}}
                        <th> اليوم</th>
                        <th> قسم خيري  </th>
                        <th>   الضريبة المضافة  </th>
                        <th>    سعر  التوصيل  </th>
                        <th>   سعر الطلبات     </th>
                        <th>    قيمة الخصم    </th>
                        <th>   السعر  النهائي  </th>
                        <th>   حساب الأدمن  للطلب  </th>
                        <th>   حساب التاجر  للطلب  </th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($reports as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->user->name }}</td>
{{--                            <td>{{ $row->delivery->name }}</td>--}}
{{--                            <td>{{ $row->day->name_ar }}</td>--}}
                            <td style="width: 10%">{{ \Carbon\Carbon::parse($row->created_at)->format('Y-m-d') }}</td>
                            <td>{{ $row->is_charity ? "نعم" : "لا" }}</td>
                            <td>{{ $row->tax }}</td>
                            <td>{{ $row->delivery_price }}</td>
                            <td>{{ $row->sum_price }}</td>
                            <td>{{ $row->promo_code ? $row->discount_code : "لا يوجد خصم" }}</td>
                            <td>{{ $row->total_price }}</td>
                            <td>{{ $row->admin_price }}</td>
                            <td>{{ $row->agent_price }}</td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection


@section('scripts')


@endsection



