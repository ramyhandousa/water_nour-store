@extends('admin.layouts.master')
@section('title' , 'إضافة مستخدم')

@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }


    </style>
@endsection
@section('content')
    <form id="myForm" method="POST" action="{{ route('agent_products.update', $product->id) }}" enctype="multipart/form-data" data-parsley-validate
          novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة المنتجات</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> تعديل منتج   </h4>

                    <div class="col-xs-6">
                        <div class="form-group ">
                            <label for="passWord2">المنتج *</label>
                            <select   class="form-control  product_change"  name="product_id"
                                      required  data-parsley-required-message="من فضلك اختار المنتج"  >
                                <option value="" disabled selected hidden class="text-white">إختر </option>
                                @foreach($products as  $value)
                                    <option value="{{ $value->id }}" @if($value->id == $product->product_id) selected @endif " >{{ $value->name_ar }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName">(price)  سعر المنتج  *</label>
                            <input type="number" name="price" value="{{$product->price}}" min=1 maxlength="6"
                                   oninput="validity.valid||(value='');" class="form-control " required>

                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName">(quantity) الكمية المتوفرة من المنتج  *</label>
                            <input type="number" name="quantity" value="{{$product->quantity}}" min=1 maxlength="6"
                                   oninput="validity.valid||(value='');"  class="form-control " required>

                        </div>
                    </div>

                    <br>



                    <div class="form-group text-right m-t-20">
                        <button id="hiddenButton" class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            حفظ البيانات
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            إلغاء
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

            <div class="col-lg-4" id="renderHtml">
                <div class="card-box" style="overflow: hidden;">
                    <h4 class="header-title m-t-0 m-b-30">صورة المنتج </h4>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="hidden" value="{{ \App\Models\Product::find($product->product_id)->getFirstMediaUrl() }}" name="oldImage"/>
                            <input data-parsley-fileextension='jpg,png' id="image" type="file" disabled accept='image/*'
                                   name="image" class="dropify" data-max-file-size="6M"
                                   data-default-file="{{ \App\Models\Product::find($product->product_id)->getFirstMediaUrl() }}"/>

                        </div>
                    </div>

                    <span class="help-block">
	                	<strong hidden id='error' style="color: red;">الصورة يجب ان تكون بصيغة PNG او JPG</strong>
	            	</span>


                </div>
            </div><!-- end col -->
        </div>
        <!-- end row -->
    </form>
@endsection

@section('scripts')

    <script>

        function validImages() {

            var images = $( "input[name='oldImage']" ).val();


            if ( images === undefined || images === ""   ) {
                var shortCutFunction = 'error';
                var msg = 'من فضلك إختار   صورة واحدة علي الأقل  ';
                var title = 'نجاح';
                toastr.options = {
                    positionClass: 'toast-top-left',
                    preventDuplicates: true,
                    onclick: null
                };
                $toastlast = toastr[shortCutFunction](msg, title);
                return false;
            }

            return true;
        }

        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            // if (form.parsley().isValid() && validImages()){
            if (form.parsley().isValid()  ){
                $('.loading').show();

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                        messageDisplay( 'نجاح' ,data.message )
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 2000);
                    },
                    error: function (data) {
                        $('.loading').hide();
                        errorMessageTostar('فشل',data.responseJSON.error[0]);
                    }
                });
            }else {

            }
        });

        function messageDisplay($title, $message) {
            var shortCutFunction = 'success';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            $toastlast = toastr[shortCutFunction](msg, title);
        }

        function errorMessageTostar($title, $message) {
            var shortCutFunction = 'error';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            var $toast = toastr[shortCutFunction](msg, title);
            $toastlast = $toast;
        }


        $('.product_change').on('change', function (e) {
            var product_id  =   $(this).val();

            $('.loading').show();

            $.ajax({
                type: 'POST',
                url: "{{ route('agent_products.renderProductData') }}",
                data: {product_id : product_id},
                datatype:'json',
                success: function (data) {
                    $('.loading').hide();
                    $("#renderHtml").html(data.html)

                },
                error: function (data) {
                    $('.loading').hide();
                    errorMessageTostar('نعتذر', 'للاسف يوجد خطا ما ')
                }
            });

        });

    </script>


@endsection
