
<div class="card"  >
    <img src="{{$product->getFirstMediaUrl() }}" style="max-height: 250px;max-width: 250px" class="card-img-top" alt="...">
    <div class="card-body card">
        <h5 class="card-title"> إسم المنتج : {{$product->name_ar}}</h5>
        <p class="card-text"> وصف المنتج : {{$product->desc_ar}}</p>
        <p class="card-text"> إسم القسم : {{optional($product->category)->name_ar}}</p>
        <p class="card-text"> إسم الماركة :{{optional($product->brand)->name_ar}}</p>
    </div>
</div>
