@extends('admin.layouts.master')
@section('title', __('maincp.user_data'))
@section('content')


    <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}



    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    {{--<button type="button" class="btn btn-custom  waves-effect waves-light"--}}
                        {{--onclick="window.history.back();return false;"> @lang('maincp.back') <span class="m-l-5"><i--}}
                                {{--class="fa fa-reply"></i></span>--}}
                    {{--</button>--}}

                    <a href="{{ route('deliveries.index') }}"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>

                </div>
                <h4 class="page-title">@lang('maincp.user_data') </h4>
            </div>
        </div>


        <div class="row">


                <div class="col-sm-12">

                <div class="card-box">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-b-0">


                                <h4 class="header-title m-t-0 m-b-30">التفاصيل</h4>

                                <form>
                                    <div id="basicwizard" class=" pull-in">
                                        <ul class="nav nav-tabs navtab-wizard nav-justified bg-muted">
                                            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">البيانات الاساسية</a></li>
                                            @if($user->profile)
                                                <li ><a href="#tab2" data-toggle="tab" aria-expanded="false"> الفرعية  </a></li>
                                            @endif
                                        </ul>
                                        <div class="tab-content b-0 m-b-0">
                                            <div class="tab-pane m-t-10 fade active in" id="tab1">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <h4>@lang('maincp.personal_data')</h4>
                                                            <hr>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>@lang('maincp.full_name') :</label>
                                                            <input class="form-control" value="{{ $user->name }}"><br>
                                                        </div>

                                                        @if(  $user->phone )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.mobile_number') :</label>
                                                                <input class="form-control" value="{{ $user->phone }}"><br>
                                                            </div>
                                                        @endif

                                                        @if( $user->email )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.e_mail')  :</label>
                                                                <input class="form-control" value="{{ $user->email }}"><br>
                                                            </div>
                                                        @endif

                                                        @if( $user->city )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>المدينة  :</label>
                                                                <input class="form-control" value="{{ optional( $user->city)->name_ar }}"><br>
                                                            </div>
                                                        @endif

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>العنوان  :</label>
                                                            <input class="form-control" value="{{  $user->address }}"><br>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>السيارة  :</label>
                                                            <input class="form-control" value="{{ optional(optional( $user->profile)->car)->name_ar }}"><br>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>رقم اللوحة  :</label>
                                                            <input class="form-control" value="{{ optional( $user->profile)->plate_number }}"><br>
                                                        </div>


                                                    </div>



                                                    <div class="col-sm-4">
                                                        <div class="card-box">
                                                            <div class="row">

                                                                <div class="card-box" style="overflow: hidden;">
                                                                    <h4 class="header-title m-t-0 m-b-30">@lang('institutioncp.personal_image')</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage( $image_profile, request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( $image_profile, request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            @if($user->profile)

                                                <div class="tab-pane m-t-10 fade  in" id="tab2">
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="col-xs-12 col-lg-12">
                                                                <h4>البيانات الفرعية</h4>
                                                                <hr>
                                                            </div>

                                                                <label>     الصور المرفقة :</label>
                                                                <div class="card-box" style="overflow: hidden;">

                                                                    <div class="form-group">
                                                                        <h4 class="header-title m-t-0 m-b-30">صورة الهوية</h4>
                                                                        <div class="col-sm-3">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage( $hobby_picture, request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( $hobby_picture, request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <h4 class="header-title m-t-0 m-b-30">صورة الرخصة</h4>
                                                                        <div class="col-sm-3">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage($license, request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( $license , request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <h4 class="header-title m-t-0 m-b-30">صورة الإستمارة</h4>
                                                                        <div class="col-sm-3">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage( $vehicle_investment , request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( $vehicle_investment , request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

        </div>


    </form>

@endsection

