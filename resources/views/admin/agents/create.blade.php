@extends('admin.layouts.master')
@section('title' , 'إضافة مستخدم')

@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }


    </style>
@endsection
@section('content')
    <form id="myForm" method="POST" action="{{ route('agents.store') }}" enctype="multipart/form-data" data-parsley-validate
          novalidate>
    {{ csrf_field() }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة الوكلاء</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">إضافة وكيل</h4>

                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="userName">الاسم الأول*</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control"
                                   required
                                   placeholder="اسم المستخدم  ..."
                                   data-parsley-maxLength="20"
                                   data-parsley-maxLength-message=" الاسم  يجب أن يكون عشرون حروف فقط"
                                   data-parsley-minLength="3"
                                   data-parsley-minLength-message=" الاسم  يجب أن يكون اكثر من 3 حروف "
                                   data-parsley-required-message="يجب ادخال  اسم المستخدم"

                            />
                            <p class="help-block" id="error_userName"></p>

                        </div>

                    </div>

                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="userPhone">رقم الجوال*</label>
                            <input type="number" name="phone" value="{{ old('phone') }}" class="form-control"
                                   required
                                   data-parsley-maxLength="20"
                                   data-parsley-maxLength-message=" الاسم  يجب أن يكون عشرون حروف فقط"
                                   data-parsley-minLength="5"
                                   data-parsley-minLength-message=" الاسم  يجب أن يكون اكثر من 5 حروف "
                                   data-parsley-required-message="يجب ادخال رقم الجوال"
                                   placeholder="رقم الجوال..."
                            />
                            @if($errors->has('phone'))
                                <p class="help-block">
                                    {{ $errors->first('phone') }}
                                </p>
                            @endif
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="emailAddress">البريد الإلكتروني*</label>

                            <input type="email" name="email" parsley-trigger="change" value="{{ old('email') }}"
                                   class="form-control"
                                   placeholder="البريد الإلكتروني..." required
                                   data-parsley-type="email"
                                   data-parsley-type-message="أدخل البريد الالكتروني بطريقة صحيحة"
                                   data-parsley-required-message="يجب ادخال  البريد الالكتروني"
                                   data-parsley-maxLength="30"
                                   data-parsley-maxLength-message=" البريد الالكتروني  يجب أن يكون ثلاثون حرف فقط"
                                   {{--data-parsley-pattern="/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm"--}}
                                   {{--data-parsley-pattern-message="أدخل  البريد الالكتروني بطريقة الايميل ومن غير مسافات"--}}
                                   required
                            />
                            @if($errors->has('email'))
                                <p class="help-block">{{ $errors->first('email') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="pass1">كلمة المرور*</label>
                            <input type="password" name="password" id="pass1" value="{{ old('password') }}"
                                   class="form-control"
                                   placeholder="كلمة المرور..."
                                   required
                                   data-parsley-required-message="هذا الحقل مطلوب"
                                   data-parsley-maxLength="20"
                                   data-parsley-maxLength-message=" الباسورد  يجب أن يكون عشرون حروف فقط"
                                   data-parsley-minLength="5"
                                   data-parsley-minLength-message=" الباسورد  يجب أن يكون اكثر من 5 حروف "
                            />

                            @if($errors->has('password'))
                                <p class="help-block">{{ $errors->first('password') }}</p>
                            @endif

                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="passWord2">تأكيد كلمة المرور*</label>
                            <input data-parsley-equalto="#pass1" name="password_confirmation" type="password" required
                                   placeholder="تأكيد كلمة المرور..." class="form-control" id="passWord2"
                                   data-parsley-required-message="تأكيد كلمة المرور مطلوب"
                                   data-parsley-maxlength="55"
                                   data-parsley-minlength="8"
                                   data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (55) حرف"
                                   data-parsley-minlength-message=" أقل عدد الحروف المسموح بها هى (8) حرف"
                                   data-parsley-equalto="#pass1"
                                   data-parsley-equalto-message ='غير مطابقة لكلمة المرور'
                            >
                            @if($errors->has('password_confirmation'))
                                <p class="help-block">{{ $errors->first('password_confirmation') }}</p>
                            @endif


                        </div>
                    </div>


                    <div class="col-xs-6">
{{--                        <div class="form-group ">--}}
{{--                            <label for="passWord2">الدولة *</label>--}}
{{--                            <select   class="form-control  "  name="city_id"--}}
{{--                                      required  data-parsley-required-message="من فضلك اختار الدولة"  >--}}
{{--                                <option value="" disabled selected hidden class="text-white">إختر </option>--}}
{{--                                @foreach($cities as  $value)--}}
{{--                                    <option value="{{ $value->id }}" >{{ $value->name_ar }}</option>--}}
{{--                                @endforeach--}}

{{--                            </select>--}}
{{--                        </div>--}}

                        <div class="form-group{{ $errors->has('cities') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">  المدينة</label>
                            <div class="col-sm-6">
                                @foreach($cities as  $value)
                                    <div class="checkbox checkbox-pink">
                                        <input id="checkbox{{$value->id}}" type="checkbox"  value="{{$value->id}}" name="cities[]"  >
                                        <label for="checkbox{{$value->id}}"> {{$value->name_ar}} </label>
                                    </div>
                                @endforeach
                                @if($errors->has('cities'))
                                    <p class="help-block">{{ $errors->first('cities') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>

                        <div class="col-xs-12{{ $errors->has('address') ? ' has-error' : '' }}">
                            <div class="form-group">
                                <label>تحديد موقع الوكيل</label>
                                <input id="pac-input" name="address_search"
                                       class="controls " value="{{old('address_search')}}"
                                       type="text"  style="z-index: 1; position: absolute;  top: 10px !important;
                left: 197px; height: 40px;   width: 63%;   border: none;  box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; padding: 0 1em;   color: rgb(86, 86, 86);
                font-family: Roboto, Arial, sans-serif;   user-select: none;  font-size: 18px;   margin: 10px;"  placeholder="بحث"  >
                                <input type="hidden" name="latitude" value="{{old('latitude')}}" id="lat"/>
                                <input type="hidden" name="longitude" value="{{old('longitude')}}" id="lng"/>
                                <input type="hidden" name="address" value="{{old('address')}}" id="address"/>
                                <div id="googleMap" width="100%" height="300" style="height: 300px;"></div>
                                @if($errors->has('address'))
                                    <p class="help-block">{{ $errors->first('address') }}</p>
                                @endif

                            </div>
                        </div>

                    <div class="form-group text-right m-t-20">
                        <button id="hiddenButton" class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            حفظ البيانات
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            إلغاء
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

            <div class="col-lg-4">
                <div class="card-box" style="overflow: hidden;">
                    <h4 class="header-title m-t-0 m-b-30">الصورة الشخصية</h4>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input data-parsley-fileextension='jpg,png' id="image" type="file" accept='image/*' name="image" class="dropify" data-max-file-size="6M"/>

                        </div>
                    </div>

                    <span class="help-block">
	                	<strong hidden id='error' style="color: red;">الصورة يجب ان تكون بصيغة PNG او JPG</strong>
	            	</span>


                </div>
            </div><!-- end col -->
        </div>
        <!-- end row -->
    </form>
@endsection

@section('scripts')

    <script>

        $(document).ready(function() {


        });


    </script>

    <script>
        function initAutocomplete() {

            map = new google.maps.Map(document.getElementById('googleMap'), {

                // center: {lat:  window.lat   , lng:  window.lng   },
                center: {lat: 24.774265, lng: 46.738586},
                zoom: 15,
                mapTypeId: 'roadmap'
            });


            var marker;
            google.maps.event.addListener(map, 'click', function (event) {

                map.setZoom();
                var mylocation = event.latLng;
                map.setCenter(mylocation);


                $('#lat').val(event.latLng.lat());
                $('#lng').val(event.latLng.lng());



                codeLatLng(event.latLng.lat(), event.latLng.lng());

                setTimeout(function () {
                    if (!marker)
                        marker = new google.maps.Marker({position: mylocation, map: map});
                    else
                        marker.setPosition(mylocation);
                }, 600);

            });


            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });


            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                // var location = place.geometry.location;
                // var lat = location.lat();
                // var lng = location.lng();
                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];


                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    $('#lat').val(place.geometry.location.lat());
                    $('#lng').val(place.geometry.location.lng());
                    $('#address').val(place.formatted_address);


                });


                map.fitBounds(bounds);
            });


        }


        function showPosition(position) {

            map.setCenter({lat: position.coords.latitude, lng: position.coords.longitude});
            codeLatLng(position.coords.latitude, position.coords.longitude);


        }


        function codeLatLng(lat, lng) {

            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({
                'latLng': latlng
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        // console.log(results[1].formatted_address);
                        $("#demo").html(results[1].formatted_address);

                        $("#address").val(results[1].formatted_address);
                        $("#map").val(results[1].formatted_address);
                        $("#pac-input").val(results[1].formatted_address);

                        $('.alert').addClass('fade');





                    } else {
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrQCgni07QfWbo-CFd7BiJbReWGNERGok&language=ar&&callback=initAutocomplete&libraries=places"></script>

@endsection
