@extends('admin.layouts.master')
@section('title', 'إدارة الإشتراكات ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">


            </div>
            <h4 class="page-title"> قائمة تقارير   {{$pageName}} </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box rotateOutUpRight ">

                <table id="datatable-responsive" class="table  table-striped">
                    <thead>
                    <tr>
                        <th>رقم الإشتراك</th>
                        <th>اسم التاجر  </th>
                        <th>تاريخ   الإشتراك </th>
                        <th>      تفاصيل    </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($subscriptions as $report)

                        <tr>

                            <td> {{$report->id}}  </td>
                            <td>  {{  optional( $report->user)->name}} </td>
                            <td>  {{ $report->created_at ? $report->created_at->format('Y-m-d') :" لا يوجد تاريخ  " }} </td>
                            <td>
                                <button class="btn btn-primary waves-effect waves-light"
                                        data-toggle="modal"
                                        data-target="#tabs-modal{{$report->id}}">مشاهدة</button>

                            </td>

                            <div id="tabs-modal{{$report->id}}" class="modal fade" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content p-0">
                                        <ul class="nav nav-tabs navtab-bg nav-justified">
                                            <li class="active">
                                                <a href="#home-2{{$report->id}}" data-toggle="tab" aria-expanded="false">
                                                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                                                    <span class="hidden-xs">المعلومات الأساسية</span>
                                                </a>
                                            </li>

                                            <li class="">
                                                <a href="#messages-2{{$report->id}}" data-toggle="tab" aria-expanded="true">
                                                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                                    <span class="hidden-xs">الحسابات</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="home-2{{$report->id}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-1" class="control-label">إسم التاجر  </label>
                                                            <input type="text"  value="{{  optional( $report->user)->name}}" class="form-control"  readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-1" class="control-label">رقم التاجر  </label>
                                                            <input type="text"  value="{{  optional( $report->user)->phone}}" class="form-control"  readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-1" class="control-label">إيميل التاجر  </label>
                                                            <input type="text"  value="{{  optional( $report->user)->email}}" class="form-control"  readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane " id="messages-2{{$report->id}}">
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تاريخ بداية  الإشتراك  </label>
                                                            <input type="text"  value="{{   $report->created_at ? $report->start_at->format('Y-m-d') :" لا يوجد تاريخ  " }}"  class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تاريخ نهاية الإشتراك    </label>
                                                            <input type="text"  value="{{   $report->created_at ? $report->end_at->format('Y-m-d') :" لا يوجد تاريخ  " }}"  class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">متبقي كام يوم علي  نهاية الإشتراك    </label>
                                                            <input type="text"  value="{{  optional($report->user)->remain_subscription()}}"  class="form-control" readonly>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->


                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('#datatable-responsive').DataTable( {
                "order": [[ 2, "desc" ]]
            } );

        });
    </script>


@endsection

