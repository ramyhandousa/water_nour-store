<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {

    return [
        'car_type_id' => rand(1,4),
        'plate_number' => \Illuminate\Support\Str::random(10),
    ];

});
