<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\User::class, 1)->create()->each(function ($user) {

            $image_profile = "https://mostaql.hsoubcdn.com/uploads/357227-SgktG-1515107806-%D9%85%D9%86%D8%AF%D9%88%D8%A8-%D8%AA%D9%88%D8%B5%D9%8A%D9%84.jpg";
            $user->addMediaFromUrl($image_profile)->withCustomProperties(['image' => 'profile'])->toMediaCollection();

//            $hobby_picture  = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLoD_gRmnFojHhqOhNLum3jjKzQ-2jgEZDYA&usqp=CAU";
//
//            $license = "https://m.jamalouki.net/uploads//richTextEditor/default_richTextEditor/5b78cbd2710f667fe4e2bc2713363edd.jpeg";
//
//            $user->profile()->save(factory(\App\Models\Profile::class)->make());
//
//
//            $user->profile->addMediaFromUrl($license)->withCustomProperties(['image' => 'vehicle_investment'])->toMediaCollection();
//            $user->profile->addMediaFromUrl($image_profile)->withCustomProperties(['image' => 'profile'])->toMediaCollection();
//            $user->profile->addMediaFromUrl($hobby_picture)->withCustomProperties(['image' => 'hobby_picture'])->toMediaCollection();
//            $user->profile->addMediaFromUrl($license)->withCustomProperties(['image' => 'license'])->toMediaCollection();

        });

//        factory(App\User::class, 1)->create()->each(function ($user) {
//
//            $user->children()->save(factory(\App\Models\Child::class)->make())->each(function ($child){
//                $this->createQuestion($child);
//            });
//
//        });
    }

//    public function createDays( $user){
//        $days = \App\Models\Day::all();
//        foreach ($days as $day){
//            $data = [
//                'user_id'   => $user->id,
//                'day_id'    => $day->id,
//                'start'     => date("H:i:s"),
//                'end'       => date("H:i:s"),
//                'working'   => rand(0,1),
//            ];
//            \App\Models\DoctorDay::create($data);
//        }
//    }
//
//    public function createQuestion($child){
//        $questions = \App\Models\Question::all();
//
//        $data = [];
//        foreach ($questions as $question){
//
//            $data[] = [
//                'child_id'      => $child->id,
//                'question_id'   => $question->id,
//                'checked'       => rand(0,1),
//            ];
//        }
//        \Illuminate\Support\Facades\DB::table('question_user')->insert($data);
//    }
}
