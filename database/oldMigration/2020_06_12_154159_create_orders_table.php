<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->boolean('is_charity')->default(0);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('agent_id') ;
            $table->unsignedBigInteger('delivery_id')->nullable();
            $table->unsignedBigInteger('address_id')->comment('address maybe address of user or charity') ;
            $table->unsignedBigInteger('promo_code_id')->nullable() ;
            $table->unsignedTinyInteger('day_id');
            $table->time('time');
            $table->string('notes')->nullable();
            $table->string('plate_number')->nullable();
            $table->boolean('is_payed')->default(0);
            $table->string('tax');
            $table->decimal('delivery_price', 8, 2);
            $table->decimal('total_price', 8, 2);
            $table->enum('payment', ['cash','online'])->default('cash');
            $table->enum('status', ['pending', 'accepted',  'finish','refuse_user','refuse_delivery','refuse_agent'])->default('pending');
            $table->string('message')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users') ->onDelete('cascade');
            $table->foreign('agent_id')->references('id')->on('users') ->onDelete('cascade');
            $table->foreign('delivery_id')->references('id')->on('users') ->onDelete('cascade');
            $table->foreign('promo_code_id')->references('id')->on('promo_codes') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
