const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.scripts([
    'public/assets/admin/js/jquery.min.js',
    'public/assets/admin/js/bootstrap-rtl.min.js',
    'public/assets/admin/js/detect.js',
    'public/assets/admin/js/fastclick.js',
    'public/assets/admin/js/jquery.blockUI.js',
    'public/assets/admin/js/waves.js',
    'public/assets/admin/js/jquery.nicescroll.js',
    'public/assets/admin/js/jquery.slimscroll.js',
    'public/assets/admin/js/jquery.scrollTo.min.js',
    'public/assets/admin/plugins/jquery-knob/excanvas.js',
    'public/assets/admin/plugins/jquery-knob/jquery.knob.js',
    'public/assets/admin/plugins/morris/morris.min.js',
    'public/assets/admin/plugins/raphael/raphael-min.js',
    'public/assets/admin/pages/jquery.dashboard.js',
    'https://jeremyfagis.github.io/dropify/dist/js/dropify.js',
    'public/assets/admin/plugins/parsleyjs/dist/parsley.min.js',
    'public/assets/admin/plugins/datatables/jquery.dataTables.min.js',
    'public/assets/admin/plugins/datatables/dataTables.bootstrap.js',
    'public/assets/admin/plugins/datatables/dataTables.buttons.min.js',
    'public/assets/admin/plugins/datatables/buttons.bootstrap.min.js',
    'public/assets/admin/plugins/datatables/jszip.min.js',
    'public/assets/admin/plugins/datatables/pdfmake.min.js',
    'public/assets/admin/plugins/datatables/vfs_fonts.js',
    'public/assets/admin/plugins/datatables/buttons.html5.min.js',
    'public/assets/admin/plugins/datatables/buttons.print.min.j',
    'public/assets/admin/plugins/datatables/dataTables.fixedHeader.min.js',
    'public/assets/admin/plugins/datatables/dataTables.keyTable.min.js',
    'public/assets/admin/plugins/datatables/dataTables.responsive.min.js',
    'public/assets/admin/plugins/datatables/responsive.bootstrap.min.js',
    'public/assets/admin/plugins/datatables/dataTables.scroller.min.js',
    'public/assets/admin/pages/datatables.init.js',
    'public/assets/admin/js/jquery.core.js',
    'public/assets/admin/js/jquery.app.js',
    'public/assets/admin/js/myscripts.js',
], 'public/js/all.js');


mix.styles([
    'public/assets/admin/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css',
    'public/assets/admin/plugins/fileuploads/css/dropify.min.css',
    'public/assets/admin/plugins/bootstrap-sweetalert/sweet-alert.css',
    'public/assets/admin/plugins/multiselect/css/multi-select.css',
    'public/assets/admin/plugins/datatables/jquery.dataTables.min.css',
    'public/assets/admin/plugins/datatables/buttons.bootstrap.min.css',
    'public/assets/admin/plugins/datatables/fixedHeader.bootstrap.min.css',
    'public/assets/admin/plugins/datatables/responsive.bootstrap.min.css',
    'public/assets/admin/plugins/datatables/scroller.bootstrap.min.css',
    'public/assets/admin/css-ar/bootstrap.min.css',
    'public/assets/admin/css-ar/core.css',
    'public/assets/admin/css-ar/components.css',
    'public/assets/admin/css-ar/icons.css',
    'public/assets/admin/css-ar/pages.css',
    'public/assets/admin/css-ar/menu.css',
    'public/assets/admin/css-ar/responsive.css',
    'public/assets/admin/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css',
    'public/assets/admin/plugins/toastr/toastr.css',
    'public/assets/admin/plugins/toastr/toastr.min.css',
    'public/assets/admin/plugins/switchery/switchery.min.css',
    'public/assets/admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
],'public/css/all.css');
