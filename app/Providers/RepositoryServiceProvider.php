<?php

namespace App\Providers;

use App\Http\Controllers\Api\CartController;
use App\Repositories\AuthRepository;
use App\Repositories\CartRepository;
use App\Repositories\DealerRepository;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\Repositories\Interfaces\CartRepositoryInterface;
use App\Repositories\Interfaces\DealerRepositoryInterface;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Repositories\ProductRepository;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( AuthRepositoryInterface::class,  AuthRepository::class );
        $this->app->bind( CartRepositoryInterface::class,  CartRepository::class );
        $this->app->bind( ProductRepositoryInterface::class,  ProductRepository::class );
        $this->app->bind( DealerRepositoryInterface::class,  DealerRepository::class );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
