<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('percentage', function ($attribute, $value, $parameters, $validator) {

            $sum = 0;
            foreach ($parameters as $key => $attributeName) {
                $attributeValue = array_get($validator->getData(), $attributeName);
                $sum += floatval($attributeValue);
            }
            $sum += floatval($value);

            return $sum <= 100;
        });

//        if (App::environment('production', 'local'))
//        {
//            URL::forceScheme('https');
//        }
    }
}
