<?php

namespace App\Policies;

use App\Models\Order;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

use Illuminate\Auth\Access\Response;
class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function validProgress(User $user, Order $order)
    {
        return $user->id === $order->dealer_id
            ? Response::allow()
            : Response::deny('انت لا تمتلك الصلاحيات  لهذا الطلب ');

    }


    public function refuseOrder(User $user, Order $order)
    {
        return $user->id === $order->delivery_id
            ? Response::allow()
            : Response::deny('تأكد من انك الشخص المستحق لرفض الطلب ');

    }


}
