<?php


namespace App\filter;


use App\Models\Product;
use App\Scoping\Scopes\CategoryScope;
use App\Scoping\Scopes\CityProductScope;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProductQuery
{

    public  function filterByCityOrCategory($request){
        $user = $this->checkUser($request);

        $products = Product::IsSuspend(0)->IsDeleted(0);
        if ($user){
            $products->where('user_id',$user->id);
        }

        $products->withScopes($this->filterByCategory())
            ->whereHas('user',function ($user){
                $user->withScopes($this->filterByCity());
            });

        $this->pagination_query($request, $products);

        return $products->with(['user.cart'])->get();
    }


    public function validRateWithDealer(){ // make sure the products have dealer with  rate
        $products =  Product::IsSuspend(0)->IsDeleted(0)->whereHas('user',function ($user){

            $user->IsDealer()->IsActive(1)->IsAccepted(1)->IsSuspend(0)->IsPayed(1);

        })->with('user.cart');

        $data =  $products->latest()->get();

        $dataWithRating =  $products->whereHas('user.rating')->get();

        if ($dataWithRating->count() > 0 ){

            return ['status' => true , 'data' =>$dataWithRating];
        }

        return ['status' => false , 'data' => $data];
    }

    public function checkUser($request){
        $authorization =  $request->header('authorization');

        if ($authorization){

            $token =ltrim($authorization,"Bearer ");

            $user = User::where('api_token',$token)->first();

            if (!$user){
                return response()->json(['status' => 401]);
            }

            return  $user;
        }
    }



    protected function filterByCategory(){
        return [
            'category'  => new CategoryScope(),
        ];
    }

    protected function filterByCity(){
        return [
            'city' =>new CityProductScope()
        ];
    }

    public function pagination_query($request ,$query ){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        return $query->latest();
    }

}
