<?php


namespace App\filter;


use App\Scoping\Scopes\CategoryScope;
use App\Scoping\Scopes\CityProductScope;
use App\User;

class DealerQuery
{

    public  function filterByCityOrCategory($request){
        $users =  User::IsActive(1)->IsAccepted(1)->IsSuspend(0)->IsPayed(1)
            ->whereHas('profile',function ($profile) use ($request){
                 $profile->withScopes($this->filterByCategory());
            })
            ->withScopes($this->filterByCity())
            ->with('rating','products');

            $this->pagination_query($request, $users);
        return $users->get();
    }



    protected function filterByCategory(){
        return [
            'category'  => new CategoryScope(),
        ];
    }

    protected function filterByCity(){
        return [
            'city' =>new CityProductScope()
        ];
    }

    public function pagination_query($request ,$query ){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        return $query->latest();
    }


}
