<?php

namespace App;

use App\Models\Address;
use App\Models\Brand;
use App\Models\City;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Product;
use App\Models\Profile;
use App\Models\Subscription;
use App\Models\UserImage;
use App\Models\VerifyUser;
use App\Traits\CanBeScoped;
use App\Traits\GlobalScope;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use App\Notifications\MyAdminResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements HasMedia
{
    use Notifiable ,HasRolesAndAbilities  , GlobalScope , CanBeScoped , InteractsWithMedia ;

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));

    }

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {

            if (Schema::hasColumn('users', 'api_token'))
            {
                $model->api_token = Str::random(60);
            }

        });
    }



    public function hasAnyRoles()
    {
        if (auth()->check()) {

            if (auth()->user()->roles->count()) {
                return true;
            }

        } else {
            redirect(route('admin.login'));
        }
    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function code(){
        return $this->hasOne(VerifyUser::class);
    }


    public function imageProfile(  $customProperty = null){
        if (!$customProperty){
            $image =  $this->getFirstMediaUrl();

            return  $image ?  $image : null;
        }

        if ($this->profile){

            $image = $this->profile->getMedia('default', ['image' => $customProperty]);
        }else{
            $image = $this->getMedia('default', ['image' => $customProperty]);
        }


        if ($image->first()){
            return  $image->first()->getUrl();
        }
//        return  $image->first() ? $image[0]->getUrl() : null;
        return  null;
    }



    public function rating_products(){
        return $this->belongsToMany(Product::class,'user_rate','user_id','product_id')->withPivot('rate','product_id');
    }

    public function products(){
        return $this->belongsToMany(Product::class,'user_product','user_id','product_id')
            ->withPivot(['id','quantity','price','is_suspend','is_deleted'])->wherePivot('is_deleted','=',0);
    }

    public function cities(){
        return $this->belongsToMany(City::class,'user_city','user_id','city_id');
    }

    public function avgRate(){
        return number_format($this->rating()->avg('rate'), 1);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function orders_agent(){
        return $this->hasMany(Order::class,'agent_id');
    }

    public function orders_agent_finsih(){
        return $this->hasMany(Order::class,'agent_id')->where('status','=','finish');
    }

    public function notifications(){
        return $this->hasMany(Notification::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }


    public function address(){
        return $this->hasMany(Address::class);
    }



    public function product_favourites(){
        return $this->belongsToMany(Product::class,'user_favourite');
    }


    public function brand_favourites(){
        return $this->belongsToMany(Brand::class,'user_favourite');
    }

}
