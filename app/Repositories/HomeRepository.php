<?php


namespace App\Repositories;

use App\filter\ProductQuery;
use App\Http\Resources\categoryResource;
use App\Http\Resources\CharityAddresseResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\dealer\DealerRateRecource;
use App\Http\Resources\GlobalResource;
use App\Http\Resources\Home\BrandListResource;
use App\Http\Resources\Home\FilterListResource;
use App\Http\Resources\Offers\OfferListResource;
use App\Http\Resources\product\productFilterResource;
use App\Http\Resources\product\ProductResource;
use App\Http\Resources\UserAddresseResource;
use App\Models\Address;
use App\Models\Brand;
use App\Models\CarType;
use App\Models\Category;
use App\Models\CharityAddress;
use App\Models\City;
use App\Models\Offer;
use App\Models\Product;
use App\Models\UserProduct;
use App\Repositories\Interfaces\ListRepositoryInterface;
use App\Scoping\Scopes\BrandScope;
use App\Scoping\Scopes\CategoryScope;
use App\Scoping\Scopes\CharityScope;
use App\Scoping\Scopes\CityProductScope;
use App\Scoping\Scopes\NameScope;
use App\Scoping\Scopes\OfferScope;
use App\Traits\paginationTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeRepository implements ListRepositoryInterface
{
    use paginationTrait;

     public  $lang;

    public function __construct(  )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';
        app()->setLocale($this->lang);

    }


    public function search_products(Request  $request){

        $query = UserProduct::whereIsSuspend(0)->whereIsDeleted(0)->withScopes($this->filterOffers())

            ->whereHas('product',function ($product) use ($request){

                $product->whereIsSuspend(0)->withScopes($this->filterQuery());

                if (!$request->has('charity')){
                    $product->where('category_id','!=',1);
                }

            })->whereHas('user',function ($user){
                $user->has('profile');

                $user->withScopes($this->filterCity());

            })->with('offer','product');

        $data_count = $query->count();
        $this->pagination_query($request,$query);

        $data =  FilterListResource::collection($query->get());

        return ['data' => $data , 'total_count' => $data_count];
    }


    public function list_brands($request){


        $query = Brand::whereHas('products',function ($product){
            $product->whereIsSuspend(0)->wherehas('user_products',function ($user){
                $user->whereHas('profile');
            })->withScopes($this->filterQuery());
        })->with('products.user_products');

        $data_count = $query->count();

        $this->pagination_query($request,$query);

        $data =  BrandListResource::collection($query->get());

        return ['data' => $data , 'total_count' => $data_count];
    }

    public function car_types($request){

        $cars = CarType::whereIsSuspend(0)->get();

        return categoryResource::collection($cars);
    }


    protected function filterQuery(){
        return [
            'name'          => new NameScope(),
            'brand'         => new BrandScope(),
            'category'      => new CategoryScope(),
            'city'          => new CityProductScope(),
            'charity'       => new CharityScope(),
        ];
    }

    protected function filterOffers(){
        return [
            'offer'         => new OfferScope(),
        ];
    }

    protected function filterCity(){
        return [
            'city'         => new CityProductScope(),
        ];
    }

    public function listCity($request)
    {
        $query = City::IsSuspend(0)->latest();

        if ($request->agents == "true"){
            $query->whereHas('agents');
        }

       return CityResource::collection($query->get());
    }

    public function listCategory($request)
    {
        $data = Category::where('id','!=',1)->whereIsSuspend(0)->whereIsDeleted(0)->orderBy('order_by')->get();

        return GlobalResource::collection($data);
    }

    public function charity_addresses($request)
    {
        $query = CharityAddress::whereIsSuspend(0)->whereIsDeleted(0);

        if ($request->cityId){
            $query->whereCityId($request->cityId);
        }
        $this->pagination_query($request,$query);

        $data =  $query->get();

        return CharityAddresseResource::collection($data);
    }

    public function addresses(Request $request)
    {
        $user = User::findOrFail($request->userId);

        $query = Address::whereUserId($user->id);

        if ($request->cityId){
            $query->whereCityId($request->cityId);
        }
        $this->pagination_query($request,$query);

        $data =  $query->select('id','city_id','name','phone','address','is_default')->get();

        return UserAddresseResource::collection($data);
    }

}
