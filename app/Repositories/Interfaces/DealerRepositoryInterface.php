<?php


namespace App\Repositories\Interfaces;


interface DealerRepositoryInterface
{
    public function list($request);

    public function show($request);


}
