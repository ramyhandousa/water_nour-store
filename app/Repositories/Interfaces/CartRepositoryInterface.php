<?php


namespace App\Repositories\Interfaces;

interface CartRepositoryInterface
{

    public function products();

    public function add($products);

    public function update($productId , $quantity);

    public function delete($product);

    public function destroyCart();

    public function total();

    public function sync();

    public function isEmpty();

}
