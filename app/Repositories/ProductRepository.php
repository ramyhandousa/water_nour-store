<?php


namespace App\Repositories;


use App\Http\Resources\product\listFavouriteResource;
use App\Http\Resources\product\productCommentResource;
use App\Http\Resources\product\productFilterResource;
use App\Models\UserProduct;
use App\Models\UserRate;
use App\Scoping\Scopes\CityProductScope;
use App\Traits\paginationTrait;

class ProductRepository
{
    use paginationTrait;
    public function show($id)
    {
        $product = UserProduct::whereHas('product',function ($product) use ($id){
                $product->whereId($id)->withScopes($this->filterCity());
            })->whereHas('user',function ($user){
            $user->withScopes($this->filterCity());

        })->with('product.rating','product.user_favourites')->firstOrFail();

       return  new productFilterResource($product);
    }


    public function list_rate_and_comments($request, $product){

        $query =  UserRate::whereHas('product',function ($q) use ($product){
            $q->whereId($product->id)->withScopes($this->filterQuery());
        });

        $this->pagination_query($request,$query);

        $comments = $query->get();

        return productCommentResource::collection($comments);
    }


    public function list_favourite_products($request){

        $product_favourites =  $request->user()->product_favourites->pluck('id');

        $products =  UserProduct::whereHas('product',function ($q) use ($product_favourites){
            $q->whereIn('id',$product_favourites)->withScopes($this->filterQuery());
        })->get();

        return listFavouriteResource::collection($products);
    }


    protected function filterQuery(){
        return [
            'city'          => new CityProductScope(),
        ];
    }

    protected function filterCity(){
        return [
            'city_id'          => new CityProductScope(),
        ];
    }


}
