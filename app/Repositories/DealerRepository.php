<?php


namespace App\Repositories;


use App\filter\DealerQuery;
use App\Http\Resources\dealer\DealerFilterResource;
use App\Http\Resources\dealer\DealerResource;
use App\Repositories\Interfaces\DealerRepositoryInterface;
use App\User;

class DealerRepository implements DealerRepositoryInterface
{

    /**
     * @param $request
     * @return mixed
     */
    public function list($request)
    {
        $filter = new DealerQuery();

        $data = $filter->filterByCityOrCategory($request);

       return DealerResource::collection($data);
    }



    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $user =  User::find($id);
       return new DealerFilterResource($user);
    }
}
