<?php


namespace App\Repositories;


use App\Events\PhoneOrEmailChange;
use App\Events\RegisterDelivery;
use App\Events\UserLogOut;
use App\Http\Requests\api\login;
use App\Http\Requests\api\resgister;
use App\Http\Resources\dealer\DealerFilterResource;
use App\Http\Resources\dealer\DealerResource;
use App\Http\Resources\product\imageRecource;
use App\Http\Resources\UserRecource;
use App\Jobs\ProcessMailSend;
use App\Mail\codeActivation;
use App\Mail\mailActiveAccount;
use App\Mail\mailResendActiveAccount;
use App\Models\Device;
use App\Models\UserImage;
use App\Models\VerifyUser;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthRepository implements AuthRepositoryInterface
{
    use RespondsWithHttpStatus;

    public function __construct( )
    {
        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');
    }

    public function register($request)
    {
        $action_code = $this->random_code_active();

        $user = User::create($request->validated());

        $this->createVerfiy($request, $user, $action_code);

        $this->manageDevices($request, $user);

        return ['code' => $action_code, 'api_token' => $user->api_token];
    }

    public function register_delivery($request)
    {
        $action_code = $this->random_code_active();

        $user = User::create($request->validated()['user']);

        $user->profile()->create($request->validated()['profile']);

        $this->updateProfile($user->profile, $request);

        $this->createVerfiy($request, $user, $action_code);

        $this->manageDevices($request, $user);

        return ['code' => $action_code, 'api_token' => $user->api_token];
    }


    public function login($request)
    {
        $user =  User::wherePhone($request->phone)->first();

        $this->manageDevices($request, $user);

        $action_code = $this->random_code_active();

        $this->createVerfiy($request, $user, $action_code);

        return new UserRecource($user);
    }

    public function login_delivery($request)
    {
        $user =  Auth::user();

        $this->manageDevices($request, $user);

        return new UserRecource($user);
    }

    public function forgetPassword($request)
    {
        $user =  User::wherePhone($request->phone)->first();

        $action_code = $this->random_code_active();

        $this->createVerfiy($request , $user , $action_code);

        return ['code' => $action_code];
    }

    public function resetPassword($request)
    {
        $user =  User::wherePhone($request->phone)->first();

        if ($request->password){

            $user->update(['password' => $request->password]);

            $data = new UserRecource($user);

            return  [ 'data' => $data , 'message' => trans('global.password_was_edited_successfully') ];

        }else{
            $data = new UserRecource($user);

            return  [ 'data' => $data , 'message' => trans('global.password_not_edited') ];
        }
    }

    public function checkCode($request)
    {
        $verifyUser = VerifyUser::wherePhone($request->phone)->with('user')->first();

        $verifyUser['user']->update(['phone' => $verifyUser->phone , 'is_active' => 1]);

        $verifyUser->delete();


        if ($verifyUser['user']->defined_user == "delivery"){

            event(new RegisterDelivery($verifyUser['user'],$request,'register_delivery_to_admin'));
        }

        return new UserRecource($verifyUser['user']);
    }

    public function resendCode($request)
    {
        $user = VerifyUser::where('phone',$request->phone)->with('user')->first();

        $action_code = $this->random_code_active();

        $user->update(['action_code' => $action_code ]);

        return   ['code' => $action_code];
    }

    public function changPassword($request)
    {
        $user = Auth::user();

        if ($request->newPassword){

            $user->update( [ 'password' => $request->newPassword  ] );

            $message =  trans('global.password_was_edited_successfully') ;

        }else{

            $message = trans('global.password_not_edited');
        }

        return  ['message' => $message] ;
    }

    public function editProfile($request)
    {
        $data = $request->only(['name','phone' ,'email' ,'city_id','longitude','latitude','address']);

        $user = Auth::user();

        $action_code = $this->random_code_active();

        $columnChange = 'phone';
        $requestChange = $request->phone;

        $columnExists = $user->where($columnChange, $requestChange)->exists();

            if (!$columnExists && $requestChange){
                event(new PhoneOrEmailChange($user,$action_code,$columnChange,$requestChange));
            }

        $filterData =  collect($data)->except('phone');
        $user->fill($filterData->toArray());
        $user->save();


        if ($request->hasFile('image')):
            $user->clearMediaCollection();
            $user->addMediaFromRequest('image')->toMediaCollection();
        endif;


        if ($user->defined_user == "delivery"){
            $this->updateProfile($user->profile,$request);
            $user->load('profile');
        }

        return new UserRecource($user);
    }

    public function logOut($request)
    {
        $user = Auth::user();

        event(new UserLogOut($user,$request));
    }

    public function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            Device::updateOrCreate(['device' => $request->deviceToken], ['user_id' => $user->id]);
        }
    }

    public function createVerfiy($request , $user , $action_code){

        $data = [ 'phone' => $request->phone , 'action_code'  => $action_code];

        VerifyUser::updateOrCreate(['user_id' => $user->id,'phone' => $request->phone], $data);
    }


    public function updateProfile($profile, $request){
        if ($request->hasFile('hobby_picture')):
            $data =  $profile->getMedia('default', ['image' => 'hobby_picture']);
            $data ? $data->each->delete() : null;
            $profile->addMediaFromRequest('hobby_picture')->withCustomProperties(['image' => 'hobby_picture'])->toMediaCollection();
        endif;
        if ($request->hasFile('license')):
            $data =  $profile->getMedia('default', ['image' => 'license']);
            $data ? $data->each->delete() : null;
            $profile->addMediaFromRequest('license')->withCustomProperties(['image' => 'license'])->toMediaCollection();
        endif;
        if ($request->hasFile('vehicle_investment')):
            $data =  $profile->getMedia('default', ['image' => 'vehicle_investment']);
            $data ? $data->each->delete() : null;
            $profile->addMediaFromRequest('vehicle_investment')->withCustomProperties(['image' => 'vehicle_investment'])->toMediaCollection();
        endif;
    }


}
