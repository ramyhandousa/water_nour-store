<?php

namespace App\Repositories;

use App\Repositories\Interfaces\CartRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class CartRepository  implements CartRepositoryInterface
{

    protected $change = false;

    /**
     * @return mixed
     */
    public function products()
    {
        return Auth::user()->cart;
    }

    /**
     * @param $products
     * @return mixed
     */
    public function add($products)
    {
        Auth::user()->cart()->syncWithOutDetaching($this->payLoad($products));
    }

    /**
     * @param $productId
     * @param $quantity
     * @return mixed
     */
    public function update($productId , $quantity)
    {
        Auth::user()->cart()->updateExistingPivot($productId,['quantity' => $quantity]);
    }

    /**
     * @param $product
     * @return mixed
     */
    public function delete($product)
    {
        Auth::user()->cart()->detach($product);
    }

    /**
     * @return mixed
     */
    public function destroyCart()
    {
        Auth::user()->cart()->detach();
    }

    /**
     * @return mixed
     */
    public function total()
    {
        $user = Auth::user();
        return $user->cart->sum(function ($product){
            return   $product->price * $product->pivot->quantity;
        });
    }

    /**
     * @return mixed
     */
    public function sync()
    {
        $user = Auth::user();
        $user->cart->each(function ($product){

            $quantity =  $product->minStock($product->pivot->quantity);

            if( $quantity != $product->pivot->quantity ){

                $this->change = true;
            }
            $product->pivot->update(['quantity' => $quantity ]);
        });
    }

    /**
     * @return mixed
     */
    public function isEmpty()
    {
        $user = Auth::user();
        return     $user->cart->sum('pivot.quantity') <= 0;
    }


    public function hasChange(){
        return $this->change;
    }

    protected function payLoad($products){

        return collect($products)->keyBy('id')->map(function ($product){
            return [
                'quantity' => $product['quantity'] + $this->getCurrentQuantity($product['id'])
            ];
        })->toArray();
    }

    protected function getCurrentQuantity($productId){
        $user = Auth::user();

        if ($product = $user->cart->where('id',$productId)->first()){
            return $product->pivot->quantity;
        }

        return 0;
    }



}
