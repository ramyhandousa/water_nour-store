<?php

namespace App\Exports;

use App\Models\Order;
use App\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InvoiceExport implements FromCollection  ,WithHeadings ,ShouldAutoSize
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('orders')
            ->where("orders.agent_id",'=', $this->user->id)
            ->join('users as user','user.id','=','orders.user_id')
            ->join('users as delivery','delivery.id','=','orders.delivery_id')
            ->join('days','days.id','=','orders.day_id')
            ->select("orders.id","user.name as user_name","delivery.name as delivery_name",
                DB::raw("DATE_FORMAT(orders.created_at, '%Y-%m-%d') as day_name"),
                DB::raw("(CASE WHEN (is_charity = 1) THEN 'نعم' ELSE 'لا' END) as is_charity"),
                "orders.tax", "orders.delivery_price", "orders.sum_price",
                DB::raw("(CASE WHEN (orders.discount_code ) THEN discount_code ELSE 'لا يوجد خصم' END) as promo_code"),
                "orders.total_price",
                DB::raw("(CASE WHEN (orders.tax ) THEN ( total_price * tax )  /100 ELSE '0' END) as admin_price"),
                DB::raw("(CASE WHEN (orders.tax ) THEN ( total_price - ( total_price * tax )  /100  ) ELSE total_price END) as agent_price"),
            )
            ->get();

    }

    public function headings(): array
    {
        return [
            ' # ',
            ' إسم الشخص   ',
            '    إسم المندرب ' ,
            '  اليوم ',
            '  قسم خيري  ',
            ' الضريبة المضافة  ',
            '  سعر  التوصيل  ',
            ' سعر الطلبات  ',
            '  قيمة الخصم  ',
            ' السعر  النهائي  ',
            '  حساب الأدمن  للطلب  ',
            ' حساب التاجر  للطلب  ',
        ];
    }
}
