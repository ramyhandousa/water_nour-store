<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class OfferScope implements Scope
{

    public function apply(Builder $builder , $value){

        return $builder->whereHas('offer',function ($offer){
            $offer->where('end_at','>=',Carbon::now());
        });
    }
}
