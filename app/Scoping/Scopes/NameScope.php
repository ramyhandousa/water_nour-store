<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class NameScope implements Scope
{

    public function apply(Builder $builder , $value){

        return $builder->where('name_ar', 'Like',"%{$value}%")
            ->Orwhere('name_en', 'Like',"%{$value}%")
            ->orWhereHas('brand',function ($brand)use ($value){
                $brand->where('name_ar', 'Like',"%{$value}%")
                    ->Orwhere('name_en', 'Like',"%{$value}%");
            });
    }
}
