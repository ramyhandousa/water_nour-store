<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OrderByScope implements Scope
{

    public function apply(Builder $builder, $value)
    {

         if($value == 'accepted'){

            return $builder->where('status','=','accepted');

        }elseif($value == 'finish'){

            return $builder->where('status','=','finish');

        }elseif($value == 'refuse'){

            return $builder->whereIn('status',['refuse_delivery','refuse_agent' ,'refuse_user']);
        }else{
             return $builder->where('status','=','pending');
         }

    }
}
