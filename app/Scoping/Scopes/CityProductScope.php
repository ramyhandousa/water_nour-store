<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class CityProductScope implements Scope
{

    public function apply(Builder $builder, $value)
    {

        return $builder->whereHas('cities',function ($city) use ($value){

                $city->where('city_id',$value);
        });
    }
}
