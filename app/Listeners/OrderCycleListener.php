<?php

namespace App\Listeners;

use App\Events\OrderCycle;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class OrderCycleListener
{

    public $push;

    public function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  OrderCycle  $event
     * @return void
     */
    public function handle(OrderCycle $event)
    {
        $user = $event->user;
        $order = $event->order;
        $status = $event->status;
        $sender = null;
        $title = null;
        $body = null;
        $data = [];
        $logo =  $user->imageProfile() ? :'https://nour-store.com/logo.png';

        $this->check_status_send($status,$order,$logo , $user);

    }

    public function check_status_send($status,$order,$logo , $user){

        if ($status == "new_order"){

            $title = "طلبات جديدة";
            $body =   " لديك طلب جديد رقم " . $order->id ;
            $type = 15;

        }

        if ($status == "accepted_delivery"){

            $title = "الطلبات الجارية";
            $body =   "تم قبول الطلب رقم " . $order->id . " من المندوب " . $user->name ;
            $type = 16;
        }

        if ($status == "refuse_delivery"){

            $title = "الطلبات المعلقة";
            $body =   "تم رفض الطلب رقم " . $order->id . " برجاء إختيار مندوب اخر " ;
            $type = 17;
        }

        if ($status == "finish_order"){

            $title = "الطلبات المنتهية";
            $body =   "تم الإتهاء من الطلب رقم " . $order->id ;
            $type = 18;
        }

        $sender = $order->agent;

        $data = [
            "href" => "/administrator/agent_orders/".$order->id,
            "image" => $logo
        ];

        $devices = Device::whereUserId($sender->id)->pluck('device');

        if (isset($devices) && count($devices) > 0){

            $this->push->sendPushNotification(null,$devices,$title,$body,$data);
        }

        Notification::create([  'user_id' => $sender->id ,'sender_id' => $user->id,
                                'title' => $title,'body' => $body,'order_id' => $order->id,
                                "type" => $type]);
    }
}
