<?php

namespace App\Listeners;

use App\Events\RegisterDelivery;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\Notification;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class RegisterDeliveryListener
{
    public $push;

    public function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  RegisterDelivery  $event
     * @return void
     */
    public function handle(RegisterDelivery $event)
    {

        $user = $event->user;
        $reuest = $event->request;
        $status = $event->status;
        $title = "مناديب جديدة";
        $body = " يوجد مندوب جديد إسمه " . $user->name;
        $type = 19;

        $logo =  $user->imageProfile() ? :'https://nour-store.com/logo.png';


        if ($status == "register_delivery_to_admin"){

            $sender = User::find(1);

        }else{
            $user_city = DB::table('user_city')->where("city_id",'=',$user->city_id)->first();

            if ($user_city){

                $sender = User::find($user_city->user_id);

            }
        }

        if ($sender){

            $devices = Device::whereUserId($sender->id)->pluck('device');

            if (isset($devices) && count($devices) > 0){

                $data = [
                    "href" => "/administrator/deliveries?is_accepted=0",
                    "image" => $logo
                ];

                $this->push->sendPushNotification(null,$devices,$title,$body,$data);
            }

            Notification::create([  'user_id' => $sender->id ,'sender_id' => $user->id,
                'title' => $title,'body' => $body,"type" => $type]);
        }


    }
}
