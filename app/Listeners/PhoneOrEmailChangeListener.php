<?php

namespace App\Listeners;

use App\Events\PhoneOrEmailChange;
use App\Jobs\ProcessMailSend;
use App\Mail\codeActivation;
use App\Models\VerifyUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PhoneOrEmailChangeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhoneOrEmailChange  $event
     * @return void
     */
    public function handle(PhoneOrEmailChange $event)
    {
        // $event->change => what will Be Change Phone Or E-mail

        $data = [ $event->change => $event->request , 'action_code'  => $event->code];

        VerifyUser::updateOrCreate(['user_id' => $event->user->id], $data);

        if ($event->change === 'email'){
           // Sending E-mail
              ProcessMailSend::dispatch($event->user ,$event->code , codeActivation::class);

        }else{
            // Sms
                //Sms::sendMessage('Activation code:' . $event->code, $event->request);
            return;
        }

    }
}
