<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;

class InsertNotification {

    public $push;
    function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $order = null ,$additional = null){

            switch($type){

            case $type == 1:

                $data = Notification::create([
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                ]);

                return $data;

             break;

            case $type == 2:

                // contactUs Form Users
                $data =  Notification::create([
                    'user_id' => 1 ,
                    'sender_id' => $sender ,
                    'title' => trans('global.connect_us'),
                    'body' => $request ,
                    'type' => 2,
                ]);

                return  $data;

                 break;

            case $type == 5:

                $data = Notification::create([
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'order_type' => 'product',
                    'order_id' => $order->id,
                    'product_id' => $order->product_id,
                    'title' =>   'طلبات المنتجات'  ,
                    'body' =>  ' تم رفض طلب المنتج من مزود الخدمة ' .   $sender->name .' بسبب  ' . $request  ,
                    'type' => 5,
                ]);

               return  $data;

             break;

            case $type == 6:

                $data = Notification::create([
                    'user_id'       => $user->id ,
                    'sender_id'     => $sender->id,
                    'order_type'    => 'project',
                    'order_id'      => $order->id,
                    'title'         =>   ' طلبات المشاريع  '  ,
                    'body'          =>  ' يوجد طلب مشروع جديد من ' .   $sender->name ,
                    'type'          => 6,
                 ]);

                return  $data;
             break;

             case $type == 7:

                $data = Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => $sender->id,
                    'invitation_id'      => $order->id,
                    'title'         =>   'العروض '  ,
                    'body'          =>     $order->id .' علي المشروع رقم  '. $sender->name .' تم قبول عرضك من المستخدم   '  ,
                    'type'          => 7,
                ]);

                 return  $data;

              break;

             case $type == 8:

                $data = Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => 1,
                    'order_type'    => 'project',
                    'order_id'      => $order->id,
                    'title'         =>   'العروض '  ,
                    'body'          =>   $order->id . ' تم انتهاء من التوقيت يمكنك الأن تفحص العروض علي طلبك رقم  '  ,
                    'type'          => 8,
                ]);
                 return  $data;

             break;

            case $type == 11:

                    $data = [
                        'user_id'   => $user ,
                        'sender_id' => 1,
                        'topic'     => 'provider',
                        'title'     =>   ' المحفظة  '  ,
                        'body'      =>   ' قامت الإدارة بتصفية حسابك والرصيد هو ' . $request . ' ريال ' ,
                        'type'      => 11,
                    ];
                    $this->insertData($data);
                break;


            case $type == 23:
                $data = Notification::create([
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'invitation_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم قبول التحويل الخاص بي المشروع  علي الطلب رقم ' .    $order->id ,
                    'type'         => 23,
                ]);
                return  $data;
            break;

            case $type == 24:
                $data = Notification::create([
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'invitation_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم رفض التحويل علي طلبك    رقم ' .    $order->id  .' بسبب  ' . $request,
                    'type'         => 24,
                ]);
                 return  $data;
            break;

            case $type == 25:
                $data = Notification::create([
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'invitation_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم رفض التحويل علي طلبك من المشروع  رقم ' .    $order->id  .' بسبب  ' . $request,
                    'type'         => 25,
                ]);
                return  $data;
            break;

            default:

     }


    }


    private function insertData($data)
    {
     if (count($data) > 0) {

         $time = ['created_at' => now(),'updated_at' => now()];
           Notification::insert($data   + $time);

     }
    }


}

