<?php


namespace App\Libraries;


use App\Libraries\payment_tap\Invoice;
use GuzzleHttp\Client;

class TapPayments
{

    public $token;
    protected $client;
    protected $method = 'post';
    public $basURL;
    public $charges;
    protected $attributes = [];
    protected $endpoint;
    public function __construct( $id = null  )
    {

        if ( $id ) {
            $this->attributes['id'] = $id;
            $this->setEndpoint( $id );
        }

        $this->client = new Client();

        $language = request()->headers->get('lang') ? : 'ar';
        app()->setLocale($language);

//        $this->token = 'sk_test_XKokBfNWv6FIYuTMg5sLPjhJ';
        $this->token = 'sk_test_IuhcaB1sO9TibDfjzGrYwLqC';
//        $this->token = 'pk_test_XPz7HWdLqDo8aMpyGxv5tCUr';

        $this->basURL           = 'https://api.tap.company/v2';
        $this->charges           = $this->basURL .'/charges';
    }

    protected function setEndpoint( $endpoint )
    {
        $this->basURL .= $endpoint;
    }

    protected function setMethod( $method )
    {
        $this->method = $method;
    }

    public function find()
    {
        $this->setMethod( 'get' );

        return $this->send();
    }

    public function getPath()
    {
        return $this->basURL . '/' . $this->endpoint;
    }



    protected function send()
    {

        try {
            $response = $this->client->request(
                $this->method,
                $this->getPath(),
                [
                    'form_params' => $this->attributes,
                    'headers'     => [
                        'Authorization' => 'Bearer ' . $this->token,
                        'Accept'        => 'application/json',
                    ]
                ]
            );


            return new Invoice( json_decode( $response->getBody()->getContents(), true ) );
        }
        catch ( \Throwable $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }


    public function myfatoorahResponse( ){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->basURL,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $this->fides(),
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $this->token", "Content-Type: application/json"),
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $results =  json_decode($response, true);

        return $results;
    }


    function fides(){
        $fields = [
                "amount"=> 1,
                "currency"=> "SAR",
                "threeDSecure"=> true,
                "save_card"=> false,
                "description"=> "Test Description",
                "statement_descriptor"=> "Sample",
                "metadata"=> [
                    "udf1"=> "test 1",
                    "udf2"=> "test 2"
                ],
                "reference"=> [
                    "transaction"=> "txn_0001",
                    "order"=> "ord_0001"
                ],
                "receipt"=> [
                    "email"=> false,
                    "sms"=> true
                ],
                "customer"=> [
                    "first_name"=> "test",
                    "middle_name"=> "test",
                    "last_name"=> "test",
                    "email"=> "test@test.com",
                    "phone"=> [
                        "country_code"=> "965",
                        "number"=> "50000000"
                    ]
                ],
                "source"=> [
                    "id"=> "src_all"
                ],
                "post"=> [
                    "url"=> URL('/'). "/api/post_url"
                ],
                "redirect"=> [
                    "url"=> URL('/')."/api/redirect_url"
                ]
            ];

        return json_encode($fields);
    }


}
