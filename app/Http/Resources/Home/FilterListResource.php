<?php

namespace App\Http\Resources\Home;

use App\Http\Resources\Offers\OfferListResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class FilterListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = Config::get('app.locale');

        $token = $request->bearerToken();
        $user = User::whereApiToken($token)->first();

        $user_id = $token ? $user?$user->id:null : null;
        return [
            "id"                 => $this->product->id,
            "agent_id"           => $this->user_id,
            "name"  => $lang == 'ar' ?  optional($this->product)->name_ar : optional($this->product)->name_en,
            "price" => $this->price,
            "image" => $this->product->getFirstMediaUrl(),
            "is_favourite"      => (boolean) $this->product->checkInUserFavourite($user_id),
            "is_charity"        =>  $this->product->category_id == 1,
            $this->mergeWhen($this->offer && !Carbon::parse($this->offer->end_at)->isPast(),[
                'offer' => new OfferListResource($this),
            ])
        ];
    }
}
