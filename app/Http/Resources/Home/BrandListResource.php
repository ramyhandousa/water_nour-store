<?php

namespace App\Http\Resources\Home;

use App\Models\UserRate;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class BrandListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = Config::get('app.locale');

        $token = $request->bearerToken();
        $user = User::whereApiToken($token)->first();

        $rate = $this->my_rate($this->products->pluck('id'))->average('rate');
        return [
            'id'            => $this->id,
            'name'          => $lang == 'ar' ? $this->name_ar  : $this->name_en,
            "image"         => $this->getFirstMediaUrl(),
            'rate'          =>  $rate ? number_format($rate,0) :"0",
            'count_rate'    => $this->my_rate($this->products->pluck('id'))->count(),
            'is_favourite'  => (boolean)  $user ? $user->brand_favourites->contains($this->id): false,
        ];
    }


    public function my_rate($products){
        return    UserRate::whereIn('product_id',$products )->get();
    }
}
