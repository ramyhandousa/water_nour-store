<?php

namespace App\Http\Resources\cart;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = Config::get('app.locale');
        return [
            "id"            =>  $this->id,
            "name"          =>   $lang == 'ar' ? $this->name_ar  : $this->name_en ,
            "description"   =>  $lang == 'ar' ? $this->desc_ar  : $this->desc_en  ,
            "price"         =>  $this->price,
            'quantity'      =>  $this->pivot->quantity,
            'total'         =>  $this->price * $this->pivot->quantity,
        ];
    }
}
