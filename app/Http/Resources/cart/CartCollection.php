<?php

namespace App\Http\Resources\cart;


use Illuminate\Http\Resources\Json\ResourceCollection;

class CartCollection extends ResourceCollection
{
    public $collects = CartResource::class;

    public function toArray($request)
    {
        return [
            'data' => $this->collection
        ];
    }

//    public function with($request)
//    {
//
//        return [
//            'meta' => [
//                'change' => $this->cart->hasChange(),
//            ],
//        ];
//    }
}
