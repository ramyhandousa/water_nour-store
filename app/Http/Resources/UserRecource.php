<?php

namespace App\Http\Resources;

use App\Http\Resources\delivery\profileFilter;
use App\Http\Resources\Location\LocationFilterResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserRecource extends JsonResource
{

    public function toArray($request)
    {
        if ($this->agent_accepted == 0){
            $agent_accepted = null;
        }elseif ($this->agent_accepted == 1){
            $agent_accepted = true;
        }else{
            $agent_accepted = false;
        }

        if ($this->is_accepted == 0){
            $is_accepted = null;
        }elseif ($this->is_accepted == 1){
            $is_accepted = true;
        }else{
            $is_accepted = false;
        }

        return [
            'id'            => $this->id,
            $this->mergeWhen( $this->code && $this->code->phone !== null ,[
                'code'          => $this->when($this->code, optional($this->code)->action_code),
            ]),
            'defined_user'  => $this->defined_user,
            'name'          => $this->name,
            'image_profile' => $this->when($this->imageProfile() ,  $this->imageProfile()) ,
            'phone'         => $this->phone,
            'email'         => $this->when($this->email , $this->email) ,
            'api_token'     => $this->when($this->api_token , $this->api_token) ,
            'is_active'     => (boolean) $this->is_active ,
            'is_suspend'    => (boolean) $this->is_suspend  ,
            'message'       => $this->when($this->message , $this->message) ,
            'city'          => $this->when($this->city , new CityResource($this->city)) ,
            'location'      => $this->when($this->address , new LocationFilterResource($this)),

            $this->mergeWhen( $this->profile && $this->defined_user == "delivery"  ,[
                'is_accepted'      => $is_accepted,
                'agent_accepted'   => $agent_accepted,
                'profile'          => new profileFilter($this->profile),
            ]),
        ];
    }
}
