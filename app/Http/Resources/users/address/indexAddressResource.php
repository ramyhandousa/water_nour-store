<?php

namespace App\Http\Resources\users\address;

use App\Http\Resources\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class indexAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'phone'         => $this->phone,
            'address'       => $this->address,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'street'        => $this->street,
            'building_number'     => $this->building_number,
            'floor'         => $this->floor,
            'flat'          => $this->flat,
            'city'          => new CityResource($this->city),
            'is_default'    => (boolean) $this->is_default,
        ];
    }
}
