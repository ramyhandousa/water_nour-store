<?php

namespace App\Http\Resources\users\address;

use App\Http\Resources\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class showAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $agent_by_city = DB::table('user_city')->where('city_id','=',$this->city_id)->first();

        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'phone'             => $this->phone,
            'street'            => $this->street,
            'building_number'   => $this->when($this->building_number,$this->building_number),
            'floor'             => $this->when($this->floor,$this->floor) ,
            'flat'              => $this->when($this->flat,$this->flat) ,
            'additional_notes'  => $this->when($this->additional_notes,$this->additional_notes),
            'latitude'          => $this->latitude,
            'longitude'         => $this->longitude,
            'address'           => $this->address,
            'agnet_id'          => $agent_by_city ? $agent_by_city->user_id : null,
            'city'              => new CityResource($this->city),
            'is_default'        => (boolean) $this->is_default,
        ];
    }
}
