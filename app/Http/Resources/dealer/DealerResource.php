<?php

namespace App\Http\Resources\dealer;

use Illuminate\Http\Resources\Json\JsonResource;

class DealerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            'image_profile'         => $this->when($this->imageProfile('profile') ,  $this->imageProfile('profile')) ,
            'is_payed'              => $this->is_payed == 1,
            'count_products'        => $this->products->count(),
            'rate_value'            => $this->avgRate(),
            'rate_count'            => $this->rating->count(),
            'remain_subscription'   => $this->when($this->remain_subscription()  ,  $this->remain_subscription()),
        ];
    }
}
