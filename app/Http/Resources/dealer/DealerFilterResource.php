<?php

namespace App\Http\Resources\dealer;

use App\Http\Resources\categoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DealerFilterResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            'phone'                 => $this->phone,
            'email'                 => $this->email,
            'address'               => $this->when($this->address ,  $this->address) ,
            'desc'                  => $this->when($this->profile->desc ,  $this->profile->desc) ,
            'image_profile'         => $this->when($this->imageProfile('profile') ,  $this->imageProfile('profile')) ,
            'image_product'         => $this->when($this->imageProductFirst() ,  $this->imageProductFirst()) ,
            'is_payed'              => $this->is_payed == 1,
            'rate_value'            => $this->avgRate(),
            'rate_count'            => $this->rating->count(),
            $this->mergeWhen( $request->query('profile'),
                [
                    'remain_subscription'               => $this->when($this->remain_subscription()  ,  $this->remain_subscription()),
                    'category'                          => new categoryResource($this->profile->category),
                    'last_change_password'              =>  $this->when($this->password_change_at  ,  $this->password_change_at? $this->password_change_at->diffForHumans(): null),
                ])
        ];
    }

}
