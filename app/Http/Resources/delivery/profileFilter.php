<?php

namespace App\Http\Resources\delivery;

use App\Http\Resources\categoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class profileFilter extends JsonResource
{

    public function toArray($request)
    {
        $hobby_picture = $this->imageProfile('hobby_picture');
        $license = $this->imageProfile('license');
        $vehicle_investment = $this->imageProfile('vehicle_investment');

        return [
            'car'                   =>  new categoryResource($this->car),
            'plate_number'          => $this->when($this->plate_number ,$this->plate_number),
            'hobby_picture'         => $hobby_picture,
            'license'               => $license,
            'vehicle_investment'   => $vehicle_investment,

        ];
    }
}
