<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $agent_by_city = DB::table('user_city')->where('city_id','=',$this->id)->first();
        $lang = Config::get('app.locale');
        return [
            'id' => $this->id,
            'name' => $lang == 'ar' ? $this->name_ar  : $this->name_en,
            'agnet_id'          => $agent_by_city ? $agent_by_city->user_id : null,
        ];
    }
}
