<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserFilterRecource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'name'  =>  $this->when($this->name,$this->name),
            'phone' =>  $this->when($this->phone,$this->phone),
            'image' => $this->when($this->imageProfile() ,  $this->imageProfile()) ,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
//            'email' =>  $this->when($this->email,$this->email),
        ];
    }
}
