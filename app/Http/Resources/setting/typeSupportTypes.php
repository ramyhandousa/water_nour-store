<?php

namespace App\Http\Resources\setting;

use Illuminate\Http\Resources\Json\JsonResource;

class typeSupportTypes extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name_ar ? : $this->name_en
        ];
    }
}
