<?php

namespace App\Http\Resources\setting;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class BankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = Config::get('app.locale');
        return [
            'id'        => $this->id,
            'name'      => $lang == 'ar' ? $this->name_ar  : $this->name_en,
            'number'    => $this->number
        ];
    }
}
