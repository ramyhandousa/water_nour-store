<?php

namespace App\Http\Resources\order;

use App\Http\Resources\CharityAddresseResource;
use App\Http\Resources\dealer\DealerResource;
use App\Http\Resources\Location\LocationFilterResource;
use App\Http\Resources\UserFilterRecource;
use App\Http\Resources\users\address\indexAddressResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderRecource extends JsonResource
{


    public function toArray($request)
    {
        $user = $request->user()->defined_user == "user";

        return [
            'id'                => $this->id,
            'notes'             => $this->when($this->notes,$this->notes),
            'tax'               => $this->tax,
            'tax_value'         => $this->tax_value,
            'discount_code'     => $this->when($this->discount_code,$this->discount_code),
            'discount_value'    => $this->when($this->discount_value,$this->discount_value),
            'delivery_price'    => $this->delivery_price,
            'sum_price'         => $this->sum_price,
            'total_price'       => $this->total_price,
            'tax_number'        => $this->tax_number,
            'payment'           => $this->payment  ,
            'status'            => $this->status   ,
            'created_at'        => $this->created_at ,
            'message'           => $this->when($this->message,$this->message),
            "location"           => $this->is_charity == 1 ? new CharityAddresseResource($this->charity_address) : new indexAddressResource($this->user_address),
            'agent'             => $this->when($this->agent,new UserFilterRecource($this->agent)),
            $this->mergeWhen(!$user,[
                'user'          => $this->when($this->user,new UserFilterRecource($this->user)),
            ]),
            $this->mergeWhen($user,[
                'user'          => $this->when($this->delivery_id,new UserFilterRecource($this->delivery)),
            ]),
            'items'           => DetailsRecource::collection($this->orderDetails),
         ];
    }
}
