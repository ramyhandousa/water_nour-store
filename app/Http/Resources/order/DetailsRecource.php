<?php

namespace App\Http\Resources\order;

use App\Http\Resources\product\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DetailsRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'product'       => new ProductResource($this->product),
            'offer_price'  => $this->when($this->offer_price,$this->offer_price),
            'price'         => $this->price,
            'quantity'      => $this->quantity,
        ];
    }
}
