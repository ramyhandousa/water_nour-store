<?php

namespace App\Http\Resources\order;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderFilterRecource extends JsonResource
{

    public function toArray($request)
    {
        $user = $request->user()->defined_user == "user";
        return [
            'id'            => $this->id,
            'total_price'   => $this->total_price,
            'status'        => $this->status,
            'created_at'    =>  $this->created_at,
            $this->mergeWhen(!$user,[
                'user_name'          => $this->when($this->user,$this->user->name),
            ]),
        ];
    }
}
