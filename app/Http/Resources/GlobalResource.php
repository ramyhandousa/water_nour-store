<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class GlobalResource extends JsonResource
{

    public function toArray($request)
    {
        $lang = Config::get('app.locale');
        return [
            'id'        => $this->id,
            'name'      => $lang == 'ar' ? $this->name_ar  : $this->name_en,
            "image"     => $this->when($this->getFirstMediaUrl(), $this->getFirstMediaUrl()),

        ];
    }
}
