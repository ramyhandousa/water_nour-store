<?php

namespace App\Http\Resources\Offers;

use App\Http\Resources\product\productFilterResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class OfferListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->offer->id,
            'price_offer'       => $this->offer->price ,
            'percentage'        => 100 - (number_format( ($this->offer->price /$this->price * 100 ),2)),
            'start_at'          => $this->offer->start_at,
            'end_at'            => $this->offer->end_at,
//            'product'           => new productFilterResource($this)
        ];
    }
}
