<?php

namespace App\Http\Resources\product;

use App\Http\Resources\categoryResource;
use App\Http\Resources\GlobalResource;
use App\Http\Resources\Home\BrandListResource;
use App\Http\Resources\Offers\OfferListResource;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class productFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = Config::get('app.locale');

        $token = $request->bearerToken();
        $user = User::whereApiToken($token)->first();

        $user_id = $token ? $user?$user->id:null : null;
        return [
            "id"                => $this->product->id,
            "agent_id"           => $this->user_id,
            "name"              => $lang == 'ar' ?  optional($this->product)->name_ar : optional($this->product)->name_en,
            "description"       => $lang == 'ar' ?  optional($this->product)->desc_ar : optional($this->product)->desc_en,
            "quantity"          => $this->quantity,
            "price"             => $this->price,
            "image"             => $this->product->getFirstMediaUrl(),
            "category"          => new GlobalResource($this->product->category),
            "brand"             => new GlobalResource($this->product->brand),
            "is_charity"        =>  $this->product->category_id == 1,
            "is_favourite"      => (boolean) $this->product->checkInUserFavourite($user_id),

            $this->mergeWhen($this->offer,[
                'offer'         => $this->when($this->offer,new OfferListResource($this)),
            ]),

//            $this->mergeWhen(!$request->has('offer'),[
                'rate'          => number_format($this->product->rating->average('rate'),0),
                'count_rate'    => $this->product->rating->whereNotNull('rate')->count(),
//            ])

        ];
    }
}
