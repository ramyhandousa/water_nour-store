<?php

namespace App\Http\Resources\product;

use App\Http\Resources\Offers\OfferListResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class listFavouriteResource extends JsonResource
{

    public function toArray($request)
    {
        $lang = Config::get('app.locale');
        return [
            "id"                => $this->product->id,
            "agent_id"           => $this->user_id,
            "name"              => $lang == 'ar' ?  optional($this->product)->name_ar : optional($this->product)->name_en,
             "quantity"          => $this->quantity,
            "price"             => $this->price,
            "image"             => $this->product->getFirstMediaUrl(),
            "is_favourite"      => true,
            "is_charity"        =>  $this->product->category_id == 1,
            $this->mergeWhen($this->offer,[
                'offer' => new OfferListResource($this),
            ])
        ];
    }
}
