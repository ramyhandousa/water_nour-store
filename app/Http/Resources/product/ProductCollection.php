<?php

namespace App\Http\Resources\product;

use App\Models\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class ProductCollection extends ResourceCollection
{
    public $collects = ProductResource::class;

    public function toArray($request)
    {
        return [
            'status' => 200,
            'data' => $this->collection
        ];
    }
}
