<?php

namespace App\Http\Resources\product;

use Illuminate\Http\Resources\Json\JsonResource;

class productCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'user_id'      => optional($this->user)->id,
            'user_name'      => optional($this->user)->name,
            'comment'   => $this->comment,
            'rate'      => $this->rate,
        ];
    }
}
