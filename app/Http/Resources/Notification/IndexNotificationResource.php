<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class IndexNotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'message'   => $this->body,
            'order_id'  => $this->when($this->order_id,$this->order_id),
            'readed'    => (boolean) $this->is_read,
            'date'      => $this->when($this->created_at,$this->created_at) ,
        ];
    }
}
