<?php

namespace App\Http\Middleware;

use Closure;

class loginMiddelware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {

            return response()->json(['status' => 401, 'error' => (array)   trans('global.username_password_notcorrect')], 401);
        }

        return $next($request);
    }
}
