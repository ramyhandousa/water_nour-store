<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Auth\changePassRequest;
use App\Http\Requests\api\Auth\checkActivation;
use App\Http\Requests\api\Auth\editUser;
use App\Http\Requests\api\Auth\emaiRequestVaild;
use App\Http\Requests\api\Auth\login;
use App\Http\Requests\api\Auth\loginDeliveryVaild;
use App\Http\Requests\api\Auth\phoneRequestVaild;
use App\Http\Requests\api\Auth\registerDeliveryVaild;
use App\Http\Requests\api\Auth\resgister;
use App\Http\Requests\api\Product\validImage;
use App\Repositories\AuthRepository;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use RespondsWithHttpStatus;

    private $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;

        $this->middleware('auth:api')->only(['changPassword' ,'editProfile' ,'logOut']);
    }

    public function register(resgister $request){

       $data =  $this->authRepository->register($request);

       return  $this->success(trans('global.register_in_successfully'),$data);
    }


    public function register_delivery(registerDeliveryVaild $request){

        $data =  $this->authRepository->register_delivery($request);

        return  $this->success(trans('global.register_in_successfully'),$data);
    }

    public function login(login $request){

        $data =   $this->authRepository->login($request);

        return  $this->success(trans('global.logged_in_successfully'),$data);
    }

    public function login_delivery(loginDeliveryVaild $request){

        $data =   $this->authRepository->login_delivery($request);

        return  $this->success(trans('global.logged_in_successfully'),$data);
    }

    public function forgetPassword(phoneRequestVaild $request){

        $data =    $this->authRepository->forgetPassword($request);

        return  $this->success(trans('global.activation_code_sent_email'),$data);
    }

    public function resetPassword(phoneRequestVaild $request){

        $data =    $this->authRepository->resetPassword($request);

        return  $this->success($data['message'] );
    }

    public function resendCode(phoneRequestVaild $request){

        $data =    $this->authRepository->resendCode($request);

        return  $this->success(trans('global.activation_code_sent') , $data);
    }

    public function checkCodeActivation(checkActivation $request){

        $data =    $this->authRepository->checkCode($request);

        return  $this->success(trans('global.your_account_was_activated') , $data);
    }

    public function checkCodeCorrect(checkActivation $request){

        return  $this->success("الكود صحيح" );
    }


    public function changPassword ( changePassRequest  $request )
    {
        $data =    $this->authRepository->changPassword($request);

        return  $this->success($data['message'] );
    }


    public function editProfile (editUser $request )
    {
        $data =  $this->authRepository->editProfile($request);

        return  $this->success(trans('global.profile_edit_success') , $data );
    }


    public function logOut(Request $request){

         $this->authRepository->logOut($request);

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.logged_out_successfully') ,
        ] , 200 );
    }

    public function upload(validImage $request){

        $data =  $this->authRepository->uploadImage($request);

        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }
}
