<?php

namespace App\Http\Controllers\Api;

use App\Events\OrderCycle;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\order\OrderRateRequest;
use App\Http\Requests\api\order\OrderStatusRequest;
use App\Http\Requests\api\order\OrderStoreRequest;
use App\Http\Requests\api\order\OrderUpdateRequest;
use App\Http\Requests\api\order\promoCodeVaild;
use App\Http\Requests\api\order\refuseDeliveryUnderway;
use App\Http\Resources\order\OrderFilterRecource;
use App\Http\Resources\order\OrderRecource;
use App\Libraries\PushNotification;
use App\Models\DayWork;
use App\Models\Device;
use App\Models\Order;
use App\Models\OrderDelivery;
use App\Models\OrderDetail;
use App\Models\PricePackage;
use App\Models\Profile;
use App\Models\PromoCode;
use App\Models\Setting;
use App\Scoping\Scopes\OrderByScope;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    use  RespondsWithHttpStatus , paginationTrait;
    public $push;
    public function __construct(PushNotification $push )
    {
        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');

        $this->middleware('auth:api');
        $this->push = $push;
    }


    public function index(Request  $request)
    {
        $user =  $request->user();

        $query = Order::whereUserId($user->id)->withScopes($this->filterQuery());

        $total_count = $query->count();

        $this->pagination_query($request, $query);

        return $this->successWithPagination(trans('global.orders'), $total_count, OrderFilterRecource::collection($query->get()));
    }

    public function list_order_delivery(Request $request){
        $user =  $request->user();

        if (!in_array($request->filter,['accepted','finish','refuse'])){

            $query = OrderDelivery::whereUserId($user->id)->where('is_accepted',0)->whereHas('order',function ($order){
                $order->where('status','pending');
            });

        }else{
            $query = Order::whereDeliveryId($user->id)->withScopes($this->filterQuery());
        }

        $total_count = $query->count();

        $this->pagination_query($request, $query);

        if ( !in_array($request->filter,['accepted','finish','refuse'])) {

            $data = collect($query->get())->pluck('order')->reverse()->values();

        }else{
             $data  = $query->get();
        }
        return $this->successWithPagination(trans('global.orders'), $total_count, OrderFilterRecource::collection($data));
    }

    protected function filterQuery(){
        return [
            'filter' => new OrderByScope(),
        ];
    }

    public function cart_data(Request $request){
        $profile = Profile::whereUserId($request->agent_id)->firstOrFail();

        $tax = Setting::getBody("app_percentage");

        // total_offer
        if ($profile->minimum_order != 0 &&$request->total_price >= $profile->minimum_order){

            $delivery_price = 0;
        }else{
            $delivery_price = $this->calculator_distance($request);
        }
        $data = [
            'tax_number' =>  (string)$profile->tax_number  ,
            'delivery_price' => $delivery_price,
            'tax' => $tax,
        ];

        if ($profile->minimum_order != null){
            $data['free_delivery'] = $profile->minimum_order - $request->total_price;
        }

        return $this->success("بيانات الإضافية",$data);
    }

    public function store(OrderStoreRequest $request)
    {
        $data =collect($request->validated())->except('products')->toArray();

        $order = $request->user()->orders()->create($data);

        OrderDetail::insert($this->map_products($request,$order));

        event(new OrderCycle($order,$request->user(),$request,'new_order'));

        $this->update_quantity_cart($request);

        return $this->success('تم إنشاء طلبك بنجاح',new OrderFilterRecource($order));
    }

    public function update_quantity_cart($request){
        foreach ($request->products as $product){

            $item =  \App\Models\UserProduct::where('user_id','=',$request->agent_id)
                ->where('product_id','=',$product['product_id'])->first();

            $item->decrement('quantity',$product['quantity']);
        }
    }


    public function show(Request $request ,$id)
    {
        $order = Order::findOrFail($id);

        return $this->success('تفاصيل الطلب ',new OrderRecource($order));
    }

    public function accepted(OrderStatusRequest $request){

        $orderDetail = OrderDelivery::whereOrderIdAndUserId($request->orderId,$request->delivery_id)->with('order')->first();

        $order = $orderDetail['order'];

        $orderDetail->update(['is_accepted' => 1]);

        $order->update(['delivery_id' => $request->delivery_id ,'status' => 'accepted']);

        $devices = Device::whereUserId($order->user_id)->pluck('device');
        if (count($devices)){
            $data = [
                'id'            => $order->id,
                'total_price'   => $order->total_price,
                'status'        => $order->status,
                'created_at'    => $order->created_at,
            ];
            $this->push->sendPushNotification($devices,null,trans('global.orders'),trans('global.accepted_order'),[
                'type'          =>  'order',
                "order_data"    => $data
            ]);
        }

        event(new OrderCycle($order,$request->user(),$request,'accepted_delivery'));

        return  $this->success(__('global.accepted_order'));
    }

    public function refuse_delivery(OrderStatusRequest $request){
        $orderDetail = OrderDelivery::whereOrderIdAndUserId($request->orderId,$request->delivery_id)->first();

        $orderDetail->update(['is_accepted' => -1,'message' => $request->message]);


        event(new OrderCycle($orderDetail->order,$request->user(),$request,'refuse_delivery'));

        return  $this->success("تم رفض الطلب");
    }

    public function refuse_delivery_underway(refuseDeliveryUnderway $request){
        $order = Order::whereIdAndDeliveryId($request->orderId,$request->delivery_id)->first();

        $order->update(['status' => "refuse_delivery",'message' => $request->message]);

        $devices = Device::whereUserId($order->user_id)->pluck('device');
        if (count($devices)){
            $data = [
                'id'            => $order->id,
                'total_price'   => $order->total_price,
                'status'        => $order->status,
                'created_at'    => $order->created_at,
            ];
            $this->push->sendPushNotification($devices,null,trans('global.orders'),trans('global.refuse_delivery'),[
                'type'          =>  'order',
                "order_data"    => $data
            ]);
        }

        return  $this->success(" تم رفض الطلب قيد التوصيل");
    }

    public function finish(Request $request){
        $order = Order::findOrFail($request->orderId);

        $order->update(['status' => "finish" ,"message" => null ]);

        $devices = Device::whereUserId($order->user_id)->pluck('device');
        if (count($devices)){
            $data = [
                'id'            => $order->id,
                'total_price'   => $order->total_price,
                'status'        => $order->status,
                'created_at'    => $order->created_at,
            ];
            $this->push->sendPushNotification($devices,null,trans('global.orders'),trans('global.finish_order'),[
                'type'          =>  'order',
                "order_data"    => $data
            ]);
        }


        event(new OrderCycle($order,$request->user(),$request,'finish_order'));

        return  $this->success(" تم توصيل الطلب بنجاح");
    }

    public function promo_code(promoCodeVaild $request){

        $promo_code = PromoCode::whereCode($request->code)->first();

        $orders = Order::whereUserId(Auth::id())->wherePromoCode($request->code)->count();

        if ( Carbon::now() > $promo_code->end_at){
            return  $this->failure('تم إنتهاء صلاحية الكود للإستخدام');
        }

        if ($orders >= $promo_code->times ){
            return  $this->failure('تم إنتهاء صلاحية  عدد مرات الكود للإستخدام');
        }

        $data = ['percent' => $promo_code['percent']];

        return $this->success('بيانات الكود',$data);
    }


    public function map_products($request,$order){

        return  collect($request->products)->map(function ($product) use ($order){
            return [
                'order_id'      => $order->id,
                'product_id'    =>  $product['product_id'],
                'price'         =>  $product['price'],
                'quantity'      =>  $product['quantity'],
                'offer_price'   => isset($product['offer_price']) ? $product['offer_price'] : null
            ];
        })->toArray();
    }


    function calculator_distance($request){

//        $user =  $request->user();

        $agent = User::whereId($request->agent_id)->first();

        $distance =  $this->distance($request->latitude ,$request->longitude,$agent->latitude ,$agent->longitude,"K");

        $package_price = PricePackage::where('kilometer','>=', number_format($distance,0))->first();

        $last_kilo_meter = PricePackage::orderBy('kilometer', 'desc')->first();

        return $package_price ? $package_price->price : $last_kilo_meter->price;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

}
