<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Notification\IndexNotificationResource;
use App\Models\Notification;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    use RespondsWithHttpStatus ,paginationTrait;

    public function __construct( )
    {

        $this->middleware('auth:api');
    }

    public function index(Request $request){
        $user = $request->user();

        if ($user->defined_user == "user"){

            $topic = "all_users";
        }else{

            $topic = "all_deliveries";
        }

        $query = Notification::whereUserId($user->id)->orWhere('topic',$topic)->where('created_at','>=',$user->created_at);

        $total_count = $query->count();

        $this->pagination_query($request, $query);

        $data = IndexNotificationResource::collection($query->get());

        $query->get()->each->update(['is_read' => true]);

        return $this->successWithPagination("الإشعارات",$total_count,$data );
    }


}
