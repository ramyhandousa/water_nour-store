<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\DealerRepository;
use Illuminate\Http\Request;

class DealerController extends Controller
{
    protected  $dealer;

    function __construct(DealerRepository $dealer)
    {
//        $this->middleware('auth:api');

        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');

        $this->dealer = $dealer;
    }

    public function index(Request $request){
        return $this->dealer->list($request);
    }

    public function show($id){
        return $this->dealer->show($id);
    }
}
