<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\cart\CartStoreRequest;
use App\Http\Requests\api\cart\CartUpdateRequest;
use App\Http\Resources\cart\CartCollection;
use App\Models\Product;
use App\Repositories\CartRepository;
use Illuminate\Http\Request;

class CartController extends Controller
{

    protected  $cart;

    function __construct(CartRepository $cart)
    {
        $this->middleware('auth:api');

        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');

        $this->cart = $cart;
    }

    public function index(Request $request)
    {
        $this->cart->sync();

        return $this->responseCart($request);
    }


    public function store(CartStoreRequest $request)
    {
        $this->cart->add($request->products);

        return $this->responseCart($request);
    }

    public function update( Product $product , CartUpdateRequest $request)
    {
        $this->cart->update($product,$request->quantity);

        return $this->responseCart($request);
    }


    public function destroy(Product $product , Request $request)
    {
        $this->cart->delete($product->id);

        return $this->responseCart($request);
    }

    protected function responseCart($request){

        $user = $request->user()->load('cart');

        return (new CartCollection($user['cart']) )->additional(['status' => 200 ,'meta' => $this->meta()]);
    }

    protected function meta(){

        return [
            'change' => $this->cart->hasChange(),
            'isEmpty' => $this->cart->isEmpty(),
            'total' => $this->cart->total(),
        ];
    }

    }
