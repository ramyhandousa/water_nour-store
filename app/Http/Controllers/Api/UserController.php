<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\Auth\updateAddress;
use App\Http\Resources\UserRecource;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use  RespondsWithHttpStatus;
    public function __construct(  )
    {
        $this->middleware('auth:api');

        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');
    }

    public function user_info(){
        $user = Auth::user();

        return $this->success("بيانات  ",new UserRecource($user));
    }

    public function updateAddress(updateAddress $request){
        $user = Auth::user();

        $user->update(['city_id' => $request->city_id , 'address' => $request->address]);

        $data = new UserRecource($user);
        return response()->json( [
            'status' => 200 ,
            'message' =>    trans('global.logged_in_successfully'),
            'data'      => $data
        ] , 200 );
    }

    public function rateDealer(Request $request){

        $user = Auth::user();

        $dealer = User::whereId($request->dealerId)->first();

        $rate =  $dealer->rating->where('id', $user->id)->first();

        if ($rate){

            $rate->pivot->update(['rate' => $request->rate]);
        }else{

            $dealer->rating()->attach( $user->id ,['dealer_id' => $dealer->id , 'rate' => $request->rate]);
        }

        return response()->json( [
            'status' => 200 ,
            'message' =>    trans('global.rate_success'),
        ] , 200 );
    }





}
