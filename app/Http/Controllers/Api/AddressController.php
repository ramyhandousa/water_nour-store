<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\users\address\storeAddressVaild;
use App\Http\Resources\UserAddresseResource;
use App\Http\Resources\users\address\indexAddressResource;
use App\Http\Resources\users\address\showAddressResource;
use App\Models\Address;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{
    use RespondsWithHttpStatus , paginationTrait;
    function __construct( )
    {
        $this->middleware('auth:api');

        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');

    }

    public function index(Request  $request){
        $address = Address::whereUserId(Auth::id())->where('is_deleted',0);

        if ($request->cityId){
            $address->whereCityId($request->cityId);
        }

        $this->pagination_query($request,$address);

        $data = showAddressResource::collection($address->get());

        return $this->success("العناوين الخاصة بك",$data);
    }

    public function show(Address $address){

        return $this->success('تفاصيل العنوان', new showAddressResource($address));
    }

    public function store(storeAddressVaild  $request){

        $data = $request->user()->address()->create($request->validated());

        return $this->success('تم بنجاح', new showAddressResource($data));
    }

    public function update(storeAddressVaild $request, Address $address){

        $address->update($request->validated());

        return $this->success('تم بنجاح', new showAddressResource($address));
    }


    public function destroy(Address $address){

        $address->update(['is_deleted' => 1]);

        return $this->success('تم مسح العنوان بنجاح'  );
    }

    public function update_is_default(Request $request,Address $address){

         $request->user()->address()->update(['is_default' => 0]);

         $address->update(['is_default' => 1]);

        return $this->success('تم بنجاح', new showAddressResource($address));
    }
}
