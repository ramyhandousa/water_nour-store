<?php

namespace App\Http\Controllers\Api;

use App\Events\ContactUs;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\setting\bankTransferRequest;
use App\Http\Requests\api\setting\contactVaild;
use App\Http\Resources\setting\BankResource;
use App\Http\Resources\setting\typeSupportTypes;
use App\Models\Bank;
use App\Models\BankTransfer;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypeSupport;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{

    use RespondsWithHttpStatus;
    public $language;
    public function __construct( )
    {
        $language = request()->headers->get('Accept-Language') ? : 'ar';
        app()->setLocale($language);
        $this->language = $language;

        $this->middleware('auth:api')->only(['contact_us']);
    }



    public function aboutUs(){
        $setting = Setting::all();
        $faceBook =  $setting->where('key','faceBook')->first();
        $twitter =  $setting->where('key','twitter')->first();
        $instagram =  $setting->where('key','instagram')->first();
        $youtube =  $setting->where('key','youtube')->first();
        $phone_contact =  $setting->where('key','phone_contact')->first();
        $contactus_email =  $setting->where('key','contactus_email')->first();
        $about_us =  $setting->where('key','about_us_' . $this->language)->first();

        $data = [
            'phone_contact' => $phone_contact ? $phone_contact->body : '',
            'contactus_email' => $contactus_email ? $contactus_email->body : '',
            'about_us' => $about_us ? $about_us->body : '',
            'faceBook' => $faceBook ? $faceBook->body : '',
            'twitter' => $twitter ? $twitter->body : '',
            'instagram' => $instagram ? $instagram->body : '',
            'youtube' => $youtube ? $youtube->body : '',
        ];
        return response()->json( [
            'status' => 200 ,
            'data' => $data  ,
        ] , 200 );

    }

    public function terms_user(){

        $setting =  Setting::getBody('terms_user_' . $this->language);

        return response()->json( [
            'status' => 200 ,
            'data' => $setting ,
        ] , 200 );

    }

    public function terms_delivery(){

        $setting =  Setting::getBody('terms_delivery_' . $this->language);

        return response()->json( [
            'status' => 200 ,
            'data' => $setting ,
        ] , 200 );
    }

    public function getTypesSupport(){
        $supports = TypeSupport::select('id','name_'.$this->language)->get();

        return response()->json( [
            'status' => 200 ,
            'data' =>  typeSupportTypes::collection($supports) ,
        ] , 200 );
    }


    public function contact_us(contactVaild $request ){
        $user = Auth::user();

        $support = new Support();
        $support->user_id = 1;
        $support->sender_id = $user->id;
        $support->type_id = (int) $request->type;
        $support->message = $request->message;
        $support->save();

        event(new ContactUs(1,$user->id,$request));

        return $this->success( trans('global.message_was_sent_successfully'));
    }

}
