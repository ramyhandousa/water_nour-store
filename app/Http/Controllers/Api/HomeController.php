<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\Product;
use App\Repositories\HomeRepository;
use App\Traits\RespondsWithHttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use RespondsWithHttpStatus;
    protected  $homeRepository;

    public function __construct(HomeRepository $homeRepository )
    {
        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');

        $this->homeRepository = $homeRepository;
    }


    public function car_types(Request $request){

        $data =    $this->homeRepository->car_types($request);

        return $this->success("نوع السيارة  ",$data);
    }

    public function brands(Request $request){

        $data =    $this->homeRepository->list_brands($request);

        return $this->successWithPagination("العلامات التجارية ",$data['total_count'],$data['data']);
    }


    public function search_products(Request $request){

        $data =    $this->homeRepository->search_products($request);

        return $this->successWithPagination("فلتر ",$data['total_count'],$data['data']);
    }

   public function cities(Request $request){

       $data =    $this->homeRepository->listCity($request);

        return $this->success("cities",$data);
   }

   public function categories(Request $request){

       $data =    $this->homeRepository->listCategory($request);

       return $this->success("categories",$data);
   }

   public function charity_addresses(Request $request){

       $data =    $this->homeRepository->charity_addresses($request);

       return $this->success("عنواين الأعمال الخيري",$data);
   }

   public function user_addresses(Request $request){

       $data =    $this->homeRepository->addresses($request);

       return $this->success("عنواين المستخدم ",$data);
   }
}
