<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\order\OrderRateRequest;
use App\Http\Requests\api\Product\validImage;
use App\Http\Requests\api\Product\validProductRequest;
use App\Http\Resources\Home\BrandListResource;
use App\Http\Resources\product\imageRecource;
use App\Http\Resources\product\listFavouriteResource;
use App\Http\Resources\product\productCommentResource;
use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\UserFavourite;
use App\Models\UserProduct;
use App\Models\UserRate;
use App\Policies\ProductPolicy;
use App\Repositories\ProductRepository;
use App\Scoping\Scopes\CityProductScope;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    use  RespondsWithHttpStatus ,paginationTrait;
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');

        $this->productRepository = $productRepository;

        $this->middleware('auth:api')->except(['show','list_comments']);

    }


    public function show(Request $request,$id)
    {
        $data =   $this->productRepository->show($id);

        return $this->success('تفاصيل المنتج',$data);
    }

    public function list_comments(Request $request,Product $product){

        $data =   $this->productRepository->list_rate_and_comments($request,$product);

        return $this->success('التعليقات',$data);
    }


    public function make_rate(OrderRateRequest  $request, Product $product){

       $rate =  UserRate::updateOrCreate(
            ['user_id' => $request->user()->id,'agent_id' => $request->agent_id,'product_id' => $product->id],
            ['rate' => $request->rate ,'comment' => $request->comment,'product_id' => $product->id]);
        return $this->success('تم التقييم بنجاح',new productCommentResource($rate));
    }


    public function make_favourite_product(Request  $request, Product $product){

        $favourite = UserFavourite::whereUserIdAndProductId($request->user()->id,$product->id)->first();

        if ($favourite){
            $favourite->delete();

            $message = "مسح";
        }else{
            UserFavourite::create(['user_id' => $request->user()->id,'product_id' => $product->id]);

            $message = "مسح";
        }

        return $this->success('تم '.$message.' بنجاح');
    }


    public function make_favourite_brand(Request  $request,Brand $brand){

        $favourite = UserFavourite::whereUserIdAndBrandId($request->user()->id,$brand->id)->first();

        if ($favourite){
            $favourite->delete();
            $message = "مسح";
        }else{
            UserFavourite::create(['user_id' => $request->user()->id,'brand_id' => $brand->id]);

            $message = "الإضافة";
        }

        return $this->success('تم '.$message.' بنجاح');
    }


    public function list_favourite_products(Request $request){
        $products =  $this->productRepository->list_favourite_products($request);

        return $this->success("مفضلة المنتجات", $products);
    }

    public function list_favourite_brands(Request $request){
        $data =   $request->user()->brand_favourites;

        return $this->success("مفضلة الماركات", BrandListResource::collection($data));
    }


    protected function filterQuery(){
        return [
            'city'          => new CityProductScope(),
        ];
    }

}
