<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Agent\StoreOfferProduct;
use App\Models\Offer;
use App\Models\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AgentProductOfferController extends Controller
{
    public function index(Request  $request){

        $user = Auth::user();

        $offers = Offer::join('user_product','offers.product_id','=','user_product.product_id')
            ->join('products','offers.product_id','=','products.id')
            ->select('offers.id','offers.user_id','offers.price as offer_price','offers.start_at','offers.end_at','offers.is_suspend'
                ,'user_product.price as product_price', 'products.name_ar',
                DB::raw('round( ( offers.price /user_product.price * 100 ),2) as percentage'))
            ->where('offers.end_at','>=',Carbon::now())->where('user_product.user_id','=',$user->id)
            ->where('offers.user_id','=',$user->id)->get();

        $pageName = 'العروض';

        return view('admin.agent_product_offers.index',compact('offers','pageName'));
    }


    public function create(){

        $user = Auth::user();

        $products = $user->products->where('is_suspend',0)->where('is_deleted',0)
            ->where('pivot.is_suspend',0)->where('pivot.is_deleted',0);

        if ($products->count() == 0){
            session()->flash('myErrors','للاسف لا يوجد منتجات حالية للإضافة تواصل مع الإدارة');
            return  redirect()->back();
        }

        return view('admin.agent_product_offers.create',compact('products'));
    }


    public function store(StoreOfferProduct  $request){

        $user = Auth::user();

        offer::updateOrCreate(['user_id' => $user->id,'product_id' => $request->product_id],$request->validated());

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'المنتج']),
            "url" => route('agent_products.index'),
        ]);
    }

    public function edit($id){
        $user = Auth::user();

        $products = $user->products->where('is_suspend',0)->where('is_deleted',0)
            ->where('pivot.is_suspend',0)->where('pivot.is_deleted',0);

        if ($products->count() == 0){
            session()->flash('myErrors','للاسف لا يوجد منتجات حالية للإضافة تواصل مع الإدارة');
            return  redirect()->back();
        }

        $offer = Offer::findOrFail($id);

        return view('admin.agent_product_offers.edit',compact('products','offer'));
    }

    public function update(StoreOfferProduct  $request){

        $user = Auth::user();

        offer::updateOrCreate(['user_id' => $user->id,'product_id' => $request->product_id],$request->validated());

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => "المنتج"]),
            "url" => route('agent_product_offer.index'),
        ]);
    }


    public function suspend(Request $request)
    {
        $model = Offer::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {
            $message = "لقد تم حظر العرض بنجاح";
        } else {
            $message = "لقد تم فك الحظر على العرض بنجاح";
        }
        $model->save();
        return response()->json([
            'status' => true,
            'message' => $message,
            'id' => $request->id,
            'type' => $request->type

        ]);

    }
}
