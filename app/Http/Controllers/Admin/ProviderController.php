<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;use Illuminate\Support\Facades\Gate;


class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $quary = User::where('defined_user','provider')->whereDoesntHave('roles')->whereDoesntHave('abilities');

        if ($request->type == 'new'){

            $quary->where('is_active', 1)->where('is_accepted', 0);

            $pageName = 'إدارة الطلبات جديدة';

        }elseif ($request->type == 'refuse'){

            $quary->where('is_active', 1)->where('is_accepted', -1);

            $pageName = 'مزودي الخدمات المرفوضين';

        }else{

            $quary->where('is_active', 1)->where('is_accepted', 1);

            $pageName = 'مزودي الخدمات المقبولين';
        }

        $users =  $quary->latest()->get();

        return view('admin.providers.index', compact('users' , 'pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('products.images','categories','my_works.images')->findOrFail($id);

        $products = $user['products'];
        $categories = $user['categories'];
        $my_works = $user['my_works'];

        return view('admin.providers.show', compact('user','products','categories','my_works' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function suspend(Request $request)
    {
        $model = User::findOrFail($request->id);
        $model->is_suspend = $request->type;
        $model->message = $request->message;
        if ($request->type == -1) {

            $userPending = User::whereId($model->id)->whereHas('provider_products',function ($q) use ($model){
                    $q->where('status' ,'pending')->orWhere('status' ,'accepted');

            })->orWhere('id',$model->id)->whereHas('provider_projects',function ($q)use ($model){

                    $q->where('status' ,'pending')->orWhere('status' ,'accepted');

            })->first();


            if (  $userPending  ){
                session()->flash('myErrors',"عفواً,يوجد لهذا المستخدم  طلبات حالية");
                return redirect()->back();
            }
            $message = "لقد تم حظر  بنجاح";

        } else {
            $message = "لقد تم فك الحظر بنجاح";
        }

        if ($model->save()) {
            session()->flash('success',$message);
            return redirect()->back();
        }

    }

}
