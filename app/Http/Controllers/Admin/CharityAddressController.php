<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCharity;
use App\Models\CharityAddress;
use App\Models\City;
use Illuminate\Http\Request;

class CharityAddressController extends Controller
{

    public function index()
    {
        $charity_addresses = CharityAddress::whereIsDeleted(0)
            ->join('cities', 'charity_addresses.city_id', '=', 'cities.id')
            ->select('charity_addresses.id','address','charity_addresses.is_suspend', 'cities.name_ar as city_name')
            ->get();

        $pageName = 'إدارة العنوانين الخيرية ';

        return view('admin.charity_addresses.index',compact('charity_addresses','pageName'));
    }

    public function create()
    {
        $cities = City::whereIsSuspend(0)->latest()->get();
        $pageName = 'إضافة عنوان';

        return view('admin.charity_addresses.create', compact('cities', 'pageName'));
    }


    public function store(StoreCharity $request)
    {
        CharityAddress::create($request->validated());

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => "العنوان"]),
            "url" => route('charity_addresses.index'),
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $charityAddress = CharityAddress::findOrFail($id);

        $cities =  City::whereIsSuspend(0)->latest()->get();

        $pageName = 'تعديل عنوان خيري';

        return view('admin.charity_addresses.edit', compact('charityAddress','cities', 'pageName'));

    }


    public function update(StoreCharity $request, $id)
    {
        $charityAddress = CharityAddress::findOrFail($id);

        $charityAddress->update($request->validated());

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => "العنوان"]),
            "url" => route('charity_addresses.index'),
        ]);
    }


    public function delete(Request $request)
    {
        $charityAddress = CharityAddress::findOrFail($request->id);

        $charityAddress->update(['is_deleted' => 1]);

        return response()->json([
            'status' => true,
            'data' => $charityAddress->id
        ]);
    }


    public function suspend(Request $request)
    {
        $model = CharityAddress::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر العنوان بنجاح";
        } else {
            $message = "لقد تم فك الحظر على العنوان بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }

}
