<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\StoreProductVaild;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(){
        $products = Product::join('categories', 'products.category_id', '=', 'categories.id')
            ->join('brands', 'products.brand_id', '=', 'brands.id')
            ->select('products.id','products.name_ar','products.is_suspend','products.created_at',
                    'categories.name_ar as category_name','brands.name_ar as brand_name')
            ->where('products.is_deleted',0)
            ->latest()
            ->get();

        $pageName = 'إدارة المنتجات ';

        return view('admin.products.index',compact('products','pageName'));
    }

    public function create(){
        $brands     = Brand::whereIsDeletedAndIsSuspend(0,0)->get();
        $categories = Category::whereIsDeletedAndIsSuspend(0,0)->get();
        $pageName   = '   اسم المنتج ';
        return view('admin.products.create',compact('brands','categories','pageName'));
    }

    public function edit($id){
        $product = Product::findOrFail($id);
        $brands     = Brand::whereIsDeletedAndIsSuspend(0,0)->get();
        $categories = Category::whereIsDeletedAndIsSuspend(0,0)->get();
        $pageName   = '   اسم المنتج ';
        return view('admin.products.edit',compact('product','brands','categories','pageName'));
    }

    public function store(StoreProductVaild  $request){
         $data =  collect($request->validated())->except('image')->toArray();

         $product = Product::create($data);

         if ($request->hasFile('image')):
             $product->clearMediaCollection();
             $product->addMediaFromRequest('image')->toMediaCollection();
             $product->save();
         endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'المنتج']),
            "url" => route('products.index'),
        ]);
    }

    public function update(StoreProductVaild $request , $id){
        $product = Product::findOrFail($id);
        $data =  collect($request->validated())->except('image')->toArray();

        $product->update($data);
        if ($request->hasFile('image')):
            $product->clearMediaCollection();
            $product->addMediaFromRequest('image')->toMediaCollection();
            $product->save();
        endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'المنتج']),
            "url" => route('products.index'),
        ]);
    }


    public function delete(Request $request){


        $model = Product::findOrFail($request->id);

        if ($model->update(['is_deleted' => 1])) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }


    public function suspend(Request $request)
    {
        $model = Product::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر المنتج بنجاح";
        } else {
            $message = "لقد تم فك الحظر على المنتج بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type
            ]);
        }

    }

}
