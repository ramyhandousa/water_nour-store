<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CarType;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function index( )
    {
        $categories = CarType::latest()->get();
        $pageName = 'إدارة السيارات ';

        return view('admin.cars.index',compact('categories','pageName'));
    }
    public function create()
    {
        $pageName = ' اسم السيارة ';

        return view('admin.cars.create')->with(compact('pageName'));
    }

    public function store(Request $request)
    {
        $category = new CarType;
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;

        if ($category->save()) {

            return response()->json([
                'status' => true,
                "message" => __('trans.addingSuccess',['itemName' =>  ' السيارة  ']),
                "url" => route('cars.index'),
            ]);
        }

    }

    public function edit($id)
    {
        $category = CarType::findOrFail($id);

        $pageName = 'السيارة';

        return view('admin.cars.edit')->with(compact('category',  'pageName' ));
    }


    public function update(Request $request, $id)
    {
        $category = CarType::findOrFail($id);
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;

        if ($category->save()) {
            return response()->json([
                'status' => true,
                "message" => __('trans.editSuccess',['itemName' => "السيارة"]),
                "url" =>  route('cars.index'),

            ]);
        }

    }

    public function delete(Request $request){
        $model = CarType::findOrFail($request->id);

        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }

    public function suspend(Request $request)
    {
        $model = CarType::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر السيارة بنجاح";
        } else {
            $message = "لقد تم فك الحظر على السيارة بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }

}
