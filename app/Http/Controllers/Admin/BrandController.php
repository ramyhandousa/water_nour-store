<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\vaildImage;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/categories/';
    }

    public function index(Request $request)
    {

        $brands = Brand::whereIsDeleted(0)->latest()->get();
        $pageName = 'إدارة الماركات ';

        return view('admin.brands.index',compact('brands','pageName'));
    }
    public function create()
    {

        $pageName = '   اسم الماركة ';

        return view('admin.brands.create')->with(compact('pageName'));
    }

    public function store(vaildImage $request)
    {
        $category = new Brand;
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;
        if ($request->hasFile('image')):
            $category->clearMediaCollection();
            $category->addMediaFromRequest('image')->toMediaCollection();
        endif;
        if ($category->save()) {
            $url =  route('brands.index');
            $name = ' الماركة  ';
            return response()->json([
                'status' => true,
                "message" => __('trans.addingSuccess',['itemName' => $name]),
                "url" => $url,

            ]);
        }

    }


    public function edit($id)
    {
        $category = Brand::findOrFail($id);

        $pageName = 'الماركة';

        return view('admin.brands.edit')->with(compact('category',  'pageName' ));
    }


    public function update(vaildImage $request, $id)
    {

        $category = Brand::findOrFail($id);
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;
        if ($request->hasFile('image')):
            $category->clearMediaCollection();
            $category->addMediaFromRequest('image')->toMediaCollection();
        endif;
        if ($category->save()) {

            $url =  route('brands.index');
            $name = 'الماركة';

            return response()->json([
                'status' => true,
                "message" => __('trans.editSuccess',['itemName' => $name]),
                "url" => $url,

            ]);
        }

    }



    public function delete(Request $request){


        $model = Brand::findOrFail($request->id);

        if ($model->update(['is_deleted' => 1])) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }

    public function suspend(Request $request)
    {
        $model = Brand::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر الماركة بنجاح";
        } else {
            $message = "لقد تم فك الحظر على الماركة بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }

}
