<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\City;
use App\Models\Countery;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class CitiesController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $cities = City::latest()->get();

        $pageName = 'المدن';

        return view('admin.cities.index',compact('cities','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {

        $pageName = 'المدن';

        return view('admin.cities.create',compact('pageName'));
    }

    public function store(Request $request)
    {

        $city = new City;
        $city->name_ar = $request->name_ar;
        $city->name_en = $request->name_en;
        $city->save();

        $url =  route('cities.index');
        $name = 'المدينة';

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => $name]),
            "url" => $url,

        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Request $request)
    {
        $city = City::findOrFail($id);

        $pageName = 'المدينة';

        return view('admin.cities.edit',compact('city','pageName'));
    }


    public function update(Request $request, $id)
    {

        $city = City::findOrFail($id);
        $city->name_ar = $request->name_ar;
        $city->name_en = $request->name_en;
        $city->save();

        $url =  route('cities.index');
        $name = 'المدينة';

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => $name]),
            "url" => $url,

        ]);
    }

    public function delete(Request $request){

        $model = City::findOrFail($request->id);

        if (count($model->agents) > 0) {

            return response()->json([
                'status' => false,
                'message' => "عفواً, لا يمكنك حذف المدينة نظراً لوجود وكلاء  فيها"
            ]);
        }

        if (count($model->products) > 0) {

            return response()->json([
                'status' => false,
                'message' => "عفواً, لا يمكنك حذف المدينة نظراً لوجود منتجات  فيها"
            ]);
        }

        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }



    public function suspend(Request $request)
    {
        $model = City::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم الحظر على مستوي النظام بنجاح";

        } else {
            $message = "لقد تم فك الحظر  بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }


}
