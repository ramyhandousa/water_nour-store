<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OfferProductController extends Controller
{


    public function index(){

        $offers = Offer::join('user_product', function ($join) {
                $join->on('offers.product_id', '=', 'user_product.product_id')->whereColumn([
                    ['offers.user_id', '=', 'user_product.user_id']
                ]);
            })
            ->join('products','offers.product_id','=','products.id')
            ->join('users','offers.user_id','=','users.id')
            ->select('offers.id','offers.price as offer_price','offers.start_at','offers.end_at'
                 ,'user_product.price as product_price', 'users.name as user_name', 'products.name_ar',
                 DB::raw('round( ( offers.price /user_product.price * 100 ),2) as percentage'))
             ->where('offers.end_at','>=',Carbon::now())->get();

         $pageName = 'العروض';

        return view('admin.offers.index',compact('offers','pageName'));
    }


    public function suspend(Request $request)
    {
        $model = Offer::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر العرض بنجاح";
        } else {
            $message = "لقد تم فك الحظر على العرض بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type
            ]);
        }

    }
}
