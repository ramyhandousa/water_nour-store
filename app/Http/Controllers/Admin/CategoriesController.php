<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CategoryRequestVaild;
use App\Http\Requests\vaildImage;
use App\Models\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Validator;
use UploadImage;


class CategoriesController extends Controller
{

    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/categories/';
    }

    public function index(Request $request)
    {

	     $categories = Category::orderBy('order_by')->get();
	     $pageName = 'إدارة الاقسام ';

        return view('admin.categories.index',compact('categories','pageName'));
    }
    public function create()
    {

        $pageName = '   اسم القسم ';

        return view('admin.categories.create')->with(compact('pageName'));
    }

    public function store(CategoryRequestVaild $request)
    {
        $category = new Category;
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;
        $category->order_by = $request->order_by;
        if ($request->hasFile('image')):
            $category->clearMediaCollection();
            $category->addMediaFromRequest('image')->toMediaCollection();
        endif;
        if ($category->save()) {
            $url =  route('categories.index');
            $name = ' القسم  ';
            return response()->json([
                'status' => true,
                "message" => __('trans.addingSuccess',['itemName' => $name]),
                "url" => $url,
            ]);
        }

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $category = Category::findOrFail($id);

        $pageName = 'القسم';

        return view('admin.categories.edit')->with(compact('category',  'pageName' ));
    }


    public function update(CategoryRequestVaild $request, $id)
    {

        $category = Category::findOrFail($id);
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;
        $category->order_by = $request->order_by;
        if ($request->hasFile('image')):
            $category->clearMediaCollection();
            $category->addMediaFromRequest('image')->toMediaCollection();
        endif;
        if ($category->save()) {

            $url =  route('categories.index');
            $name = 'القسم';

            return response()->json([
                'status' => true,
                "message" => __('trans.editSuccess',['itemName' => $name]),
                "url" => $url,

            ]);
        }

    }



       public function delete(Request $request){
	    if (!Gate::allows('settings_manage')) {
		 return abort(401);
	    }

	    $model = Category::findOrFail($request->id);

	    if (count($model->products) > 0) {

		 return response()->json([
		         'status' => false,
		         'message' => "عفواً, لا يمكنك حذف التخصص نظراً لوجود منتجات  فيها"
		 ]);
	    }


	    if ($model->delete()) {
             return response()->json([
                     'status' => true,
                     'data' => $model->id
             ]);
	    }

       }

       public function suspend(Request $request)
       {
            $model = Category::findOrFail($request->id);
            $model->is_suspend = $request->type;
            if ($request->type == 1) {

                $message = "لقد تم حظر القسم بنجاح";
            } else {
                $message = "لقد تم فك الحظر على القسم بنجاح";
            }

            if ($model->save()) {
             return response()->json([
                     'status' => true,
                     'message' => $message,
                     'id' => $request->id,
                     'type' => $request->type

             ]);
            }

       }



}
