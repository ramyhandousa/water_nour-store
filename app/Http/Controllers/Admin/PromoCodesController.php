<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PromoCodeVaild;
use App\Models\PromoCode;
use Illuminate\Http\Request;

class PromoCodesController extends Controller
{
    public function index(){
        $codes = PromoCode::where('is_deleted',0)->get();

        $pageName = 'إدارة أكواد الخصم';

        return view('admin.promo_codes.index',compact('codes', 'pageName'));
    }

    public function create(){
        $pageName = 'إضافة كواد الخصم';
        return view('admin.promo_codes.create',compact('pageName'));
    }

    public function store(PromoCodeVaild  $request){

        PromoCode::create($request->validated());
        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'الكود']),
            "url" => route('promo_codes.index'),
        ]);
    }


    public function edit($id){

        $promo_code = PromoCode::findOrFail($id);

        $pageName = 'تعديل  كود الخصم';

        return view('admin.promo_codes.edit',compact('pageName','promo_code'));
    }

    public function update(PromoCodeVaild  $request , $id){
        $promo_code = PromoCode::findOrFail($id);

        $promo_code->update($request->validated());

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'الكود']),
            "url" => route('promo_codes.index'),
        ]);
    }



    public function delete(Request  $request){

        $promo_code = PromoCode::findOrFail($request->id);

        $promo_code->update(['is_deleted' => 1]);

        return response()->json([
            'status' => true,
            'data' => $promo_code->id
        ]);
    }

    public function suspend(Request $request)
    {
        $model = PromoCode::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر كود الخصم بنجاح";
        } else {
            $message = "لقد تم فك الحظر على  كود الخصم بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }

}
