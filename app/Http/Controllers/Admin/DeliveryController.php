<?php

namespace App\Http\Controllers\Admin;

use App\Events\RegisterDelivery;
use App\Http\Controllers\Controller;
use App\Libraries\PushNotification;
use App\User;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{

    public $push;
    public function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    public function index(Request  $request)
    {
        $query = User::where('defined_user','delivery');

        if (isset($request->is_accepted)){
            $query->whereIsActiveAndIsAccepted(1,0);
            $pageName  = 'طلبات الإنضمام';
        }else{
            $query->whereIsActiveAndIsAccepted(1,1);
            $pageName  = 'المناديب لدينا';
        }

        $delivery =  $query->get();

        return view('admin.delivery.index',compact('delivery','pageName'));
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $user = User::with('profile.media')->findOrFail($id);

        $image_profile = $user->imageProfile('profile');
        $hobby_picture = $user->imageProfile('hobby_picture');
        $license = $user->imageProfile('license');
        $vehicle_investment = $user->imageProfile('vehicle_investment');

        return view('admin.delivery.show',compact('user','image_profile','hobby_picture','license','vehicle_investment'));
    }




    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function accpetedUser(Request $request){

        $user = User::with('devices')->findOrFail($request->id);

        if ($user){
            $user->update(['is_accepted' => 1]);

            $devices = $user['devices']->pluck('device');
            $this->push->sendPushNotification($devices,null,' الإدارة',  ' تم قبول طلبك من الإدارة  ' ,[
                'type' =>  'refresh_auth'
            ]);


            event(new RegisterDelivery($user,$request,'register_delivery_to_agent'));

            return response()->json( [ 'status' => true  ] , 200 );

        }else{

            return response()->json( [   'status' => false  ] , 200 );
        }
    }


    public function refuseUser(Request $request){
        $user = User::findOrFail($request->id);

        if ($user){

            $user->update(['is_accepted' => -1]);
//            $user->delete();
        }
        return response()->json( [  'status' => true , ] , 200 );
    }
}
