<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index(){
//        $subscriptions = Subscription::with('user')->latest()->get();
        $subscriptions = User::whereHas('subscription')->with('subscription')->get()->pluck('subscription');

        $pageName = 'الإشترااكات';

        return  view('admin.Subscription.index',compact('subscriptions','pageName'));
    }


    public function accepted(Request $request){

        $now = Carbon::now();
        $end = $now->addMonth(3);

        $new_subscriptions = new Subscription();
        $new_subscriptions->user_id = $request->id;
        $new_subscriptions->start_at = $now;
        $new_subscriptions->end_at = $end;
        $new_subscriptions->save();

        return response()->json([
            'status'=>true,
            'title'=>"نجاح",
            'message'=>"تم تجديد الإشتراك للتاجر بنجاح",
        ]);

    }
}
