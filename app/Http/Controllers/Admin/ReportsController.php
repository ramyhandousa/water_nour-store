<?php

namespace App\Http\Controllers\Admin;

use App\Exports\InvoiceExport;
use App\Models\BankTransfer;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderProject;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class ReportsController extends Controller
{

    public function index(Request $request)
    {
        if ($request->type == 'projects'){

            $pageName = 'تقارير طلبات المشاريع ';
            $reports = User::where(function ($q) use ($request){
                                    $q->where('name','like', '%' . $request->name . '%')
                                    ->orWhere('phone','like', '%' . $request->name . '%');
                            })->whereHas('provider_projects')
                            ->latest()->get();

        }else{
            $pageName = 'تقارير طلبات المنتجات ';

            $reports = User::where(function ($q) use ($request){
                    $q->where('name','like', '%' . $request->name . '%')
                        ->orWhere('phone','like', '%' . $request->name . '%');
                })
                ->whereHas('provider_products')
                ->latest()->get();
        }

        return view('admin.Reports.index', compact('reports','pageName'));
    }


    public function gym_data(Request $request)
    {
        $id = $request->id;

        $filter =function ($q){
            $q->select('id','name');
        };
        if ($request->type == 'projects'){

            $pageName = 'تفاصيل طلبات المشاريع ';

            $reports = OrderProject::where('provider_id',$id)->with(['user' => $filter])->latest()->get();

        }else{

            $pageName = 'تفاصيل طلبات المنتجات ';

            $reports = OrderProduct::where('provider_id',$id)->with(['user' => $filter])->latest()->get();

        }

        $reports->map(function ($q){
            $q->adminPrice =  ($q->price * $q->app_percentage ) /100;
            $q->providerPrice = $q->price -  $q->adminPrice;
        });

        $provider = $reports->pluck('provider')->first();

        $payForProvider = $reports->sum('providerPrice');

        return view('admin.Reports.show', compact('reports','pageName','provider' , 'payForProvider'));
    }

    public function invoices_orders(){

      $users =  User::whereHas('orders_agent',function ($order){
          $order->where('status','finish');
      })->select('id','name','email','phone')->withCount('orders_agent_finsih')->get();


       return view('admin.Reports.invoices',compact('users'));
    }

    public function invoices_orders_show(User $user){

        $reports = Order::whereAgentId($user->id)->whereStatus('finish')->get();

        $reports->map(function ($q){
            $q->admin_price =  $q->tax != 0 ?  (  $q->total_price  * $q->tax )  /100 :  0;
            $q->agent_price =  $q->total_price - $q->admin_price ;
        });

        $admin_price = $reports->sum('admin_price');
        $agent_price = $reports->sum('agent_price');

        return view('admin.Reports.invoices_show',compact('reports','admin_price','agent_price','user'));
    }

    public function invoices_orders_excel(User $user){

        return Excel::download(new InvoiceExport($user), 'invoices_orders.xlsx');
    }

}
