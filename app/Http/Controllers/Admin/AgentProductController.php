<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AgentProductController extends Controller
{


    public function index(Request  $request){

        $user = User::with('products')->findOrFail($request->id);
        $products =  $user['products'];

        $pageName = 'المنتجات';

        return view('admin.agent_products.index',compact('products','pageName'));
    }


    public function create(){
        $products = Product::whereIsSuspend(0)->whereIsDeleted(0)->get();

        if ($products->count() == 0){
            session()->flash('myErrors','للاسف لا يوجد منتجات حالية للإضافة تواصل مع الإدارة');
            return  redirect()->back();
        }

        return view('admin.agent_products.create',compact('products'));
    }


    public function store(Request  $request){

//        $user = Auth::user();
        $user = User::findOrFail(12);

        $user_product = DB::table('user_product')->whereUserIdAndProductId($user->id, $request->product_id)->first();

        if ($user_product){
            $user->products()->where('product_id' , $request->product_id)->update([ 'quantity' => $request->quantity , 'price' => $request->price ]);
        }else{
            $user->products()->attach(['product_id' => $request->product_id ],[ 'quantity' => $request->quantity , 'price' => $request->price ]);
        }

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'المنتج']),
            "url" => route('agent_products.index'),
        ]);
    }

    public function edit($id){
        $products = Product::whereIsSuspend(0)->whereIsDeleted(0)->get();

        if ($products->count() == 0){
            session()->flash('myErrors','للاسف لا يوجد منتجات حالية للإضافة تواصل مع الإدارة');
            return  redirect()->back();
        }

//        $user = Auth::user();
        $user = User::findOrFail(12);

        $product =  DB::table('user_product')->whereUserIdAndProductId($user->id, $id)->first();

        return view('admin.agent_products.edit',compact('products','product'));
    }

    public function update(Request  $request , $id){

        $user = User::findOrFail(12);

        $user_product = DB::table('user_product')->whereUserIdAndProductId($user->id, $request->product_id)->first();

        if ($user_product){
            $user->products()->where('product_id' , $request->product_id)->update([ 'quantity' => $request->quantity , 'price' => $request->price ]);
        }else{
            $user->products()->attach(['product_id' => $request->product_id ],[ 'quantity' => $request->quantity , 'price' => $request->price ]);
        }

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => "المنتج"]),
            "url" => route('agent_products.index'),
        ]);
    }



    public function renderProductData(Request  $request){

        $product = Product::findOrFail($request->product_id);

        return response()->json([
            'status' => 200,
            'html' => view('admin.agent_products.show_details' , compact('product'))->render(),
        ]);

    }


    public function delete(Request  $request){

        DB::table('user_product')->where('id', $request->id)->update(['is_deleted' => 1]);

        return response()->json([
            'status' => true,
            'message' => "تم المسح بنجاح",
            'id' => $request->id,
            'type' => $request->type

        ]);
    }


    public function suspend(Request $request)
    {

        $model = DB::table('user_product')->where('id', $request->id)->update(['is_suspend' => $request->type]);

        if ($request->type == 1) {

            $message = "لقد تم حظر القسم بنجاح";
        } else {
            $message = "لقد تم فك الحظر على القسم بنجاح";
        }

        return response()->json([
            'status' => true,
            'message' => $message,
            'id' => $request->id,
            'type' => $request->type

        ]);

    }

}
