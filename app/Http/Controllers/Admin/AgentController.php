<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Agent\StoreAgent;
use App\Models\City;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AgentController extends Controller
{

    public function index( )
    {
        $agents = User::where('defined_user','agent')->get();

        $pageName  = '  الوكلاء ';

        return view('admin.agents.index',compact('agents','pageName'));
    }


    public function create(){

        $user_cities = DB::table('user_city')->pluck('city_id');

        $cities = City::whereNotIn('id', $user_cities)->get();

        if ($cities->count() == 0){
            session()->flash('myErrors','للاسف لا يوجد مدينة فارغة لإضافة وكيل فيها ');

            return  redirect()->back();
        }
        $pageName  = '  إضافة وكيل ';

        return view('admin.agents.create',compact('cities','pageName'));
    }


    public function show($id){

        $user = User::where('defined_user','agent')->with('products','cities')->findOrFail($id);

        $image_profile = $user->imageProfile();

        return view('admin.agents.show',compact('user','image_profile'));
    }

    public function store(StoreAgent  $request){

        $data = collect($request->validated())->except(['image','cities'])->toArray();

        $user = User::create($data);
        $user->cities()->sync($request->cities);
        if ($request->hasFile('image')):
            $user->addMediaFromRequest('image')->toMediaCollection();
            $user->save();
        endif;

        session()->flash('success','تم إضافة الوكليل بنجاح');
        return redirect()->route('agents.index');
    }
}
