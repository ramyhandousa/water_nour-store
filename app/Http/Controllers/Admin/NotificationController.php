<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\sendPromoCodeVaild;
use App\Libraries\Main;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\Notification;
use App\Models\PromoCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    public $push;

    public function __construct(PushNotification $push )
    {
        $this->push = $push;
    }

    public function index(){
        $notifications = auth()->user()->notifications()->latest()->get();

        return view('admin.notifications.index',compact('notifications'));
    }

    public function create(){

        return view('admin.notifications.public');
    }

    public function promo_code(){

        $codes = PromoCode::whereIsSuspend(0)->whereIsDeleted(0)->whereDate('end_at', '>', Carbon::now())->get();

        if ($codes->count() == 0) {
            session()->flash("myErrors","للأسف لا يوجد حاليا اكواد خصم لارسالها ");
            return  redirect()->back();
        }
        return view('admin.notifications.promo_code',compact('codes'));
    }

    public function get_promo_code(Request  $request){
        $code = PromoCode::whereCode($request->id)->first();

        return response()->json([
            'status' => 200,
            'message' =>    "نسبة الخصم علي هذا الكود "  .$code->percent . " % " ,
        ],200);
    }

    public function store(sendPromoCodeVaild $request){

        $data = [
            'title'=>  $request->title,
            'body'=> $request->message
        ];

        if ($request->type === "topic"){
            Notification::create(['topic' => $request->type , 'title' => $request->title , 'body' => $request->message , 'type' => 30]);

            $this->push->sendPushNotification(null, null, $request->title,$request->message, $data,'topic' );

        }else{
            $this->user_insert_and_push_notification($request,$data);
        }

        session()->flash('success', __('trans.successSendPublicNotifications'));
        return redirect()->back();
    }

    public function send_promo_code(sendPromoCodeVaild  $request){

        $data = [
            'title'=>  $request->title,
            'body'=> $request->message
        ];

        if ($request->type === "topic"){
            $this->push->sendPushNotification(null, null, $request->title,$request->message . " " . $request->code, $data,'topic' );

        }else{
            $this->user_insert_and_push_notification($request,$data);
        }


        session()->flash('success',  " تم إرسال كود الخصم بنجاح ");
        return redirect()->back();
    }

    public function collect_data_users($request){

        $data = [];
        foreach ($request->users as $user){
            $data[] = $user;
        }
        return collect($data)->map(function ($q) use ($request){
            return [
                "user_id" => $q,
                "sender_id" => 1,
                "title" => $request->title,
                "body" => $request->message . "   ". $request->code,
                "type" =>  12,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        })->toArray();

    }

   public function send_to_users($request , $data){
       if ($request->type === "all_users" || $request->type === "all_agent" || $request->type === "all_deliveries"){

           if ($request->type === "all_users"){
               $devices = User::where('defined_user', 'user')->join('devices','devices.user_id','users.id')->pluck('device');

           }elseif ($request->type === "all_agent"){

               $devices = User::where('defined_user', 'agent')->join('devices','devices.user_id','users.id')->pluck('device');

           }else{
               $devices = User::where('defined_user', 'delivery')->join('devices','devices.user_id','users.id')->pluck('device');
           }

       }else{

           $devices = Device::whereUserId('user_id',$request->users)->pluck('device');
       }
       $this->push->sendPushNotification($devices, null, $request->title,$request->message, $data );
    }


    public function renderUsesData(Request  $request){

        $query = User::where('defined_user', $request->type)->where('is_suspend',0);

        if ($request->type == "agent"){

            $data =   $query->where('defined_user',"=","agent");

        }elseif ($request->type == "delivery"){

            $data =  $query->where('is_active',1)->where('is_accepted',1)->where('agent_accepted',1);

        }else{

            $data =   $query->where('is_active',1);
        }

        $users = $data->get();

         if (count($users) > 0){
             return response()->json([
                 'status' => 200,
                 'html' => view('admin.notifications.show_users' , compact('users'))->render(),
             ]);
         }
        return response()->json([
            'status' => 400,
            'message' => "للاسف لا يوجد اي عدد متاح",
            "type" => $request->type
        ],400);

    }


    public function delete(Request $request){

        $notification = Notification::find($request->id);

        if ($notification){
            $notification->delete();
        }
        return response()->json([
            'status' => 200,
            'message' => "تم مسح الإشعار بنجاح",
        ]);
    }


    public function user_insert_and_push_notification($request , $data){

        if (in_array($request->type,['all_users','all_agent','all_deliveries'])){

            Notification::create(['topic' => $request->type , 'title' => $request->title , 'body' => $request->message. " " .$request->code , 'type' => 30]);

            $query = User::join('devices','devices.user_id','users.id');
            if ($request->type === "all_users"){
                $devices = $query->where('defined_user', 'user')->pluck('device');

            }elseif ($request->type === "all_agent"){
                $devices = $query->where('defined_user', 'agent')->pluck('device');

            }else{
                $devices = $query->where('defined_user', 'delivery')->pluck('device');
            }
        }

        if (in_array($request->type,['user','agent','delivery'])) {

            Notification::insert($this->collect_data_users($request));

            $devices = Device::whereHas('user',function ($user) use ($request){
                $user->whereIn('id', $request->users);
            })->pluck('device');
        }

        $this->push->sendPushNotification($devices, null, $request->title,$request->message. " " .$request->code, $data );
    }
}
