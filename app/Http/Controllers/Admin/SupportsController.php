<?php

namespace App\Http\Controllers\Admin;

use App\Device;
use App\Libraries\Main;
use App\Libraries\PushNotification;
use App\Message;
use App\Notifications;
use App\Models\Support;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Carbon\Carbon;
use App\Models\Notification;

class SupportsController extends Controller
{


    public $push;
    public $main;

    public function __construct(PushNotification $push, Main $main)
    {

        $this->push = $push;
        $this->main = $main;

    }


    public function index()
    {
        $supports = Support::whereParentId(0)->get();
        return view('admin.supports.index')->with(compact('supports'));
    }

    public function show($id)
    {

        $message = Support::with('user')->whereId($id)->first();
        $message->is_read = 1;
        $message->save();

        return view('admin.supports.show')->with(compact('message'));
    }

    public function reply(Request $request, $id)
    {
        $message = Support::findOrFail($id);

        $sender = User::whereId($message->user_id)->first();

        if ($request->message == '') {
            return response()->json([
                'status' => false,
                'message' => 'من فضلك ادخل بيانات الرسالة ثم اعد الإرسال'
            ]);
        }

        $support = new Support;
        $support->message = $request->message;
        $support->type_id = 1;
        $support->user_id = $message->sender_id;
        $support->sender_id = auth()->id();
        $support->parent_id = $id;

        if ($support->save()) {

            return response()->json([
                'status' => true,
                'message' => __('web.reply_sent_successfully'),
                'data' => $support

            ], 200);

        } else {
            return response()->json([
                'status' => false,
            ]);
        }
    }

    
    public function delete(Request $request)
    {


        if (!Gate::allows('users_manage')) {
            return abort(401);
        }


        $model = Support::findOrFail($request->id);

        if ($model->children->count() > 0)
            $model->children()->delete();


        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => [
                    'id' => $request->id
                ]
            ]);
        }


    }

}
