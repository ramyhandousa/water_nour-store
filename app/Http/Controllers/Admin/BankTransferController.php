<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BankTransfer;
use Illuminate\Http\Request;

class BankTransferController extends Controller
{
   public function index(){
       $banks = BankTransfer::latest()->get();
       $pageName = 'التحويلات البنكية' ;

       return view('admin.bankTransfer.index',compact('banks','pageName'));
   }

    public function accepted(Request $request){

        $bank = BankTransfer::whereId($request->id)->first();

        $bank->update(['is_accepted'=> 1]);

        return response()->json([
            'status'=>true,
            'title'=>"نجاح",
            'message'=>"تم قبول الطلب بنجاح",
        ]);
    }

    public function refuse(Request $request){

        $bank = BankTransfer::whereId($request->id)->first();

        $bank->update(['is_accepted' =>  -1 ,'message' => $request->refuse_reason]);

        session()->flash('success','تم رفض الطلب بنجاح');
        return redirect()->back();
    }

}
