<?php

namespace App\Http\Requests\api\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class phoneRequestVaild extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'phone' => 'required|exists:users',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => trans('global.required'),
            'phone.email' => trans('validation.phone'),
//            'email.email' => 'يجب ان يكون شكل إيميل',
            'phone.exists' =>  trans('global.user_not_found'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
