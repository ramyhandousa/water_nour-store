<?php

namespace App\Http\Requests\api\users\address;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class storeAddressVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id'           => 'required|exists:cities,id',
            'name'              => 'required|max:255',
            'phone'             => "required|max:255",
            'street'            => "required|max:255",
            'building_number'   => "nullable|max:10",
            'floor'             => "nullable|max:255",
            'flat'              => "nullable|max:255",
            'additional_notes'  => "nullable|max:255",
            'latitude'          => "required|max:255",
            'longitude'         => "required|max:255",
            'address'           => "required|max:255",
        ];
    }

    protected function failedValidation(Validator $validator) {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 400));
    }
}
