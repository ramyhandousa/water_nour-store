<?php

namespace App\Http\Requests\api\cart;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CartUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity'   => 'required|numeric|min:1',
        ];
    }

    public function withValidator($validator)
    {

        $validator->after(function ($validator) {

           $product_amount = $this->product->amount;

            if (count($this->user()->cart) <= 0)
            {
                $validator->errors()->add('product_amount', 'تاكد من وجود مشتريات لديك' );

            }

           if ($this->quantity >$product_amount ){
               $validator->errors()->add('product_amount', 'لا يوجد كمية كافية لإضافتها' );
           }
        });
        return;
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
