<?php

namespace App\Http\Requests\api\cart;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CartStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products'              => 'required|array',
            'products.*.id'         => 'required|exists:products,id',
            'products.*.quantity'   => 'numeric|min:1',
        ];
    }

    public function messages()
    {
        return [
            'products.*.id.exists' => 'تاكد من رقم المنتج  لانه غير موجود ',
        ];
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
