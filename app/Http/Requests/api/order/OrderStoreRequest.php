<?php

namespace App\Http\Requests\api\order;

use App\Models\DayWork;
use App\Models\Product;
use App\Models\Profile;
use App\Models\Setting;
use App\Models\UserProduct;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class   OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();

        if ($user->defined_user !== 'user') {
            return false;
        }else{
            return true;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_charity'                =>  'required|boolean',
            'agent_id'                  =>  'required|exists:users,id',
            'address_id'                =>  'required|exists:addresses,id',
//            'day_id'                    =>  'required|exists:days,id',
//            'time'                      =>  'required|date_format:H:i:s',
            'notes'                     =>  'max:225',
            'tax_number'                =>  'required|max:50',
            'tax'                       =>  'required|max:50',
            'tax_value'                 =>  'required|max:50',
            'promo_code'                =>  'max:20',
            'discount_code'             =>  'max:50',
            'discount_value'            =>  'max:50',
            'delivery_price'            =>  'required:max:50',
            'sum_price'                 =>  'required|max:10',
            'total_price'               =>  'required|max:10',
            'payment'                   =>  'required|in:cash,online',
            'products'                  =>  'required|array',
            'products.*.product_id'     =>  'required|distinct|exists:products,id',
            'products.*.quantity'       =>  'required|max:255',
            'products.*.price'          =>  'required|max:255',
            'products.*.offer_price'    =>  'max:255',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            if ($this->day_id){

                $day =  DayWork::whereUserId($this->agent_id)->whereDayId($this->day_id)->first();


                if ($day && $day->working == 0){
                    $validator->errors()->add('product_max','للأسف لا يوجد عمل في هذا اليوم ');
                }
            }

            $minimum_order = Setting::getBody("minimum_order");

            if ($minimum_order != 0 && $minimum_order > $this->total_price ){

                $validator->errors()->add('minimum_order',  ' للأسف اقل قيمة إجمالية للطلب '. $minimum_order .  " ريال");
            }



            if ($this->products){
                $flage[] = true;
                $error = [];

                foreach ($this->products as $item) {


                    $product = UserProduct::whereHas('product',function ($product) use ($item){

                        $id = isset($item['product_id']) ? $item['product_id'] :null ;

                        $product->where('user_id',$this->agent_id)->where('id',$id);
                    })->first();

                    if ($product){
                        $productQy = $product->quantity;

                        if ($item['quantity'] > $productQy) {

                            $flage[] = false;
                            $error[] = [
                                'product_id' => $product->product_id
                            ];
                        }
                    }

                }



                if (in_array(false, $flage)) {

                    $product = Product::find(collect($error)->first()['product_id']);

                    if ($product->quantity > 0) {
                        $message = ' للاسف الكمية غير متاحة من  ' . $product->name_ar . ' المتاح ' . $product->quantity;
                    } else {
                        $message = ' للاسف الكمية غير متاحة من  ' . $product->name_ar;
                    }

                    $validator->errors()->add('unavailable', $message);

                }
            }

        });
    }

    public function validated()
    {
        $data =  parent::validated();
        $data["day_id"] = 1;
        $data["time"] = date("H:i:s");
        $data["status"] = "pending";
        return $data;
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
