<?php

namespace App\Http\Requests\api\order;

use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class OrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' =>  'required|exists:order_details',
            'quantity' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'product_max' => trans('site.max_quantity'),
        ];
    }
    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order_details = OrderDetail::findOrFail($this->id);

            $product = Product::findOrFail($order_details->product_id);

            if ($this->quantity > $product->amount ) {
                $validator->errors()->add('product_max','كمية الطلب من هذا المنتج لا تكفي');
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
