<?php

namespace App\Http\Requests\api\order;

use App\Models\Order;
use App\Models\OrderDelivery;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class refuseDeliveryUnderway extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orderId'       => 'required|exists:orders,id',
            'delivery_id'   => 'required|exists:order_deliveries,user_id',
            'message'       => 'required|max:225'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = Order::findOrFail($this->orderId);

            if ($order->status !== 'accepted') {
                $validator->errors()->add('dealer', 'للاسف لا يمكنك حدوث اي عميلة الأن نم تغير حالة الطلب');
                return;
            }

            $order_delivery = $order->where('delivery_id',$this->delivery_id)->first();

            if (!$order_delivery){
                $validator->errors()->add('accepted_order_delivery', 'من فضلك تأكد من أن هذه العميلة تخصك  ');
                return;
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
