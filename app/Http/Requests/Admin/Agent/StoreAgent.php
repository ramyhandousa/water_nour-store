<?php

namespace App\Http\Requests\Admin\Agent;

use App\Models\City;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class StoreAgent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'       => 'required',
           'phone'      => 'required|unique:users|numeric',
           'email'      => 'required|email|unique:users',
           'password'   => 'required',
           'cities'   => 'required',
           'latitude'   => 'required',
           'longitude'  => 'required',
           'address'    => 'required',
           'image'      => 'image|mimes:jpeg,png,jpg|max:10000',
        ];
    }

    public function messages()
    {
        return [
            'cities.required' => 'من فضلك اضف مدينة واحدة للوكيل لإستقبال طلبات له'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            if ($this->cities){
                $user_cities = DB::table('user_city')->whereIn('city_id',$this->cities)->get();


                if ($user_cities->count() > 0) {
                    $city = City::find($user_cities->first()->city_id);
                    session()->flash('myErrors','للاسف تم حجز  مدينة '. $city->name_ar.' لوكيل اخر ');
                    $validator->errors()->add('myErrors', 'للاسف تم حجز  مدينة '. $city->name_ar.' لوكيل اخر ');
                    return;
                }
            }
        });
    }

    public function validated()
    {
        $data =  parent::validated();

        $data['defined_user'] = 'agent';

        return $data;
    }
}
