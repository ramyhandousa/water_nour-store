<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreCharity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'   => 'required',
            'latitude'  => 'required',
            'longitude' => 'required',
            'city_id'   => 'required',
            'desc_ar'   => 'required',
            'desc_en'   => 'required',
        ];
    }
}
