<?php

namespace App\Http\Requests\Admin;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CategoryRequestVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "name_ar"   => "required",
            "name_en"    => "required",
            'image'     => 'image|mimes:jpeg,png,jpg|max:10000',
            'order_by'  =>  ['required',
                'unique:categories,order_by,'. $this->category,
                'numeric' , 'min:1', 'max:10000']
        ];
    }

    public function messages()
    {
        return [
            "order_by.required" => "من فضلك اضف الرقم التسلسلي",
            "order_by.min" => "  الرقم التسلسلي اقل قيمة 1",
            "order_by.max" => "  الرقم التسلسلي اعلي قيمة 10000",
            "order_by.unique" => " الرقم التسلسلي تم أخده مسبقا ",
        ];
    }


    protected function failedValidation(Validator $validator) {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 400));
    }
}
