<?php


namespace App\Media;

use Spatie\MediaLibrary\Models\Media as BaseMedia;

class media extends  BaseMedia
{


    public static $image = [
        'image/png',
        'image/jpg',
        'image/jpeg',
    ];

    public static $video = [
        'video/mp4',
    ];

    public function type( ){

        $mime = $this->mime_type;

        if (in_array($mime , self::$image)){
            return  'image';
        }
        if (in_array($mime , self::$video)){
            return  'video';
        }

        return 'null';
    }


}
