<?php

namespace App\Models;

use App\Models\Collections\OrderCustomCollection;
use App\Traits\CanBeScoped;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    use CanBeScoped  ;
    protected $guarded = [];



    public function user(){
        return $this->belongsTo(User::class);
    }

    public function agent(){
        return $this->belongsTo(User::class,'agent_id');
    }

    public function delivery(){
        return $this->belongsTo(User::class ,'delivery_id');
    }

    public function day(){
        return $this->belongsTo(Day::class);
    }


    public function user_address(){
        return $this->belongsTo(Address::class,'address_id');
    }

    public function charity_address(){
        return $this->belongsTo(CharityAddress::class,'address_id');
    }

    public function orderDetails(){

        return $this->hasMany(OrderDetail::class);
    }

    public function order_delivery_waiting(){

        return $this->hasMany(OrderDelivery::class)->where('is_accepted','=',0);
    }

    public function order_delivery(){

        return $this->hasMany(OrderDelivery::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class,'order_details')
            ->withPivot(['amount','price'])->withTimestamps();
    }



    public function rateOrder($id){
        $user =  Auth::user();

        if ($user->defined_user === 'user'){

            $check = $this->user->rating_user_order->contains('id', $id) ;
            $rate = $check ? $this->user->rating_user_order->where('id',$id)->first()->pivot->rate : 0;
        }else{

            $check = $this->dealer->rating_dealer_order->contains('id', $id) ;
            $rate = $check ? $this->dealer->rating_dealer_order->where('id',$id)->first()->pivot->rate : 0;
        }

        return $check ?  $rate : 0 ;
    }



    public function newCollection(array $models = [])
    {
        return new OrderCustomCollection($models);
    }

//    public function getStatusTranslationAttribute()
//    {
//        $status = $this->attributes['status'];
//
//        if ($status)
//            switch ($status) {
//                case 'pending':
//                    return  __('global.pending_order');
//                    break;
//                case 'accepted':
//                    return  __('global.accepted_order');
//                    break;
//                case 'refuse_user':
//                    return __('global.refuse_user');
//                    break;
//                case 'refuse_agent':
//                    return __('global.refuse_dealer');
//                    break;
//                case 'finish':
//                    return __('global.finish_order');
//                    break;
//                default:
//                    return '';
//            }
//    }

}
