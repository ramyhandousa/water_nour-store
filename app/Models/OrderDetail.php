<?php

namespace App\Models;

use App\Models\Collections\OrderCustomCollection;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{

    protected $guarded = [];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function product_name(){
        return $this->product()->select("id","name_ar");
    }

}
