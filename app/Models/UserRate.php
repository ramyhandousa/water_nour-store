<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UserRate extends Model
{
    protected $table = "user_rate";

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function agent(){
        return $this->belongsTo(User::class,'agent_id');
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
