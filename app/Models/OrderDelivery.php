<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use App\User;
use Illuminate\Database\Eloquent\Model;

class OrderDelivery extends Model
{
    use CanBeScoped;

    protected $guarded = [];


    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
