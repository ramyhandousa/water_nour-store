<?php


namespace App\Models;


use App\Media\media;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class ProductImage extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = ['product_id' , 'media_id'];

    public function baseMedia(){
        return $this->belongsTo(media::class, 'media_id');
    }

}
