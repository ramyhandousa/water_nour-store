<?php

namespace App\Models;

use App\Media\media;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class UserImage extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = ['info'];

    public function baseMedia(){
        return $this->belongsTo(media::class, 'media_id');
    }

}
