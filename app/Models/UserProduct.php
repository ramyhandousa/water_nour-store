<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
    use CanBeScoped;

    protected $table = "user_product";

    public function offer(){
        return $this->belongsTo(Offer::class,'product_id','product_id');
    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id')->where('is_deleted','=',0);
    }


    public function user(){
        return $this->belongsTo(User::class);
    }


}
