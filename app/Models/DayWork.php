<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DayWork extends Model
{

    protected $guarded = [];

    public $timestamps =false;


    public function working_time(){
        return $this->hasMany(DayWorkTime::class);
    }
}
