<?php

namespace App\Models;

use App\Models\Collections\ProductCustomCollection;
use App\Traits\CanBeScoped;
use App\Traits\GlobalScope;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Product extends Model implements HasMedia
{
    use CanBeScoped  , InteractsWithMedia;

    protected $guarded = [];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function cities(){
        return $this->belongsToMany(City::class,'product_cities','product_id','city_id');
    }

    public function user_products(){
        return $this->belongsToMany(User::class,'user_product');
    }

    public function rating(){
        return $this->hasMany(UserRate::class);
    }

    public function user_favourites(){
        return $this->belongsToMany(User::class,'user_favourite');
    }


    public function checkInUserFavourite($id){
        return (boolean) $this->user_favourites->contains($id);
    }

    public function newCollection(array $models = [])
    {
        return new ProductCustomCollection($models);
    }

}
