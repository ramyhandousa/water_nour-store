<?php

namespace App\Models;

use App\Traits\GlobalScope;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Category extends Model implements HasMedia
{
    use  GlobalScope , InteractsWithMedia;

    public function products(){

        return $this->hasMany(Product::class);
    }
}
