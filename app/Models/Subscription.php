<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $dates = [ 'start_at' , 'end_at'];

    public function user(){
       return $this->belongsTo(User::class);
    }
}
