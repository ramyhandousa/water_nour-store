<?php


namespace App\Models\Collections;


use Illuminate\Database\Eloquent\Collection;

class ProductCustomCollection extends Collection
{

    public function mapCart(){

        return  $this->keyBy('id')->map(function ($product){
            return [
                'product_id'     =>  $product->id,
                'price'     =>  $product->price,
                'amount' =>  $product->pivot->quantity,
            ];
        })->toArray();
    }


    public function total_price_cart(){
       return  $this->sum('price');
    }
}
