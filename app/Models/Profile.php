<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Profile extends Model implements HasMedia
{
    use  InteractsWithMedia;

    protected $guarded = [];

    public function user(){
       return $this->belongsTo(User::class);
    }

    public function car(){
        return $this->belongsTo(CarType::class,'car_type_id');
    }


    public function imageProfile(  $customProperty = null){

       $image = $this->getMedia('default', ['image' => $customProperty]);

        if ($image->first()){
            return  $image->first()->getUrl();
        }
        return  null;
    }
}
