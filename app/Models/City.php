<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use App\Traits\GlobalScope;
use App\User;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use CanBeScoped , GlobalScope;


    public function agents(){
        return $this->belongsToMany(User::class,'user_city','city_id','user_id');
    }


    public function products(){
        return $this->belongsToMany(Product::class,'product_cities','city_id','product_id');
    }

}
